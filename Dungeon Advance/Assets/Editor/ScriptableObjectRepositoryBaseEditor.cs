﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Assets.Code.DataAccess.Interfaces;
using BusinessTycoonSim;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    /// <summary> This scriptable object is the base repository for entities that are managed by the unity3d editor. </summary>
    ///
    /// <typeparam name="T">         Generic type parameter. </typeparam>
    /// <typeparam name="TDatabase"> Type of the database. </typeparam>
    public abstract class ScriptableObjectRepositoryBaseEditor<T, TDatabase> :
        ScriptableObject, 
        IEditorRepository<T> where T : class, IEntity, new()
        where TDatabase : ScriptableObject        
    {
        [SerializeField] [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local")]
        protected List<T> Database = new List<T>();
        private readonly T _default = new T();

        public void Add(T data)
        {
            Database.Add(data);
            EditorUtility.SetDirty(this);
        }

        public void Insert(int index, T data)
        {
            Database.Insert(index, data);
            EditorUtility.SetDirty(this);
        }

        public void Remove(T data)
        {
            Database.Remove(data);
            EditorUtility.SetDirty(this);
        }

        public T GetAtIndex(int index)
        {
            return Database.ElementAt(index);
        }

        public void RemoveAt(int index)
        {
            Database.RemoveAt(index);
            EditorUtility.SetDirty(this);
        }

        public int Count => Database.Count;

        public T Get(int index)
        {
            return Database.ElementAt(index);
        }

        public void Replace(int index, T data)
        {
            Database[index] = data;
            EditorUtility.SetDirty(this);
        }

        public static TDatabase GetDatabase(string dbpath, string dbName) 
        {
            var dbFullPath = $@"Assets/{dbpath}/{dbName}";
            var db = AssetDatabase.LoadAssetAtPath(dbFullPath, typeof(TDatabase)) as TDatabase;

            if (db == null)
            {
                if (!AssetDatabase.IsValidFolder("Assets/" + dbpath))
                {
                    AssetDatabase.CreateFolder("Assets", dbpath);
                }

                db = CreateInstance<TDatabase>();
                AssetDatabase.CreateAsset(db, dbFullPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            return db;
        }


        public Guid Id { get; set; }
        public IEnumerable<PropertyInfoForDisplay> PropertyInformation => _default.GetPropertyInformationForDisplay();
    }
}