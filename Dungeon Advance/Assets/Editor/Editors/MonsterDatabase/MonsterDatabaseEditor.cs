﻿using Assets.Code.SlashNet.Monsters.Concrete;
using Assets.Editor;
using Assets.Editor.Editors.DatabaseEditorComponents;
using UnityEditor;
using UnityEngine;

namespace BusinessTycoonSim
{
    public class MonsterDatabaseEditor : EditorWindow
    {
        [SerializeField] private static MonsterRepository _monsterDatabase;
        private static ListView _listView;
        private static BottomBar _bottomBar;


        [MenuItem("Database/Monster Editor ")]
        public static void Init()
        {
            _listView = ScriptableObject.CreateInstance<ListView>();
            _bottomBar = ScriptableObject.CreateInstance<BottomBar>();
            _bottomBar.Init("Add New Monster", () => _monsterDatabase.Add(new MonsterTemplate()));
            CreateWindow();
        }
        private static void CreateWindow()
        {
            MonsterDatabaseEditor window = EditorWindow.GetWindow<MonsterDatabaseEditor>();
            window.minSize = new Vector2(600, 400);
            window.titleContent = new GUIContent("Monster Editor");
            window.Show();
        }

        void OnEnable()
        {
            //_monsterDatabase = DataAccess.Monsters;
        }

        void OnGUI()
        {
            _listView.Show(_monsterDatabase);
            _bottomBar.Show($"Monsters: {_monsterDatabase.Count}");
        }



        

  
    }
}