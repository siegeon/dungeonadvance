﻿using Assets.Editor;
using Assets.Editor.Editors.DatabaseEditorComponents;
using Assets.Editor.MasterData;
using UnityEditor;
using UnityEngine;

namespace BusinessTycoonSim
{
    public class StoreDatabaseEditor : EditorWindow
    {
        [SerializeField] private static StoreRepository _storeRepository;
        private static ListView _listView;
        private static BottomBar _bottomBar;


        [MenuItem("Database/Store Editor %#i")]
        public static void Init()
        {
            _listView = ScriptableObject.CreateInstance<ListView>();
            _bottomBar = ScriptableObject.CreateInstance<BottomBar>();
            _bottomBar.Init("Add New Store", () => _storeRepository.Add(new StoreMasterData()));
            CreateWindow();
        }
        private static void CreateWindow()
        {
            StoreDatabaseEditor window = EditorWindow.GetWindow<StoreDatabaseEditor>();
            window.minSize = new Vector2(600, 400);
            window.titleContent = new GUIContent("Store Editor");
            window.Show();
        }

        void OnEnable()
        {
            //_storeRepository = DataAccess.Stores;
        }

        void OnGUI()
        {
            _listView.Show(_storeRepository);
            _bottomBar.Show($"Stores: {_storeRepository.Count}");
        }



        

  
    }
}