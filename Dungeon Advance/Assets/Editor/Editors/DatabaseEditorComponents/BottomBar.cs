﻿using System;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.Editors.DatabaseEditorComponents
{
    public class BottomBar : EditorWindow
    {
        private string _addNewButtonText;
        private Action _addNewAction;

        //exposed because constructor is owned for scriptable objects
        public void Init(string addNewButtonText, Action addNewAction)
        {
            _addNewButtonText = addNewButtonText;
            _addNewAction = addNewAction;
        }
        public void Show(string lableText)
        {
            GUILayout.BeginHorizontal("Box", GUILayout.ExpandWidth(true));
            GUILayout.Label(lableText);
            if (GUILayout.Button(_addNewButtonText))
            {
                _addNewAction();
            }
            GUILayout.EndHorizontal();
        }
    }
}