﻿using BusinessTycoonSim;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.Editors.DatabaseEditorComponents
{
    public class ListView : EditorWindow
    {
        private Vector2 _scrollPos;
        public void Show<T>(IEditorRepository<T> repository) where T : class
        {
            _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos, GUILayout.ExpandHeight(true));

            EditorHelpers.EditorHelpers.ListView_Editable(repository);
            Repaint();

            EditorGUILayout.EndScrollView();
        }
    }
}