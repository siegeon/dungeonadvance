﻿using Assets.Editor.Editors.DatabaseEditorComponents;
using Assets.Editor.MasterData;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.Editors.ManagerDatabase
{
    /// <summary> Editor for manager database. </summary>
    public class ManagerDatabaseEditor : EditorWindow
    {
        [SerializeField]private static ManagerRepository _managerDatabase;

        private static ListView _listView;
        private static BottomBar _bottomBar;
        
        [MenuItem("Database/Manager Editor %#w")]
        public static void Init()
        {
            _listView = ScriptableObject.CreateInstance<ListView>();
            _bottomBar = ScriptableObject.CreateInstance<BottomBar>();
            _bottomBar.Init("Add New Manager", () => _managerDatabase.Add(new ManagerMasterData()));
            CreateWindow();
        }

        private static void CreateWindow()
        {
            ManagerDatabaseEditor window = EditorWindow.GetWindow<ManagerDatabaseEditor>();
            window.minSize = new Vector2(600, 400);
            window.titleContent = new GUIContent("Manager Editor");
            window.Show();
        }

        void OnEnable()
        {
            //_managerDatabase = Database.DataAccess.Managers;
        }

        void OnGUI()
        {
            _listView.Show(_managerDatabase);
            _bottomBar.Show($"Managers: {_managerDatabase.Count}");
        }

       
    }
}