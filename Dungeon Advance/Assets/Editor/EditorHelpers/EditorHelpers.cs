using BusinessTycoonSim;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.EditorHelpers
{
    public static class EditorHelpers
    {
        private static int _selectedIndex = -1
            ; // used to track the index of the element of the list that has had a GUI selection interaction.

        public static string LabelValue_Editable(string label, string value)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(label, GUILayout.Width(100));
            var newValue = GUILayout.TextField(value, GUILayout.Width(100));
            GUILayout.EndHorizontal();
            return newValue;
        }

        public static void ListView_Editable<T>(IEditorRepository<T> collection) where T : class
        {
            int index;
            GUILayout.BeginVertical();

            for (index = 0; index < collection.Count; index++)
            {
                DrawItem(collection, index);
            }


            GUILayout.EndVertical();
        }

        private static void DrawItem<T>(IEditorRepository<T> collection, int index) where T : class
        {
            GUILayout.BeginHorizontal("Box");

            var item = collection.GetAtIndex(index);


            PropertyInfoForDisplay info;
            var layoutManager = new LayoutManager(2);
            foreach (var t in collection.PropertyInformation)
            {
                t.PopulateValue(item);

                info = t;

                layoutManager.DrawElement(info);

                if (info.Type == typeof(Texture2D))
                    ManageTexture2D(index, item, info);
                else
                    item.SetValue(info, LabelValue_Editable(info.Name, info.Value.ToString()));
            }
            layoutManager.Close();
            if (GUILayout.Button("Delete", GUILayout.Width(EditorLayoutConstants.SpriteButtonSize*2), GUILayout.Height(EditorLayoutConstants.SpriteButtonSize)))
            {
                collection.RemoveAt(index);
            }
            else
            {
                collection.Replace(index, item);
            }


            
            GUILayout.EndHorizontal();
        }

        private static void ManageTexture2D<T>(int index, T item, PropertyInfoForDisplay info)
            where T : class
        {
            if (GUILayout.Button((Texture2D) info.Value, GUILayout.Width(EditorLayoutConstants.SpriteButtonSize),
                GUILayout.Height(EditorLayoutConstants.SpriteButtonSize)))
            {
                //opened the object picker for a texture 2d.
                _selectedIndex = index;
                EditorGUIUtility.ShowObjectPicker<Texture2D>(null, true, null,
                    GUIUtility.GetControlID(FocusType.Passive));
            }

            if (Event.current.commandName != "ObjectSelectorUpdated") return;
            if (_selectedIndex == -1 || index != _selectedIndex) return;

            item.SetValue(info, EditorGUIUtility.GetObjectPickerObject());
            _selectedIndex = -1;
        }
    }
}
