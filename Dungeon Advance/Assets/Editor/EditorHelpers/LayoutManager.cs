using UnityEngine;

namespace BusinessTycoonSim
{
    public class LayoutManager
    {
        private readonly int _itemsPerRow;
        private int _elementsInRow;

        public LayoutManager(int itemsPerRow)
        {
            _itemsPerRow = itemsPerRow;
            _elementsInRow = 0;
        }

        public void DrawElement(PropertyInfoForDisplay propertyInfo)
        {
            if (_elementsInRow == _itemsPerRow)
            {
                GUILayout.EndVertical();
                _elementsInRow = 0;
            }

            if(_elementsInRow == 0)
                GUILayout.BeginVertical();

            _elementsInRow += propertyInfo.RowHeight;

        }

        public void Close()
        {
            if (_elementsInRow != 0)
            {
                GUILayout.EndVertical();
            }
        }
    }
}