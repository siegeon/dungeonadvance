﻿using System.Collections.Generic;
using Assets.Code.DataAccess.Interfaces;
using BusinessTycoonSim;

namespace Assets.Editor
{
    public interface IEditorEntity : IEntity
    {
        IEnumerable<PropertyInfoForDisplay> PropertyInformation { get; }
    }

    public interface IEditorRepository<T> : IEditorEntity where T : class
    {
        int Count { get; }
        T GetAtIndex(int index);
        void RemoveAt(int index);
        void Replace(int index, T item);


    }
}