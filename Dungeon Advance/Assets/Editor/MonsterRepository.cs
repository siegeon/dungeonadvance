﻿using Assets.Code.SlashNet;
using Assets.Code.SlashNet.Monsters;
using Assets.Code.SlashNet.Monsters.Concrete;
using Assets.Code.SlashNet.Monsters.Types;
using SlashNet;

namespace Assets.Editor
{
    public class MonsterRepository : ScriptableObjectRepositoryBaseEditor<MonsterTemplate, MonsterRepository>
    {
        public IMonster Random()
        {
            return new SimpleMonster(Database[UnityEngine.Random.Range(0, Database.Count - 1)],SlashNetGame.Instance.Player.Level);
        }
    }
}