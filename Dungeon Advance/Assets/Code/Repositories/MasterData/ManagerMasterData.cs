﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.DataAccess.Interfaces;
using Assets.Databases;
using BusinessTycoonSim;
using UnityEngine;

namespace Assets.Editor.MasterData
{
    [Serializable]
    public class ManagerMasterData : IManager, IEntity
    {
        [SerializeField] private Guid _id;
        [SerializeField] private int _cost;
        [SerializeField] private string _name;
        [SerializeField] private Texture2D _image;
        private List<PropertyInfoForDisplay> _propertyInformation;

        public ManagerMasterData()
        {
            _id = Guid.NewGuid();
            _cost = 0;
            _name = string.Empty;
            _image = null;

        }
        [Editor(order: 4, height: 1)]
        public int Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }
        [Editor(order: 3, height: 1)]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        [Editor(order: 0, height: 2)]
        public Texture2D Image
        {
            get { return _image; }
            set { _image = value; }
        }
        [Editor(order: 2, height: 1)]
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public IEnumerable<PropertyInfoForDisplay> PropertyInformation => 
            _propertyInformation ?? (_propertyInformation = this.GetPropertyInformationForDisplay());
    }
}