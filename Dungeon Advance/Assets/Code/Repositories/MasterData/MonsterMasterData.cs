﻿using System;
using System.Collections.Generic;
using Assets.Code.DataAccess.Interfaces;
using BusinessTycoonSim;
using UnityEngine;

namespace Assets.Editor.MasterData
{
    [Serializable]
    public class MonsterMasterData : IEntity
    {
        //public EffectType[] Resistances;
        //public readonly Attack[] Attacks;
        //public readonly Drawable View;
        private IEnumerable<PropertyInfoForDisplay> _propertyInformation;
        [SerializeField] private int _ac;
        [SerializeField] private int _difficulty;
        [SerializeField] private int _experience;
        [SerializeField] private int _level;
        [SerializeField] private int _speed;
        [SerializeField] private string _name;
        [SerializeField] private Guid _id;
        //[SerializeField] private Type Type;

        public MonsterMasterData()
        {
            Name = "New";
        }

        [Editor(order: 0, height: 1)]
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [Editor(order: 1, height: 1)]
        public int AC
        {
            get { return _ac; }
            set { _ac = value; }
        }

        [Editor(order: 2, height: 1)]
        public int Difficulty
        {
            get { return _difficulty; }
            set { _difficulty = value; }
        }

        [Editor(order: 3, height: 1)]
        public int Experience
        {
            get { return _experience; }
            set { _experience = value; }
        }

        [Editor(order: 4, height: 1)]
        public int Level
        {
            get { return _level; }
            set { _level = value; }
        }

        [Editor(order: 5, height: 1)]
        public int Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        [Editor(order: 6, height: 1)]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }



        public IEnumerable<PropertyInfoForDisplay> PropertyInformation =>
            _propertyInformation ?? (_propertyInformation = this.GetPropertyInformationForDisplay());
    }
}
