﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.DataAccess.Interfaces;
using Assets.Databases;
using BusinessTycoonSim;
using UnityEngine;

namespace Assets.Editor.MasterData
{
    [Serializable]
    public class StoreMasterData : IStore, IEntity
    {
        [SerializeField] private int _baseStoreCost;
        [SerializeField] private int _baseStoreProfit;
        [SerializeField] private Guid _id;
        [SerializeField] private Texture2D _image;
        [SerializeField] private string _name;
        [SerializeField] private float _storeMultiplier;
        [SerializeField] private int _storeTimer;
        [SerializeField] private int _storeTimerDivision;
        private List<PropertyInfoForDisplay> _propertyInformation;

        public StoreMasterData()
        {
            Id = Guid.NewGuid();
            Name = string.Empty;
            Image = default(Texture2D);
            BaseStoreCost = 0;
            BaseStoreProfit = 0;
            StoreTimer = 0;
            StoreMultiplier = 0;
            StoreTimerDivision = 0;
        }

        [Editor(order:1, height: 1)]
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [Editor(order: 2, height: 1)]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [Editor(order: 0, height: 2)]
        public Texture2D Image
        {
            get { return _image; }
            set { _image = value; }
        }

        [Editor(order: 3, height: 1)]
        public int BaseStoreCost
        {
            get { return _baseStoreCost; }
            set { _baseStoreCost = value; }
        }

        [Editor(order: 4, height: 1)]
        public int BaseStoreProfit
        {
            get { return _baseStoreProfit; }
            set { _baseStoreProfit = value; }
        }

        [Editor(order: 5, height: 1)]
        public int StoreTimer
        {
            get { return _storeTimer; }
            set { _storeTimer = value; }
        }

        [Editor(order: 6, height: 1)]
        public float StoreMultiplier
        {
            get { return _storeMultiplier; }
            set { _storeMultiplier = value; }
        }

        [Editor(order: 7, height: 1)]
        public int StoreTimerDivision
        {
            get { return _storeTimerDivision; }
            set { _storeTimerDivision = value; }
        }
        //public UpgradesMasterData Upgrades { get; set; }

        //public ManagerMasterData Manager { get; set; }
        public IEnumerable<PropertyInfoForDisplay> PropertyInformation
        {
            get { return _propertyInformation ?? (_propertyInformation = this.GetPropertyInformationForDisplay()); }
        }
    }

}