﻿using System.Collections.Generic;

namespace Assets.Editor.MasterData
{
    public class UpgradesMasterData
    {
        public List<UpgradeMasterData> Upgrade { get; set; }
    }
}