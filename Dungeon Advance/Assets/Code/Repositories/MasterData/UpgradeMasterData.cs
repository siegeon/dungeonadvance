﻿namespace Assets.Editor.MasterData
{
    public class UpgradeMasterData
    {
        public string UpgradeName { get; set; }
        public string UpgradeCost { get; set; }
        public string UpgradeMultiplier { get; set; }
    }
}