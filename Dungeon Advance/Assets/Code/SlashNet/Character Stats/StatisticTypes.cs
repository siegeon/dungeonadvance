namespace Assets.Code.SlashNet.Character_Stats
{
    public enum StatisticTypes
    {
        None,
        Strength,
        Dexterity,
        Constitution,
        Intelligence,
        Wisdom,
        Charisma,
        Fire,
        Ice,
        Earth,
        Wind,
        Light,
        Shadow,
        Physical
    }
}