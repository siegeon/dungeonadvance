using System;
using System.Collections.Generic;
using Assets.Code.MVC.Models;

namespace Assets.Code.SlashNet.Character_Stats
{
    [Serializable]
    public class Statistic
    {
        public StatisticTypes Type { get; }
        private readonly double _baseCost;
        private readonly double _multiplyer;
        private readonly double _baseValue;
        private double _value;

        protected bool IsDirty = true;
        protected double LastBaseValue;
        public double Value
        {
            get
            {
                if (!IsDirty && LastBaseValue == _baseValue)
                    return _value;

                LastBaseValue = _baseValue;
                _value = CalculateFinalValue();
                IsDirty = false;
                return _value;
            }
        }

        protected readonly List<StatModifier> StatModifiers;
        
        public Statistic(StatisticModel statisticModel)
        {
            _baseCost = statisticModel.Cost;
            _multiplyer = statisticModel.Multiplyer;
            _baseValue = statisticModel.Value;
            Type = statisticModel.Type;
            StatModifiers = new List<StatModifier>();
        }
        
        public virtual void AddModifier(StatModifier mod)
        {
            IsDirty = true;
            StatModifiers.Add(mod);
            StatModifiers.Sort(CompareModifierOrder);
        }

        public virtual bool RemoveModifier(StatModifier mod)
        {
            if (!StatModifiers.Remove(mod))
                return false;
            IsDirty = true;
            return true;
        }

        public virtual bool RemoveAllModifiersFromSource(object source)
        {
            var didRemove = false;

            for (var i = StatModifiers.Count - 1; i >= 0; i--)
                if (StatModifiers[i].Source == source)
                {
                    IsDirty = true;
                    didRemove = true;
                    StatModifiers.RemoveAt(i);
                }

            return didRemove;
        }

        protected virtual int CompareModifierOrder(StatModifier a, StatModifier b)
        {
            if (a.Order < b.Order)
                return -1;
            if (a.Order > b.Order)
                return 1;
            return 0;
        }

        protected virtual double CalculateFinalValue()
        {
            var finalValue = _baseValue;
            float sumPercentAdd = 0;

            for (var i = 0; i < StatModifiers.Count; i++)
            {
                var mod = StatModifiers[i];

                if (mod.Type == StatModType.Flat)
                {
                    finalValue += (int)mod.Value;
                }
                else if (mod.Type == StatModType.PercentAdd)
                {
                    sumPercentAdd += mod.Value;

                    if (i + 1 >= StatModifiers.Count || StatModifiers[i + 1].Type != StatModType.PercentAdd)
                    {
                        finalValue *= (int)(1 + sumPercentAdd);
                        sumPercentAdd = 0;
                    }
                }
                else if (mod.Type == StatModType.PercentMult)
                {
                    finalValue *= (int) (1 + mod.Value);
                }
            }

            // Workaround for float calculation errors, like displaying 12.00002 instead of 12
            return finalValue;
        }

        public StatisticModel ToModel =>
            new StatisticModel
            {
                Type = Type,
                Cost = _baseCost,
                Value = _baseValue,
                Multiplyer = _multiplyer
            };
    }
}