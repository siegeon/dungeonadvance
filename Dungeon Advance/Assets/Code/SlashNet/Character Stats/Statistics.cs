using System;
using System.Collections;
using Assets.Code.MVC.Models;
using Newtonsoft.Json;

namespace Assets.Code.SlashNet.Character_Stats
{
    public class Statistics : IEnumerable, IEquatable<Statistics>
    {
        private readonly StatisticsModel _stats;

        public Statistics(StatisticsModel model)
        {
            _stats = model;
        }

        public StatisticsModel ToModel => _stats;

        public double Value
        {
            get
            {
                double total = new double();

                foreach (var statsStatisticModel in _stats.StatisticModels)
                {
                    total += statsStatisticModel.Value;
                }

                return total;
            }
        }

        public IEnumerator GetEnumerator() => _stats.StatisticModels.GetEnumerator();

        public bool Equals(Statistics other)
        {
            if (other == null) return false;
            var equal = JsonConvert.SerializeObject(this._stats) == JsonConvert.SerializeObject(other._stats);
            return equal;
        }

        public override bool Equals(object obj)
        {
            return Equals((Statistics) obj);
        }

        public override int GetHashCode()
        {
            return (_stats != null ? _stats.GetHashCode() : 0);
        }

        public static bool operator ==(Statistics left, Statistics right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Statistics left, Statistics right)
        {
            return !Equals(left, right);
        }
    }
}