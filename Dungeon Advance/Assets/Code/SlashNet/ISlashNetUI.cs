using System;
using Assets.Code.SlashNet.Common;

namespace SlashNet
{
    public interface ISlashNetUI
    {
        void Pline(String message);
        void EndPline();

        void ShowStack(ItemStack itemStack, bool isInventory);

        void DYWYPI();
    }
}