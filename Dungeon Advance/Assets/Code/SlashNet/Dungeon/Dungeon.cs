using System;
using System.Collections.Generic;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Dungeon;
using Assets.Code.SlashNet.Monsters;
using Assets.SlashNet.Dungeon;
using Assets.Structures;

namespace SlashNet.Dungeon
{
    public class GenericDungeon : IDungeon
    {
        private readonly Tile[,] _board;
        public byte[,] Grid { get; }

        #region IDungeon

        #region Size

        public int Width { get; }

        public int Height { get; }


        #endregion

        #region Visit

        public bool Visit(Point p)
        {
            return Visit(p.X, p.Y);
        }

        public bool Visit(ILocatable loc)
        {
            return Visit(loc.Location);
        }

        public bool Visit(int x, int y)
        {
            _board[x, y].Visit();

            return IsObstacle(x, y);
        }

        #endregion

        #region Contains

        public bool Contains(int x, int y)
        {
            return x >= 0 && x < Width && y >= 0 && y < Height;
        }

        public bool Contains(Point p)
        {
            return Contains(p.X, p.Y);
        }

        public bool Contains(ILocatable loc)
        {
            return Contains(loc.Location);
        }

        #endregion

        #region IsObstacle

        public bool IsObstacle(int x, int y) => this[x, y].IsObstacle;

        public bool IsObstacle(Point p) => IsObstacle(p.X, p.Y);

        public bool IsObstacle(ILocatable loc) => IsObstacle(loc.Location);

        #endregion

        #region Tile
        
        public Tile this[int x, int y]
        {
            get
            {
                try
                {
                    return _board[x, y];
                }
                catch (Exception e)
                {
                    e.Data.Add("requested tile", $"{x},{y}");
                    throw;
                }

            }
        }

        public Tile this[Point p] => this[p.X, p.Y];

        public Tile this[ILocatable loc] => this[loc.Location];

        #endregion

        #region reset

        public void Reset()
        {
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    _board[i, j].Reset();
                }
            }
        }

        #endregion

        #endregion


        public Point Start { get; }

        public GenericDungeon(TileType[,] tmpMap, Point start)
        {
            Height = tmpMap.GetLength(0);
            Width = tmpMap.GetLength(1);
            Grid = new byte[Width, Height];

            Start = start;
            _board = new Tile[Width, Height];
;
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    if (i < Width - 1 && j < Height - 1)
                    {
                        _board[i, j] = new Tile(tmpMap[i, j], new Point(i, j));
                    }
                    else
                    {
                        _board[i, j] = new Tile(TileType.Rock, new Point(i, j));
                    
                    }

                    Grid[i, j] = _board[i, j].AsByte;
                }
            }
        }

        #region IDungeon Members

        private readonly Dictionary<Guid, IMonster> _monsters = new Dictionary<Guid, IMonster>();

        public void Remove(IMonster monster)
        {
            this[monster].Monster = null;
            _monsters.Remove(monster.Id);
        }

        public void Add(IMonster monster)
        {
            _monsters.Add(monster.Id, monster);
            this[monster].Monster = monster;
        }

        public Point GetRandomOpenPoint()
        {
            Point p;
            do
            {
                p = new Point(Random.Next(0, Width * 2), Random.Next(0, Height * 2));
            } while (!IsOpen(p));

            return p;
        }

        public bool IsOpen(Point point) =>
            Contains(point) &&
            !IsObstacle(point) &&
            this[point].Monster == null;

        #endregion

        #region IEnumerable<AMonster> Members

        public IEnumerator<IMonster> GetEnumerator()
        {
            return ((IEnumerable<IMonster>) _monsters.Values).GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}

