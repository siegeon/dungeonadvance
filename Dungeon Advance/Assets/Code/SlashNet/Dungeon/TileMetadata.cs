using System.Collections.Generic;

namespace SlashNet.Dungeon
{
    public class TileMetadata
    {
        internal readonly Dictionary<TileType, bool> Obsticals = new Dictionary<TileType, bool>
        {
            {TileType.Corridor, false},
            {TileType.Rock, true},
            {TileType.Floor, false},
            {TileType.Door, false},
            {TileType.Invisible, true},
            {TileType.VWall, true},
            {TileType.HWall, true},
            {TileType.NECorner, true},
            {TileType.NWCorner, true},
            {TileType.SECorner, true},
            {TileType.SWCorner, true},
            {TileType.NTWall, true},
            {TileType.ETWall, true},
            {TileType.STWall, true},
            {TileType.WTWall, true},
            {TileType.XWall, true}
        };
        internal readonly Dictionary<TileType, byte> Bytes = new Dictionary<TileType, byte>
        {
            {TileType.Corridor, 1},
            {TileType.Rock, 0},
            {TileType.Floor, 1},
            {TileType.Door, 1},
            {TileType.Invisible, 0},
            {TileType.VWall, 0},
            {TileType.HWall, 0},
            {TileType.NECorner, 0},
            {TileType.NWCorner, 0},
            {TileType.SECorner, 0},
            {TileType.SWCorner, 0},
            {TileType.NTWall, 0},
            {TileType.ETWall, 0},
            {TileType.STWall, 0},
            {TileType.WTWall, 0},
            {TileType.XWall, 0}
        };

        public TileMetadata()
        {
        }
    }
}