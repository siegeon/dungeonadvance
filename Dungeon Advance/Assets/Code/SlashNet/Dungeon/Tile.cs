using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Monsters;
using Assets.SlashNet.Monsters;
using Assets.Structures;
using SlashNet;
using SlashNet.Dungeon;

namespace Assets.SlashNet.Dungeon
{
    public class Tile
    {
        private readonly TileMetadata _tileMetadata;

        internal ItemStack Items { get; }

        public Tile(TileType type, Point point)
        {
            Items = new ItemStack();
            Type = type;
            Point = point;
            Reset();
            _tileMetadata = new TileMetadata();
        }

        public TileType Type { get; } 
        public Point Point { get; }
        public bool Visible { get; private set; }
        public bool Seen { get; private set; }
        public bool Visited { get; private set; }

        public bool IsObstacle => _tileMetadata.Obsticals[Type];
        public byte AsByte => _tileMetadata.Bytes[Type];

        public bool HasMonster => Monster != null;

        public IMonster Monster { get; set; }

        public void Visit()
        {
            Visible = true;
            Seen = true;
            Visited = true;
        }

        public void Reset()
        {
            Visible = false;
            Seen = false;
            Visited = false;
        }
        
    }
}
