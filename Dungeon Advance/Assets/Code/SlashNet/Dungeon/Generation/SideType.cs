namespace SlashNet.Dungeon.Generation
{
    public enum SideType
    {
        Wall,
        Empty,
        Door
    };
}