using System;
using System.Collections.Generic;
using Assets.Structures;
using SlashNet;
using SlashNet.Dungeon;
using SlashNet.Dungeon.Generation;
using Random = SlashNet.Random;

namespace Assets.Code.SlashNet.Dungeon.Generation
{

    internal class Map : CellGrid
    {
        private readonly List<Point> _visited = new List<Point>();
        private readonly List<Room> _rooms = new List<Room>();

        internal Map(int width, int height)
            : base(width, height)
        {


        }

        internal void MarkCellsUnvisited()
        {
            for (int x = 0; x < Width; x++)
            for (int y = 0; y < Height; y++)
                Cells[x, y].Visited = false;
        }

        internal Point PickRandomCellAndMarkItVisited()
        {
            var p = new Point(
                Random.Next(Width - 1),
                Random.Next(Height - 1)
            );

            _visited.Add(p);
            this[p].Visited = true;

            return p;

        }

        internal bool HasAdjacentCellInDirection(Point location, DirectionType direction)
        {
            if (location.X < 0 || location.X >= Width || location.Y < 0 || location.Y >= Height)
                return false;

            switch (direction)
            {
                case DirectionType.North:
                    return location.Y > 0;
                case DirectionType.South:
                    return location.Y < (Height - 1);
                case DirectionType.West:
                    return location.X > 0;
                case DirectionType.East:
                    return location.X < (Width - 1);
                default:
                    return false;
            }
        }

        internal bool AdjacentCellInDirectionIsVisited(Point location, DirectionType direction)
        {
            if (!HasAdjacentCellInDirection(location, direction))
                throw new InvalidOperationException("No adjacent cell exists for the location and direction provided.");

            switch (direction)
            {

                case DirectionType.North:
                    return this[location.X, location.Y - 1].Visited;
                case DirectionType.West:
                    return this[location.X - 1, location.Y].Visited;
                case DirectionType.South:
                    return this[location.X, location.Y + 1].Visited;
                case DirectionType.East:
                    return this[location.X + 1, location.Y].Visited;
                default:
                    throw new InvalidOperationException();
            }

        }

        internal void FlagCellAsVisited(Point location)
        {
            if (LocationIsOutsideBounds(location))
                throw new ArgumentException("Location is outside of Map bounds", "location");
            if (this[location].Visited)
                throw new ArgumentException("Location is already visited", "location");

            this[location].Visited = true;
            _visited.Add(location);

        }

        private bool LocationIsOutsideBounds(Point location)
        {
            return ((location.X < 0) || (location.X >= Width)
                || (location.Y < 0) || (location.Y >= Height));
        }

        internal Point GetRandomVisitedCell(Point visited)
        {
            if (_visited.Count == 0)
                throw new InvalidOperationException("There are no visited cells");

            int index = Random.Next(_visited.Count - 1);

            while (_visited[index] == visited)
                index = Random.Next(_visited.Count - 1);

            return _visited[index];
        }

        internal Point CreateCorridor(Point location, DirectionType direction)
        {
            if (!HasAdjacentCellInDirection(location, direction))
                throw new InvalidOperationException(
                    "No adjacent cell exists for the location and direction provided.");
            this[location].IsCorridor = true;
            this[location.Translate(direction)].IsCorridor = true;
            return CreateSide(location, direction, SideType.Empty);


        }

        internal Point CreateSide(Point location, DirectionType direction, SideType type)
        {
            Point target = location.Translate(direction);

            this[location][direction] = type;
            this[target][DirectionOperations.Opposite(direction)] = type;

            return target;
        }

        internal bool AllCellsVisited
        {
            get { return _visited.Count == (Width * Height); }
        }

        internal IEnumerable<Point> DeadEndCellLocations
        {
            get
            {
                for (int x = 0; x < Width; x++)
                    for (int y = 0; y < Height; y++)
                        if (this[x, y].IsDeadEnd) yield return new Point(x, y);
            }
        }


        internal void CreateWall(Point location, DirectionType direction)
        {
            CreateSide(location, direction, SideType.Wall);
        }

        internal Point CreateDoor(Point location, DirectionType direction)
        {
            return CreateSide(location, direction, SideType.Door);
        }

        internal bool AdjacentCellInDirectionIsCorridor(Point location, DirectionType direction)
        {
            Point target = location.Translate(direction);
            if (LocationIsOutsideBounds(target))
                return false;
            return this[target].IsCorridor;
        }

        internal int CalculateRoomPlacementScore(Point location, Room room)
        {
            if (Bounds.Contains(new Rectangle(location.X, location.Y, room.Width + 1, room.Height + 1)))
            {
                int roomPlacementScore = 0;

                if (location.X == 0 || location.X == Width - 1 || location.Y == 0 || location.Y == Height - 1)
                    roomPlacementScore += 100;

                for (int x = 0; x < room.Width; x++)
                {
                    for (int y = 0; y < room.Height; y++)
                    {
                        Point loc = new Point(location.X + x, location.Y + y);
                        for (int dirNum = 0; dirNum < 4; dirNum++)
                        {
                            DirectionType d = (DirectionType)dirNum;

                            if (AdjacentCellInDirectionIsCorridor(loc, d))
                                roomPlacementScore++;
                        }
                        if (this[loc].IsCorridor)
                            roomPlacementScore += 3;

                        foreach (Room r in _rooms)
                        {
                            if (r.Bounds.Contains(loc.X, loc.Y))
                                roomPlacementScore += 100;
                        }
                    }
                }


                return roomPlacementScore;
            }
            return int.MaxValue;

        }

        internal IDungeon ExpandToDungeon(DungeonStyle expandAs)
        {
            TileType[,] tiles = new TileType[Width * 2 + 1, Height * 2 + 1];
            for (int x = 0; x < Width * 2 + 1; x++)
                for (int y = 0; y < Height * 2 + 1; y++)
                    tiles[x, y] = TileType.Rock;


            foreach (Room room in _rooms)
            {
                Point min = new Point(room.Bounds.X * 2 + 1, room.Bounds.Y * 2 + 1);
                Point max = new Point(room.Bounds.Right * 2, room.Bounds.Bottom * 2);

                for (int i = min.X; i < max.X; i++)
                {
                    for (int j = min.Y; j < max.Y; j++)
                    {
                        tiles[i, j] = TileType.Floor;
                    }
                }
            }


            if (expandAs == DungeonStyle.Hack)
            {

                for (int x = 1; x < Width * 2; x++)
                    for (int y = 1; y < Height * 2; y++)
                    {
                        //We are a floor.
                        if (tiles[x, y] == TileType.Floor)
                            continue;

                        //Floor to the left.
                        if (tiles[x - 1, y] == TileType.Floor)
                        {
                            //Floor to the left & right
                            if (tiles[x + 1, y] == TileType.Floor)
                            {
                                tiles[x, y] = TileType.VWall;
                                continue;
                            }

                            //Floor left & down & �(right & up)
                            if (tiles[x, y + 1] == TileType.Floor && tiles[x, y - 1] != TileType.Floor)
                            {
                                tiles[x, y] = TileType.SWCorner;
                                continue;
                            }

                            //Floor left & up & �(down & right)
                            if (tiles[x, y - 1] == TileType.Floor)
                                tiles[x, y] = TileType.NWCorner;
                            //Floor left & (�up & right & down)
                            else
                                tiles[x, y] = TileType.WTWall;
                            continue;

                        }
                        //Floor to the right & �left
                        if (tiles[x + 1, y] == TileType.Floor)
                        {

                            //Floor right & down & �(left & up)
                            if (tiles[x, y + 1] == TileType.Floor && tiles[x, y - 1] != TileType.Floor)
                            {
                                tiles[x, y] = TileType.SECorner;
                                continue;
                            }

                            //Floor right & up & �(left & down)
                            if (tiles[x, y - 1] == TileType.Floor)
                                tiles[x, y] = TileType.NECorner;
                            //Floor right & �(up & left & down)
                            else
                                tiles[x, y] = TileType.ETWall;
                            continue;
                        }

                        //Floor up & �(left & right)
                        if (tiles[x, y - 1] == TileType.Floor)
                        {
                            //up & down & �(left & right)
                            if (tiles[x, y + 1] == TileType.Floor)
                            {
                                tiles[x, y] = TileType.HWall;
                                continue;
                            }

                            //up & �(down & left & right)
                            tiles[x, y] = TileType.NTWall;
                            continue;


                        }

                        //�(left & right & up)
                        if (tiles[x, y + 1] == TileType.Floor)
                        {
                            tiles[x, y] = TileType.STWall;
                            continue;
                        }


                    }

                for (int x = 1; x < Width * 2; x++)
                    for (int y = 1; y < Height * 2; y++)
                    {
                        if (
                            //The tile to the South is: v | se | sw | st | et | wt
                            (tiles[x, y + 1] == TileType.VWall || tiles[x, y + 1] == TileType.SECorner ||
                            tiles[x, y + 1] == TileType.SWCorner || tiles[x, y + 1] == TileType.STWall ||
                            tiles[x, y + 1] == TileType.ETWall || tiles[x, y + 1] == TileType.WTWall) &&

                            //The tile to the North is: v | ne | nw | nt | et | wt
                            (tiles[x, y - 1] == TileType.VWall || tiles[x, y - 1] == TileType.NECorner ||
                            tiles[x, y - 1] == TileType.NWCorner || tiles[x, y - 1] == TileType.NTWall ||
                            tiles[x, y - 1] == TileType.ETWall || tiles[x, y - 1] == TileType.WTWall) &&

                            //The tile to the East is: h | ne | se | et | nt | st
                            (tiles[x + 1, y] == TileType.HWall || tiles[x + 1, y] == TileType.NECorner ||
                            tiles[x + 1, y] == TileType.SECorner || tiles[x + 1, y] == TileType.ETWall ||
                            tiles[x + 1, y] == TileType.NTWall || tiles[x + 1, y] == TileType.STWall) &&

                            //The tile to the West is: h | nw | sw | et | nt | st
                            (tiles[x - 1, y] == TileType.HWall || tiles[x - 1, y] == TileType.NWCorner ||
                            tiles[x - 1, y] == TileType.SWCorner || tiles[x - 1, y] == TileType.ETWall ||
                            tiles[x - 1, y] == TileType.NTWall || tiles[x - 1, y] == TileType.STWall))

                            tiles[x, y] = TileType.XWall;
                    }

            }


            TileType corridorTile = TileType.Floor;
            if (expandAs == DungeonStyle.Hack)
                corridorTile = TileType.Corridor;

            foreach (Point cellLocation in CorridorCellLocations)
            {
                Point tileLocation = new Point(cellLocation.X * 2 + 1, cellLocation.Y * 2 + 1);
                tiles[tileLocation.X, tileLocation.Y] = corridorTile;

                if (this[cellLocation][DirectionType.North] == SideType.Empty)
                    tiles[tileLocation.X, tileLocation.Y - 1] = corridorTile;
                if (this[cellLocation][DirectionType.North] == SideType.Door)
                    tiles[tileLocation.X, tileLocation.Y - 1] = TileType.Door;

                if (this[cellLocation][DirectionType.South] == SideType.Empty)
                    tiles[tileLocation.X, tileLocation.Y + 1] = corridorTile;
                if (this[cellLocation][DirectionType.South] == SideType.Door)
                    tiles[tileLocation.X, tileLocation.Y + 1] = TileType.Door;

                if (this[cellLocation][DirectionType.West] == SideType.Empty)
                    tiles[tileLocation.X - 1, tileLocation.Y] = corridorTile;
                if (this[cellLocation][DirectionType.West] == SideType.Door)
                    tiles[tileLocation.X - 1, tileLocation.Y] = TileType.Door;

                if (this[cellLocation][DirectionType.East] == SideType.Empty)
                    tiles[tileLocation.X + 1, tileLocation.Y] = corridorTile;
                if (this[cellLocation][DirectionType.East] == SideType.Door)
                    tiles[tileLocation.X + 1, tileLocation.Y] = TileType.Door;

            }

            if (expandAs == DungeonStyle.Hack)
            {
                //Redo rooms to get rid of horrible corridors and add in corners.
                foreach (Room room in _rooms)
                {
                    Point min = new Point(room.Bounds.X * 2 + 1, room.Bounds.Y * 2 + 1);
                    Point max = new Point(room.Bounds.Right * 2, room.Bounds.Bottom * 2);

                    for (int i = min.X; i < max.X; i++)
                    {
                        for (int j = min.Y; j < max.Y; j++)
                        {
                            tiles[i, j] = TileType.Floor;
                        }
                    }
                    tiles[min.X - 1, min.Y - 1] = TileType.NWCorner;
                    tiles[max.X, min.Y - 1] = TileType.NECorner;
                    tiles[min.X - 1, max.Y] = TileType.SWCorner;
                    tiles[max.X, max.Y] = TileType.SECorner;
                }
            }

            Room startRoom = _rooms[global::SlashNet.Random.Next(_rooms.Count - 1)];

            int sX = startRoom.Location.X * 2 + 1 + global::SlashNet.Random.Next(startRoom.Width - 1);
            int sY = startRoom.Location.Y * 2 + 1 + global::SlashNet.Random.Next(startRoom.Height - 1);

            return new GenericDungeon(tiles, new Point(sX, sY));
        }

        internal void PlaceDoors()
        {
            foreach (Room room in _rooms)
            {
                bool north = false;
                bool east = false;
                bool south = false;
                bool west = false;

                foreach (Point cellLoc in room.CellLocations)
                {
                    Point p = cellLoc.Translate(room.Bounds.X, room.Bounds.Y);

                    if ((cellLoc.X == 0) && (AdjacentCellInDirectionIsCorridor(p, DirectionType.West)) && !west)
                    {
                        CreateDoor(p, DirectionType.West);
                        west = true;
                    }

                    if ((cellLoc.X == room.Width - 1) && (AdjacentCellInDirectionIsCorridor(p, DirectionType.East)) && !east)
                    {
                        CreateDoor(p, DirectionType.East);
                        east = true;
                    }

                    if ((cellLoc.Y == 0) && (AdjacentCellInDirectionIsCorridor(p, DirectionType.North)) && !north)
                    {
                        CreateDoor(p, DirectionType.North);
                        north = true;
                    }

                    if ((cellLoc.Y == room.Height - 1) && (AdjacentCellInDirectionIsCorridor(p, DirectionType.South))
                        && !south)
                    {
                        CreateDoor(p, DirectionType.South);
                        south = true;
                    }


                }
            }
        }

        internal void PlaceRoom(Point location, Room room)
        {

            // Offset the room origin to the new location
            room.Location = location;


            foreach (Point p in room.CellLocations)
            {
                // Translate the room cell location to its location in the dungeon
                Point dungeonLocation = new Point(location.X + p.X, location.Y + p.Y);
                for (int dirNum = 0; dirNum < 4; dirNum++)
                {
                    DirectionType d = (DirectionType)dirNum;
                    this[dungeonLocation][d] = room[p][d];
                }

                if (p.X == 0 && HasAdjacentCellInDirection(dungeonLocation, DirectionType.West))
                    CreateWall(dungeonLocation, DirectionType.West);
                if (p.X == room.Width - 1 && HasAdjacentCellInDirection(dungeonLocation, DirectionType.East))
                    CreateWall(dungeonLocation, DirectionType.East);
                if (p.Y == 0 && HasAdjacentCellInDirection(dungeonLocation, DirectionType.North))
                    CreateWall(dungeonLocation, DirectionType.North);
                if (p.Y == room.Height - 1 && HasAdjacentCellInDirection(dungeonLocation, DirectionType.South))
                    CreateWall(dungeonLocation, DirectionType.South);

            }

            _rooms.Add(room);
        }

        public IEnumerable<Point> CorridorCellLocations
        {
            get
            {
                for (int x = 0; x < Width; x++)
                    for (int y = 0; y < Height; y++)
                        if (this[x, y].IsCorridor) yield return new Point(x, y);
            }
        }
    }
}
