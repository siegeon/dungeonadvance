using System;
using System.Collections.Generic;
using Assets.Code.SlashNet.Dungeon.Generation;
using Assets.Structures;


namespace SlashNet.Dungeon.Generation
{
    internal class Room : CellGrid
    {
        private Point _Location;


        internal Point Location
        {
            get
            {
                return _Location;
            }
            set
            {
                _Location = value;
                Bounds = new Rectangle(value.X, value.Y, Width, Height);
            }
        }

        internal Room(int width, int height)
            : base(width, height)
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Cell cell = Cells[x, y];
                    cell[DirectionType.West] = (x == 0) ? SideType.Wall : SideType.Empty;
                    cell[DirectionType.East] = (x == Width - 1) ? SideType.Wall : SideType.Empty;
                    cell[DirectionType.North] = (y == 0) ? SideType.Wall : SideType.Empty;
                    cell[DirectionType.South] = (y == Height - 1) ? SideType.Wall : SideType.Empty;
                    
                }
            }
        }

    }
}
