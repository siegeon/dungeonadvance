using System.Collections.Generic;
using Assets.Structures;
using SlashNet.Dungeon.Generation;

namespace Assets.Code.SlashNet.Dungeon.Generation
{
    internal abstract class CellGrid
    {
        protected readonly Cell[,] Cells;

        internal Rectangle Bounds { get; set; }

        internal Cell this[int x, int y] => Cells[x, y];

        internal Cell this[Point p] => Cells[p.X, p.Y];

        internal int Width;

        internal int Height;

        internal IEnumerable<Point> CellLocations
        {
            get
            {
                for (int x = 0; x < Width; x++)
                    for (int y = 0; y < Height; y++)
                        yield return new Point(x, y);
            }
        }

        protected CellGrid(int width, int height)
        {
            Cells = new Cell[width, height];
            Bounds = new Rectangle(0, 0, width, height);
            Width = Cells.GetUpperBound(0) + 1;
            Height = Cells.GetUpperBound(1) + 1;

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Cells[x, y] = new Cell();
                }
            }
        }

    }
}
