using System.Collections.Generic;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Monsters;
using Assets.SlashNet.Dungeon;
using Assets.Structures;

namespace Assets.Code.SlashNet.Dungeon
{
    public interface IDungeon : IEnumerable<IMonster>
    {
        bool Contains(int x, int y);

        bool Contains(Point p);

        bool Contains(ILocatable loc);

        bool IsObstacle(int x, int y);

        bool IsObstacle(ILocatable loc);

        bool IsObstacle(Point p);

        Point Start { get; }

        bool Visit(int x, int y);

        bool Visit(ILocatable loc);

        bool Visit(Point p);

        void Reset();

        Tile this[int x, int y] { get; }

        Tile this[Point point] { get; }

        Tile this[ILocatable loc] { get; }

        int Width { get; }
        int Height { get; }

        byte[,] Grid { get; }

        void Remove(IMonster monster);

        void Add(IMonster monster);
        Point GetRandomOpenPoint();
        bool IsOpen(Point newPoint);
    }
}