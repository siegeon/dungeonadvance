using System;
using System.Collections.Generic;
using System.Text;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Dungeon;
using Assets.Structures;


namespace SlashNet.Dungeon.FOV
{

    public class DebugFOVCalculator : IFOVCalculator
    {
        #region IFOVCalculator Members

        public void CalculateFOV(IDungeon dungeon, int x, int y, int radius)
        {
            CalculateFOV(dungeon, new Point(), radius);
        }

        public void CalculateFOV(IDungeon dungeon, Point center, int radius)
        {
            dungeon.Reset();
            for (int i = 0; i < 80; i++)
            {
                for (int j = 0; j < 40; j++)
                {
                    dungeon.Visit(i, j);
                }
                
            }
        }

        public void CalculateFOV(IDungeon dungeon, ILocatable center, int radius)
        {
            CalculateFOV(dungeon, new Point(), radius);
        }

        #endregion
    }
    
    public class BasicFOVCalculator : IFOVCalculator
    {

        private static int YFromQuadrant(DirectionType quadrant)
        {
            switch (quadrant)
            {
                case DirectionType.North:
                    return -1;
                case DirectionType.South:
                    return 1;
            }
            return 0;
        }
        private static int XFromQuadrant(DirectionType quadrant)
        {
            switch (quadrant)
            {
                case DirectionType.East:
                    return 1;
                case DirectionType.West:
                    return -1;
            }

            return 0;
        }
        private static int XModFromQuadrant(DirectionType quadrant)
        {
            switch (quadrant)
            {
                case DirectionType.North:
                case DirectionType.South:
                    return 1;
            }

            return 0;
        }
        private static int YModFromQuadrant(DirectionType quadrant)
        {
            switch (quadrant)
            {
                case DirectionType.East:
                case DirectionType.West:
                    return 1;
            }

            return 0;
        }

        public void CalculateFOV(IDungeon dungeon, ILocatable loc, int radius)
        {
            CalculateFOV(dungeon, loc.Location, radius);
        }

        public void CalculateFOV(IDungeon dungeon, int x, int y, int radius)
        {
            CalculateFOV(dungeon, new Point(x, y), radius);
        }

        public void CalculateFOV(IDungeon dungeon, Point center, int radius)
        {
            dungeon.Reset();

            //for (int i = 0; i < 80; i++)
            //{
            //    for (int j = 0; j < 40; j++)
            //    {
            //        dungeon.Visit(i, j);
            //    }
            //}

            //return;

            for (int dirNum = 0; dirNum < 4; dirNum++)
            {
                DirectionType dir = (DirectionType)dirNum;

                for (int step = 1; step < radius; step++)
                {

                    int dx = XFromQuadrant(dir) * step;
                    int dy = YFromQuadrant(dir) * step;
                    int ddx = XModFromQuadrant(dir);
                    int ddy = YModFromQuadrant(dir);


                    dungeon.Visit(center.Translate(dx + ddx, dy + ddy));
                    dungeon.Visit(center.Translate(dx - ddx, dy - ddy));


                    if (dungeon.Visit(center.Translate(dx, dy)))
                        break;

                }


            }

            for (int i = -1; i < 2; i += 2)
            {
                for (int j = -1; j < 2; j += 2)
                {
                    int step = 1;
                    for (; step <= radius / 2; step++)
                    {
                        if (dungeon.Visit(center.Translate(step * i, step * j)))
                            goto next;
                        dungeon.Visit(center.Translate(step * i + i, step * j));
                        dungeon.Visit(center.Translate(step * i, step * j + j));
                    }



                next:
                    step = 0;

                }
            }
        }
    }
}
