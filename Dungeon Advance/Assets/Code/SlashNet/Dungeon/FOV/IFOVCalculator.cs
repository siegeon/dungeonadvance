using System;
using System.Collections.Generic;
using System.Text;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Dungeon;
using Assets.Structures;

namespace SlashNet.Dungeon.FOV
{
    public interface IFOVCalculator
    {
        void CalculateFOV(IDungeon dungeon, int x, int y, int radius);
        void CalculateFOV(IDungeon dungeon, Point center, int radius);
        void CalculateFOV(IDungeon dungeon, ILocatable center, int radius);
    }

}
