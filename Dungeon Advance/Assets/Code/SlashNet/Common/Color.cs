namespace SlashNet
{
    public struct SNColor
    {
        public readonly byte R, G, B;

        internal SNColor(byte r, byte g, byte b)
        {
            R = r;
            G = g;
            B = b;
        }

        internal static SNColor Black = new SNColor(0, 0, 0);
        internal static SNColor Green = new SNColor(0, 128, 0);
        internal static SNColor Red = new SNColor(255, 0, 0);
        internal static SNColor Orange = new SNColor(255, 165, 0);
        internal static SNColor LightGray = new SNColor(211, 211, 211);
        internal static SNColor Gray = new SNColor(128, 128, 128);
        internal static SNColor Yellow = new SNColor(255, 255, 0);
        internal static SNColor White = new SNColor(255, 255, 255);
        internal static SNColor Brown = new SNColor(165, 42, 42);
        internal static SNColor Blue = new SNColor(0, 0, 255);
        internal static SNColor Purple = new SNColor(128, 0, 128);

    }
}