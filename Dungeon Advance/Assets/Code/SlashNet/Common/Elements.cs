namespace SlashNet
{
    public enum EffectType
    {
        Physical, Fire, Ice, Lightning, Sleep, Poison, Water, Acid, Special, Rust, Disintegration, Speed,
        Stealth, Death
    }
}