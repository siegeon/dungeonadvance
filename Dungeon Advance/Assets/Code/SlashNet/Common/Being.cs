using Assets.Code.SlashNet.Combat;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Items.Armor;
using Assets.Code.SlashNet.Items.Weapons;
using Assets.Structures;
using SlashNet;
using Random = SlashNet.Random;

namespace Assets.SlashNet.Common
{
    public abstract class Being : Entity, IBeing
    {
        protected Being()
        {
            Location = new Point();
        }

        public abstract double Health { get; protected set; }
        public abstract double MaxHealth { get; protected set; }

        public Weapon Wielded { get; private set; }
        public Armor Worn { get; private set; }
        public void Wield(Weapon weapon) => 
            Wielded = weapon;
        public void Wear(Armor armor) => 
            Worn = armor;

        public bool LoseHP(double amount)
        {
            Health -= amount;
            return (Health <= 0);
        }

        public double Damage() => Wielded.Damage;
        public double Defense() => Worn.DamageReduction;
        public double RollAttack() => Random.D(20);

        public abstract ICombatResult OnAttacked(ICombatResult combatResult);

        //Return true if the attack does damage.
        public abstract ICombatResult OnAttacks(IBeing defender);

        public abstract double GetLoot();

        public Point LastLocation { get; private set; }
        private Point _location;
        public Point Location
        {
            get { return _location; }
            set
            {
                LastLocation = _location;
                _location = value;
            }
        }


    }
}