using System;
using Assets.Code.Utilities;

namespace SlashNet
{
    public class Random
    {
        private static readonly Lazy<Random> Lazy = new Lazy<Random>(() => new Random());
        public static Random Instance => Lazy.Value;        
        private Random()
        {
            _random = new MersenneTwister(new System.Random().Next());
        }

        private readonly MersenneTwister _random;

        public static int Next() => Instance._random.Next();
        public static int Next(int maxValue) => Instance._random.Next(maxValue);  
        public static int Next(int low, int high) => Instance._random.Next(low, high);

        public static int D(int sides)
        {
            return Instance._random.Next(1, sides);
        }

        public static int D(int num, int sides)
        {
            int res = 0;
            for (int n = num; n > 0; n--)
            {
                res += Instance._random.Next(1, sides);
            }
            return res;
        }
    }
}