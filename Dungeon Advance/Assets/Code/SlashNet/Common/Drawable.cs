using System;
using SlashNet;

namespace Assets.Code.SlashNet.Common
{
    public struct Drawable : IEquatable<Drawable>
    {
        public readonly string Char;
        public readonly SNColor Color;

        public Drawable(string character, SNColor color)
        {
            Char = character;
            Color = color;
        }

        public static readonly Drawable Null = new Drawable(" ", SNColor.Black);

        public bool Equals(Drawable other)
        {
            return string.Equals(Char, other.Char) && Color.Equals(other.Color);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Drawable && Equals((Drawable) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Char != null ? Char.GetHashCode() : 0) * 397) ^ Color.GetHashCode();
            }
        }

        public static bool operator ==(Drawable left, Drawable right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Drawable left, Drawable right)
        {
            return !left.Equals(right);
        }
    }
}
