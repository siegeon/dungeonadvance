using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet
{
    public class DirectionPicker
    {
        private readonly List<DirectionType> _DirectionsPicked = new List<DirectionType>();
        private readonly DirectionType _PreviousDirection;
        private readonly int _Modifier;

        public DirectionPicker(int modifier, DirectionType previous)
        {
            _Modifier = modifier;
            _PreviousDirection = previous;
        }

        public bool HasNextDirection
        {
            get
            {
                return _DirectionsPicked.Count < 4;
            }
        }

        public DirectionType GetNextDirection()
        {
            if (!HasNextDirection)
                throw new InvalidOperationException("No directions available");

            DirectionType picked;

            do
            {
                picked = MustChangeDirection ? pickDifferentDirection() : _PreviousDirection;
            } while (_DirectionsPicked.Contains(picked));

            _DirectionsPicked.Add(picked);

            return picked;
        }

        private DirectionType pickDifferentDirection()
        {
            DirectionType direction;
            do
            {
                direction = (DirectionType)Random.Next(3);
            } while (direction == _PreviousDirection && _DirectionsPicked.Count < 3);

            return direction;
        }

        private bool MustChangeDirection
        {
            get
            {
                return _DirectionsPicked.Count > 0 || _Modifier > Random.Next(0, 99);
            }
        }


    }
}
