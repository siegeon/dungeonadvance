using System;
using Assets.Code.DataAccess.Interfaces;

namespace Assets.Code.SlashNet.Common
{
    public abstract class Entity : IEntity
    {
        private Guid _id;

        protected Entity()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id
        {
            get { return _id; }
            set
            {
                if (value == Guid.Empty)
                {
                    _id = Guid.NewGuid();
                }

                _id = value;

            }
        }
    }
}