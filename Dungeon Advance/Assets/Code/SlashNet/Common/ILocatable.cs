using Assets.Structures;

namespace Assets.Code.SlashNet.Common
{
    public interface ILocatable
    {
        
        Point Location { get; set; }

        Point LastLocation { get; }


    }
}
