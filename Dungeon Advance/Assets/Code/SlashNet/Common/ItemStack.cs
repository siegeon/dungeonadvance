using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.SlashNet.Items.Weapons;
using Assets.Code.Utilities;

namespace Assets.Code.SlashNet.Common
{
    public class ItemStack
    {
        private readonly Dictionary<Guid, IItem> _items = new Dictionary<Guid, IItem>();

        public int Count => _items.Count;

        internal void Add(IItem item)
        {
            if (_items.ContainsKey(item.Id))
                _items[item.Id] = item;
            else
                _items.Add(item.Id, item);
        }

        internal void Add(IEnumerable<IItem> items)
        {
            items.GuardObjectNull(nameof(items));

            foreach (var item in items)
                Add(item);
        }
        internal void Remove(IItem item, int amount)
        {
            if (_items.ContainsKey(item.Id))
                _items.Remove(item.Id);
        }

        internal IEnumerable<IItem> GetAll()
        {
            var items = _items.Select(x => x.Value);
            _items.Clear();
            return items;
        }
    }
}