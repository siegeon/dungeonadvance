using Assets.Code.Utilities;
using SlashNet;

namespace Assets.Code.SlashNet.Common
{
    public interface IRollable
    {
        int Roll();
        int Sides { get; }
    }

    internal struct Die : IRollable
    {
        internal static Die Zero = new Die(-1);
        public int Sides { get; }

        internal Die(int sides)
        {
            Sides = sides;
        }


        #region IRollable Members

        public int Roll()
        {
            if (Sides < 0)
                return 0;
            return Sides == 1 ? 1 : Random.Next(1, Sides);
        }



        #endregion
    }
}