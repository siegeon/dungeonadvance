﻿using Assets.Code.DataAccess.Interfaces;
using Assets.Code.SlashNet.Combat;
using Assets.Code.SlashNet.Items.Armor;
using Assets.Code.SlashNet.Items.Weapons;
using SlashNet;

namespace Assets.Code.SlashNet.Common
{
    public interface IBeing : IEntity, ILocatable
    {
        ICombatResult OnAttacks(IBeing defender);

        Weapon Wielded { get; }
        Armor Worn { get; }
        void Wield(Weapon weapon);
        void Wear(Armor armor);

        double Damage();
        double Defense();
        double RollAttack();

        bool LoseHP(double dmg);
        double Health { get; }
        
        ICombatResult OnAttacked(ICombatResult combatResult);
        double GetLoot();
    }
}