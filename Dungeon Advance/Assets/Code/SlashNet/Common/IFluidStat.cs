namespace Assets.Code.SlashNet.Common
{
    public interface IFluidStat
    {
        /// <summary> Gets or sets the current value raises events when appropriate. </summary>
        int Current { get; }

        /// <summary> Gets or sets the maximum. </summary>
        /// <value> The maximum value. </value>
        int Max { get; }

        /// <summary> Gets or sets the minimum. </summary>
        /// <value> The minimum value. </value>
        int Min { get; }

        bool Depleted { get; set; }

        float PercentRemaining();

        /// <summary> Resets current to base. </summary>
        void Reset();

        /// <summary> Changes the stats and resets current to base. </summary>
        /// <param name="base"> The base. </param>
        /// <param name="min">  The minimum value. </param>
        /// <param name="max">  The maximum value. </param>
        void Reset(int @base, int min, int max);

        /// <summary> Removes the given amount. </summary>
        ///
        /// <param name="amount"> The amount to remove. </param>
        void Remove(int amount);
        /// <summary> Level up. </summary>
        ///
        /// <param name="additionalHp"> The additional hp. </param>
        void LevelUp(int additionalHp);
        /// <summary> Restores. </summary>
        ///
        /// <param name="heal"> The heal. </param>
        void Restore(int heal);

    }
}