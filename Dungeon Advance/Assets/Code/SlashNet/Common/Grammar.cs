using System;
using System.Collections.Generic;
using System.Text;
using Assets.Code.SlashNet;
using Assets.Code.SlashNet.Combat.Attacks;
using Assets.Code.SlashNet.Common;
using Assets.SlashNet.Common;

namespace SlashNet
{
    public class Grammar
    {
        private static readonly Lazy<Grammar> Lazy = new Lazy<Grammar>(() => new Grammar());
        public static Grammar Instance => Lazy.Value;
        private Grammar() { }

        public static string MakeAttackString(IBeing attacker, IBeing defender, string damage)
        {
            string prefix = MakePrefix(attacker);
            var action = "attack";
            string ending = "";


            var suffix = attacker == SlashNetGame.Instance.Player
                ? " the " + defender
                : (action.EndsWith("ch") ? "es" : "s");

            return $"{prefix} {action}{suffix} {ending} for {damage}";
        }

        internal static string MakePrefix(IBeing being)
        {
            return being == SlashNetGame.Instance.Player ? "You " : "The " + being + " ";
        }

        internal static string MakePossessivePrefix(IBeing being)
        {
            return being == SlashNetGame.Instance.Player ? "Your " : "The " + being + "'s ";
        }

        internal static string MakeIntrVSuffix(IBeing being, string verb)
        {
            return being == SlashNetGame.Instance.Player ? "" : "s";
        }

        internal static string MakeMissString(IBeing attacker, IBeing defender)
        {
            string prefix = MakePrefix(attacker);
            string action = "miss" + ((attacker == SlashNetGame.Instance.Player) ? " the " + defender : "es");
            return prefix + action;
        }
    }
}