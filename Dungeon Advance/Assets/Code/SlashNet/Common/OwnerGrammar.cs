using System;
using System.Collections.Generic;
using System.Text;
using Assets.Code.SlashNet.Player;
using Assets.SlashNet.Common;
using SlashNet.Player;

namespace SlashNet
{
    public class OwnerGrammar
    {
        public static string OwnsPrefix(Being owner)
        {
            if (owner is PlayerCharacter)
                return "Your";
            else
                return "The " + owner + "'s";
        }
    }
}
