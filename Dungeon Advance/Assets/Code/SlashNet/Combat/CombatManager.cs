﻿using Assets.Code.SlashNet.Common;

namespace Assets.Code.SlashNet.Combat
{
    public class CombatManager
    {
        private readonly IBeing _attacker;
        private readonly IBeing _defender;
        private CombatResult _combatResult;

        private CombatManager(IBeing attacker, IBeing defender)
        {
            _attacker = attacker;
            _defender = defender;
            _combatResult = new CombatResult(attacker, defender);
        }

        public static CombatResult InitiateCombat(IBeing attacker, IBeing defender) => 
            new CombatManager(attacker, defender)
                .AttackRoll()
                .CalculateDamage()
                .ApplyDefense()
                .ApplyDamage()
                .Finalize();

        private CombatManager ApplyDamage()
        {
            _combatResult = (CombatResult) _defender.OnAttacked(_combatResult);
            
            return this;
        }

        private CombatResult Finalize() => _combatResult;


        /// <summary>
        ///     An attack roll represents your attempt to strike your opponent on your turn in a round. When you make an
        ///     attack roll, you roll your attack dice and add your attack bonus. (Other modifiers may also apply to this roll.) If your
        ///     result equals or beats the target's Armor Class, you hit.
        /// </summary>
        /// <param name="defender"> The defender. </param>
        /// <returns> true if it succeeds, false if it fails. </returns>
        private CombatManager AttackRoll()
        {

            var attackRoll = _attacker.RollAttack();
            var hit = attackRoll != 1;

            _combatResult.CriticalHit = attackRoll == 20;
            _combatResult.CriticalMiss = attackRoll == 1;
            _combatResult.Hit = hit;

            return this;
        }

        /// <summary>
        ///     If your attack succeeds, you deal damage. The type of weapon used determines the amount of damage you deal.
        ///     Damage reduces a target's current hit points.
        ///     Minimum Damage: If penalties reduce the damage result to less than 1, a hit still deals 1 point of nonlethal
        ///     damage.
        ///     Strength Bonus: When you hit with a melee or thrown weapon, including a sling, add your Strength modifier to the
        ///     damage result. A Strength penalty, but not a bonus, applies on damage rolls made with a bow that is not a composite
        ///     bow.
        ///     Off-Hand Weapon: When you deal damage with a weapon in your off hand, you add only 1/2 your Strength bonus.If you
        ///     have a Strength penalty, the entire penalty applies.
        ///     Wielding a Weapon Two-Handed: When you deal damage with a weapon that you are wielding two-handed, you add 1-1/2
        ///     times your Strength bonus (Strength penalties are not multiplied). You don't get this higher Strength bonus,
        ///     however, when using a light weapon with two hands.
        ///     Multiplying Damage: Sometimes you multiply damage by some factor, such as on a critical hit.Roll the damage (with
        ///     all modifiers) multiple times and total the results.
        ///     Note: When you multiply damage more than once, each multiplier works off the original, unmultiplied damage. So if
        ///     you are asked to double the damage twice, the end result is three times the normal damage.
        ///     Exception: Extra damage dice over and above a weapon's normal damage are never multiplied.
        ///     Ability Damage: Certain creatures and magical effects can cause temporary or permanent ability damage (a reduction
        ///     to an ability score).
        /// </summary>
        /// <param name="attacker"> The attacker. </param>
        /// <param name="defender"> The defender. </param>
        /// <returns> An int. </returns>
        private CombatManager CalculateDamage()
        {
            if (!_combatResult.Hit)
            {
                _combatResult.Damage = 0;
            }
            else
            {
                //if (_combatResult.CriticalHit)
                //{
                //    ApplyWeaponDamage();
                //}
                _combatResult.Damage += _attacker.Damage();
            }

            return this;
        }

        private CombatManager ApplyDefense()
        {
            var damage = _combatResult.Damage > _defender.Defense() 
                ? _combatResult.Damage - _defender.Defense() 
                : 0;

            _combatResult.Damage = damage;

            return this;
        }
    }
}

