using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Common;
using Assets.Code.Utilities;
using SlashNet;

namespace Assets.Code.SlashNet.Combat.Attacks
{
    public struct Attack
    {
        private readonly IRollable _damage;

        public EffectType Effect { get; }

        public int Damage => _damage.Roll();

        public Attack(AttackModel attackModel) : this()
        {
            attackModel.GuardObjectNull(nameof(attackModel));

            Effect = attackModel.EffectType;
            _damage = new Die(attackModel.Sides);
        }

        public AttackModel ToModel => new AttackModel
        {
            EffectType = Effect,
            Sides = _damage.Sides
        };
    }
}
