using System.Linq;
using Assets.Code.MVC.Models;

namespace Assets.Code.SlashNet.Combat.Attacks
{
    public class Attacks : IAttacks
    {
        private readonly Attack[] _attacks;

        public Attacks(AttacksModel model)
        {
            _attacks = model.AttackModels.Select(x => new Attack(x)).ToArray();
        }

        public int Damage => _attacks[0].Damage;

        public AttacksModel ToModel => new AttacksModel
        {
            AttackModels = _attacks.Select(x=>x.ToModel).ToList()
        };
    }
}