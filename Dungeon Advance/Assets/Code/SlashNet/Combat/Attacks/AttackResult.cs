namespace Assets.Code.SlashNet.Combat.Attacks
{
    /// <summary>
    /// What can happen as the result of an attack.
    /// </summary>
    public enum AttackResult
    {
        /// <summary>
        /// The attack killed the target.
        /// </summary>
        Kill, 

        /// <summary>
        /// The attack mised completely.
        /// </summary>
        Miss, 

        /// <summary>
        /// The attack hit (and therefore must have done damage).
        /// </summary>
        Hit, 

        /// <summary>
        /// Nothing happened.
        /// </summary>
        Nothing
    }
}