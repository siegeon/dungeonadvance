﻿namespace Assets.Code.SlashNet.Combat.Attacks
{
    public interface IAttacks
    {
        int Damage { get; }
    }
}