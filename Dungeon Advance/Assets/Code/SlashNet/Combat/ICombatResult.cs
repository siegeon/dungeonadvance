﻿using Assets.Code.SlashNet.Common;

namespace Assets.Code.SlashNet.Combat
{
    public interface ICombatResult
    {
        IBeing Attacker { get; set; }
        IBeing Defender { get; set; }
        bool CriticalHit { get; set; }
        bool CriticalMiss { get; set; }
        double Damage { get; set; }
        bool Hit { get; set; }
        bool Killed { get; set; }
    }
}