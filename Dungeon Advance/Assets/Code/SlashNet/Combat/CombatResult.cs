﻿using Assets.Code.SlashNet.Common;

namespace Assets.Code.SlashNet.Combat
{
    public class CombatResult : ICombatResult
    {
        public bool Hit { get; set; }
        public bool Killed { get; set; }
        public double Damage { get; set; }
        public IBeing Attacker { get; set; }
        public IBeing Defender { get; set; }
        public bool CriticalHit { get; set; }
        public bool CriticalMiss { get; set; }

        public CombatResult(IBeing attacker, IBeing defender)
        {
            Attacker = attacker;
            Defender = defender;
            Hit = false;
            Damage = 0;
            CriticalHit = false;
        }
    }
}