using System;
using System.Collections.Generic;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.SlashNet.Common;
using Assets.SlashNet.Monsters;
using BusinessTycoonSim;
using SlashNet;
using UnityEngine;

namespace Assets.Code.SlashNet.Monsters.Concrete
{
    [Serializable]
    public class MonsterTemplate : IEntity
    {
        [SerializeField] public Guid Id { get; set; }
        [SerializeField] public String Name;
        [SerializeField] public MonsterClass Class;
        [SerializeField] public SNColor Color;
        [SerializeField] public Drawable View;
        

        /// <summary>
        /// The type of the monster, used for construction.
        /// </summary>
        [SerializeField] public Type Type;
        /// <summary>
        /// How much experience the monster gives.
        /// </summary>
        [SerializeField] public int Experience;
        /// <summary>
        /// What the monster is resistant to?
        /// </summary>
        [SerializeField] public EffectType[] Resistances;
        [SerializeField] public int AC;
        private IEnumerable<PropertyInfoForDisplay> _propertyInformation;
        public IEnumerable<PropertyInfoForDisplay> PropertyInformation => _propertyInformation ?? (_propertyInformation = this.GetPropertyInformationForDisplay());
    }
}