using System;
using Assets.Code.Dungeon;
using Assets.Code.MathHelpers;
using Assets.Code.SlashNet.Combat;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Factories;
using Assets.Code.SlashNet.Factories.Specifications;
using Assets.SlashNet.Common;
using Assets.SlashNet.Dungeon;
using SlashNet;

namespace Assets.Code.SlashNet.Monsters.Concrete
{
    public abstract class Monster : Being, IMonster
    {
        private double _experience;
        private string _name;
        private double _gold;
        private bool _attacked;
        private readonly IDungeonWalker _monsterDungeonWalker;

        protected Monster(MonsterTemplate template, double level)
        {

            Wield(WeaponBuilder.Configure(WeaponSpecifications.MonsterWeaponDefault).Build());

            SetUpDefaults(template);
            
            AdjustLevel(level);

            _monsterDungeonWalker = new MonsterDungeonWalker(this);
        }

        private void SetUpDefaults(MonsterTemplate template)
        {
            _name = template.Name;
            View = template.View;
        }
        
        private void AdjustLevel(double level)
        {
            Wield(WeaponBuilder.Configure(weaponConfiguration => weaponConfiguration
                .SetName("Monster")
                .SetWeight(5)
                .SetAttacks(AttackSpecifications.DefaultBite)
                .SetStatistics(statsConfig => statsConfig
                    .AddStatistic(statConfig => statConfig
                        .SetValue(IdleMath.GetMonsterDie(1, level, 1.2f))
                    )
                )
            ).Build());

            Wear(ArmorBuilder.Configure(armorConfiguration => armorConfiguration
                .SetName("Monster")
                .SetWeight(5)
                .SetStatistics(statsBuilder => statsBuilder
                    .AddStatistic(statConfig => statConfig
                        .SetValue(IdleMath.GetMonsterDie(1, level, 1.12f))
                    )
                )
            ).Build());

            _experience = IdleMath.GetPlayerDie(1, level);
            MaxHealth = IdleMath.GetMonsterDie(1, level, 2.1f);
            _gold = IdleMath.GetPlayerDie(1, level*1.25);
            Health = MaxHealth;
        }

        public void Move() =>
            _monsterDungeonWalker.Walk();
        

        public override double GetLoot()
        {
            var temp = _gold;
            _gold = 0;
            return temp;
        }

        public override double Health
        {
            get;
            protected set;
        }

        public override double MaxHealth
        {
            get;
            protected set;
        }

        public Tile Home { get; set; }
        public int Level { get; protected set; }
        public Drawable View { get; private set; }

        public override ICombatResult OnAttacks(IBeing defender)
        {
            var combatResult = CombatManager.InitiateCombat(this, defender);
            
            return combatResult;

        }
        public override ICombatResult OnAttacked(ICombatResult combatResult)
        {
            if (combatResult.Damage == 0)
                combatResult.Damage = 1;

            if (this.LoseHP(combatResult.Damage))
            {
                SlashNetGame.Instance.Player.Experience += _experience;
                SlashNetGame.Instance.Dungeon.Remove(this);
                combatResult.Killed = true;
            }
            else
            {
                combatResult.Killed = false;
            }

            return combatResult;
        }

        public override string ToString()
        {
            return _name;
        }

    }
}