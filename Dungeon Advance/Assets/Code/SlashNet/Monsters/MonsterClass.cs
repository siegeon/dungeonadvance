namespace Assets.Code.SlashNet.Monsters
{
    public enum MonsterClass
    {
        Insect, Blob, Cockatrice, Canine, SphereEye, Feline, GremlinGargoyle, Humanoid, MinorDemon, Jelly,
        Kobold, Leprechaun, Mimic, Nymph, Orc, Piercer, Quadruped, Rodent, Arachnid, Lurker, Equine, Vortex,
        Worm, FantasticalInsect, Light, Zruty, AngelicBeing, BatBird, Centaur, Dragon, ElementalStalker,
        Fungi, Gnome, LargeHumanoid, Jabberwock, KeystoneKop, Lich, Mummy, Naga, Ogre, Amoeboid, QuantumMechanic,
        RustDisenchanter, Snake, Troll, UmberHulk, Vampire, Wraith, Xorn, Apelike, Zombie, Golem, HumanElf, GhostShade,
        MajorDemon, SeaMonster, Lizard, QuestLeader, QuestNemesis, QuestGuardian
    }
}