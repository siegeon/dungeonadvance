﻿using Assets.Code.SlashNet.Common;
using Assets.SlashNet.Dungeon;
using SlashNet;

namespace Assets.Code.SlashNet.Monsters
{
    public interface IMonster : IBeing
    {
        Tile Home { get; set; }
        int Level { get; }
        Drawable View { get; }
        string ToString();
    }
}