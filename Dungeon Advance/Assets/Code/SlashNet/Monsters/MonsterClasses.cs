using System;
using System.Collections.Generic;
using Assets.Code.SlashNet.Monsters;

namespace Assets.SlashNet.Monsters
{
    public class MonsterClassDictionary
    {
        private static readonly Lazy<MonsterClassDictionary> Lazy = new Lazy<MonsterClassDictionary>(() => new MonsterClassDictionary());

        public static MonsterClassDictionary Instance => Lazy.Value;

        private MonsterClassDictionary()
        {
        }

        private readonly Dictionary<MonsterClass, string> _data = new Dictionary<MonsterClass, string>
        {
            {MonsterClass.Amoeboid, "P"},
            {MonsterClass.AngelicBeing, "A"},
            {MonsterClass.Apelike, "Y"},
            {MonsterClass.Arachnid, "s"},
            {MonsterClass.BatBird, "B"},
            {MonsterClass.Blob, "b"},
            {MonsterClass.Canine, "d"},
            {MonsterClass.Centaur, "C"},
            {MonsterClass.Cockatrice, "c"},
            {MonsterClass.Dragon, "D"},
            {MonsterClass.ElementalStalker, "E"},
            {MonsterClass.Equine, "u"},
            {MonsterClass.FantasticalInsect, "x"},
            {MonsterClass.Feline, "f"},
            {MonsterClass.Fungi, "F"},
            {MonsterClass.GhostShade, " "},
            {MonsterClass.Gnome, "G"},
            {MonsterClass.Golem, "'"},
            {MonsterClass.GremlinGargoyle, "g"},
            {MonsterClass.HumanElf, "@"},
            {MonsterClass.Humanoid, "h"},
            {MonsterClass.Insect, "a"},
            {MonsterClass.Jabberwock, "J"},
            {MonsterClass.Jelly, "j"},
            {MonsterClass.KeystoneKop, "K"},
            {MonsterClass.Kobold, "k"},
            {MonsterClass.LargeHumanoid, "H"},
            {MonsterClass.Leprechaun, "l"},
            {MonsterClass.Lich, "L"},
            {MonsterClass.Light, "y"},
            {MonsterClass.Lizard, ":"},
            {MonsterClass.Lurker, "t"},
            {MonsterClass.MajorDemon, "&"},
            {MonsterClass.Mimic, "m"},
            {MonsterClass.MinorDemon, "i"},
            {MonsterClass.Mummy, "M"},
            {MonsterClass.Naga, "N"},
            {MonsterClass.Nymph, "n"},
            {MonsterClass.Ogre, "O"},
            {MonsterClass.Orc, "o"},
            {MonsterClass.Piercer, "p"},
            {MonsterClass.Quadruped, "q"},
            {MonsterClass.QuantumMechanic, "Q"},
            {MonsterClass.QuestGuardian, "@"},
            {MonsterClass.QuestLeader, "@"},
            {MonsterClass.QuestNemesis, null},
            {MonsterClass.Rodent, "r"},
            {MonsterClass.RustDisenchanter, "R"},
            {MonsterClass.SeaMonster, ";"},
            {MonsterClass.Snake, "S"},
            {MonsterClass.SphereEye, "e"},
            {MonsterClass.Troll, "T"},
            {MonsterClass.UmberHulk, "U"},
            {MonsterClass.Vampire, "V"},
            {MonsterClass.Vortex, "v"},
            {MonsterClass.Worm, "w"},
            {MonsterClass.Wraith, "W"},
            {MonsterClass.Xorn, "X"},
            {MonsterClass.Zombie, "Z"},
            {MonsterClass.Zruty, "z"}
        };


        public string this[MonsterClass c] => _data[c];
    }
}