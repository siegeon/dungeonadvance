using Assets.Code.SlashNet.Monsters.Concrete;

namespace Assets.Code.SlashNet.Monsters.Types
{
    public class SimpleMonster : Monster
    {
        public SimpleMonster(MonsterTemplate template, double level)
            : base(template, level)
        {
        }
    }
}