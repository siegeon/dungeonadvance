using System;
using System.Collections.Generic;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Items.Materials;
using Assets.Code.SlashNet.Items.Weapons;
using Assets.Code.Utilities;
using Assets.SlashNet.Common;
using SlashNet;
using SlashNet.Items;

namespace Assets.Code.SlashNet.Items
{
    public abstract class Item : Entity, IItem, IEquatable<Item>
    {
        protected int Weight { get; }
        protected internal string Name { get; }
        protected List<EffectType> Resistances = new List<EffectType>();
        protected AMaterial Material;

        internal virtual int ToHit(Being versus)
        {
            var tmp = 0;
            if (Material != null)
                tmp += Material.ToHitBonus(versus);
            return tmp;
        }

        public void ApplyEffect(EffectType type)
        {
            Material?.ApplyEffect(type);
        }

        protected Item(int weight, string name) : this(Guid.Empty, weight, name) { }

        protected Item(Guid id, int weight, string name)
        {
            weight.GuardObjectNull(nameof(weight));
            name.GuardString(nameof(name));

            Id = id;
            Weight = weight;
            Name = name;
        }

        public virtual void SetResistance(EffectType effect, bool set)
        {
            if (set)
                Resistances.Add(effect);
            else
                Resistances.Remove(effect);
        }

        public virtual bool Resists(EffectType effect)
        {
            return Resistances.Contains(effect);
        }

        public abstract ItemType Type { get; }

        public override string ToString()
        {
            return Name;
        }

        public bool Equals(Item other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Material, other.Material) && Weight == other.Weight && string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Item) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Material != null ? Material.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Weight;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Item left, Item right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Item left, Item right)
        {
            return !Equals(left, right);
        }
    }
}