using SlashNet;
using SlashNet.Items;

namespace Assets.Code.SlashNet.Items
{
    public class Gem : Item
    {
        public Gem(string color) : base(10, color + " gem")
        {

            SNColor c = new SNColor();
            switch (color)
            {
                case "green":
                    c = SNColor.Green;
                    break;
                case "red":
                    c = SNColor.Red;
                    break;
                case "orange":
                    c = SNColor.Orange;
                    break;
            }
        }


        public override ItemType Type => ItemType.Gem;
    }
}
