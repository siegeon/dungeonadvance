using Assets.Code.SlashNet.Common;
using SlashNet;

namespace Assets.Code.SlashNet.Items.Materials
{
    class Iron : AMaterial
    {
        #region IMaterial Members

        private int _Rusty = 0;
        private int _Corroded = 0;

        internal Iron(Item owner)
            : base(owner)
        {
        }

        internal override void ApplyEffect(EffectType e)
        {
            switch (e)
            {
                case EffectType.Water:
                case EffectType.Rust:
                case EffectType.Acid:
                    break;
            }
        }

        internal override int ToHitBonus(IBeing versus)
        {
            return -_Rusty - _Corroded;
        }

        internal override int DamageBonus(IBeing versus)
        {
            return -_Rusty - _Corroded;
        }

        public override string ToString()
        {
            string tmp = "";
            if (_Rusty > 0)
                tmp += MaterialGrammar.GetFromLevel(_Rusty) + " rusty";

            if (tmp != "")
                tmp += " ";

            if (_Corroded > 0)
                tmp += MaterialGrammar.GetFromLevel(_Corroded) + " corroded";

            if (_Item.Resists(EffectType.Rust))
                return "rustproof";
            
            return tmp;
        }

        #endregion
    }
}
