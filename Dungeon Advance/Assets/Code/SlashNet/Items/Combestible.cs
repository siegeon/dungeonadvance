
using SlashNet.Items;

namespace Assets.Code.SlashNet.Items
{
    public class Combestible : Item
    {

        public Combestible(int weight, string name) : 
            base(weight, name)
        {
        }

        public override ItemType Type => ItemType.Combestible;
    }
}
