using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Character_Stats;
using Assets.Code.SlashNet.Combat.Attacks;
using Assets.Code.Utilities;
using SlashNet.Items;

namespace Assets.Code.SlashNet.Items.Weapons
{
    public class Weapon : Item, IWeapon
    {
        public Statistics Statistics { get; }
        public Attacks Attacks { get; }

        public Weapon(WeaponModel model) : base(model.Id, model.Weight, model.Name)
        {
            model.GuardObjectNull(nameof(model));

            Statistics = new Statistics(model.StatisticsModel);
            Attacks = new Attacks(model.AttacksModel);
            WeaponType = model.WeaponType;
        }
          
        public WeaponModel ToModel => new WeaponModel
        {
            AttacksModel = Attacks.ToModel,
            Id = Id,
            WeaponType = WeaponType,
            Name = Name,
            StatisticsModel = Statistics.ToModel,
            Weight = Weight
        };

        public override ItemType Type => ItemType.Weapon;

        public WeaponType WeaponType { get; }
        public double Damage => Statistics.Value;// + Attacks.Damage;
    }
}