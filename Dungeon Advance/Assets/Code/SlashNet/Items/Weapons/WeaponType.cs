
namespace Assets.Code.SlashNet.Items.Weapons
{
    public enum WeaponType
    {
        LongSword, ShortSword, Dagger, Physical, Sword
    }

    public enum ArmorType
    {
        Head, Shoulder, Back, Chest, Wrist, Hands, Waist, Legs, Feet
    }
}
