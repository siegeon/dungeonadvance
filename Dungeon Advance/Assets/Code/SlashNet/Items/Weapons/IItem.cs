using Assets.Code.DataAccess.Interfaces;

namespace Assets.Code.SlashNet.Items.Weapons
{
    /// <summary> An item is an entity, it can be saved and loaded </summary>
    public interface IItem : IEntity
    {
    }
}