using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Combat.Attacks;

namespace Assets.Code.SlashNet.Items.Weapons
{
    public interface IWeapon :  IItem
    {
        Attacks Attacks { get; }
        WeaponModel ToModel { get; }
    }
}