using System;
using Assets.Code.MVC.Models;

namespace Assets.Code.SlashNet.Items.Armor
{
    interface IArmor
    {
        double DamageReduction { get; }
        ArmorModel ToModel { get; }
    }
}