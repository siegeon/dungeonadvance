using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Character_Stats;
using Assets.Code.SlashNet.Items.Weapons;
using Assets.Code.Utilities;
using SlashNet.Items;

namespace Assets.Code.SlashNet.Items.Armor
{
    public class Armor : Item, IArmor
    {
        public Statistics Statistics { get; }

        public Armor(ArmorModel model) : base(model.Id, model.Weight, model.Name)
        {
            model.GuardObjectNull(nameof(model));

            Statistics = new Statistics(model.StatisticsModel);
            ArmorType = model.ArmorType;
        }

        public override ItemType Type => ItemType.Armor;
        public ArmorType ArmorType { get; }
        public double DamageReduction => Statistics.Value;

        public ArmorModel ToModel => new ArmorModel
        {
            Id = Id,
            Name = Name,
            StatisticsModel = Statistics.ToModel,
            ArmorType = ArmorType,
            Weight = Weight
        };
    }
}