using System;
using Assets.Code.SlashNet.Character_Stats;

namespace Assets.Code.SlashNet.Factories.Specifications
{
    public class StatisticSpecifications
    {
        public static Action<StatisticBuilder> DefaultWeapon =>
            spec => spec
                .SetCost(4)
                .SetMultiplyer(1.07)
                .SetStatisticType(StatisticTypes.Physical)
                .SetValue(1);

        public static Action<StatisticBuilder> DefaultArmor =>
            spec => spec
                .SetCost(4)
                .SetMultiplyer(1.05)
                .SetStatisticType(StatisticTypes.Physical)
                .SetValue(1);
    }
}