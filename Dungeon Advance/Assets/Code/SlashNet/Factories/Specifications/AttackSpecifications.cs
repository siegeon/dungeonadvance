using System;
using SlashNet;

namespace Assets.Code.SlashNet.Factories.Specifications
{
    public class AttackSpecifications
    {
        public static Action<AttackBuilder> DefaultSword =>
            spec => spec
                .SetDamage(6)
                .SetEffectType(EffectType.Physical);

        public static Action<AttackBuilder> DefaultBite =>
            spec => spec
                .SetDamage(2)
                .SetEffectType(EffectType.Physical);
    }
}