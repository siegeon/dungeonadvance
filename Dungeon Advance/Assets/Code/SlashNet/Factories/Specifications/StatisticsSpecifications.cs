using System;

namespace Assets.Code.SlashNet.Factories.Specifications
{
    public class StatisticsSpecifications
    {
        public static Action<StatisticsBuilder> DefaultWeapon =>
            spec => spec.AddStatistic(StatisticSpecifications.DefaultWeapon);

        public static Action<StatisticsBuilder> DefaultArmor =>
            spec => spec.AddStatistic(StatisticSpecifications.DefaultArmor);
    }
}