using System;
using Assets.Code.SlashNet.Items.Weapons;
using SlashNet.Items;

namespace Assets.Code.SlashNet.Factories.Specifications
{
    public class WeaponSpecifications
    {
        public static Action<WeaponBuilder> PlayerWeaponDefault => 
            spec => spec
                .SetAttacks(AttackSpecifications.DefaultSword)
                .SetStatistics(StatisticsSpecifications.DefaultWeapon)
                .SetWeaponType(WeaponType.Sword)
                .SetName("Sword")
                .SetWeight(10);

        public static Action<WeaponBuilder> MonsterWeaponDefault =>
            spec => spec
                .SetAttacks(AttackSpecifications.DefaultBite)
                .SetStatistics(StatisticsSpecifications.DefaultWeapon)
                .SetWeaponType(WeaponType.Dagger)
                .SetName("claw")
                .SetWeight(10);
    }
}