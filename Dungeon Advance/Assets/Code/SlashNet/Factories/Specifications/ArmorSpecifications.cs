using System;
using Assets.Code.SlashNet.Items.Weapons;

namespace Assets.Code.SlashNet.Factories.Specifications
{
    public class ArmorSpecifications
    {
        public static Action<ArmorBuilder> Chest =>
            spec => spec
                .SetStatistics(StatisticsSpecifications.DefaultArmor)
                .SetArmorType(ArmorType.Chest)
                .SetWeight(10)
                .SetName("Chest");
    }
}