using System;

namespace Assets.Code.SlashNet.Factories.Specifications
{
    public class PlayerSpecifications
    {
        public static Action<PlayerBuilder> Default =>
            spec => spec
                .SetName("siegeon")
                .SetLevel(1)
                .SetDungeonLevel(1)
                .SetHealth(100)
                .SetExp(1)
                .SetExpToNextLevel(25)
                .SetWeapon(WeaponSpecifications.PlayerWeaponDefault)
                .AddArmor(ArmorSpecifications.Chest);
    }
}