using System;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Character_Stats;

namespace Assets.Code.SlashNet.Factories
{
    public class StatisticBuilder
    {
        public static StatisticBuilder Configure(StatisticModel statisticModel) => new StatisticBuilder(statisticModel);

        public static StatisticBuilder Configure(Action<StatisticBuilder> specification)
        {
            var builder = new StatisticBuilder(new StatisticModel());
            specification(builder);
            return builder;
        }

        private readonly StatisticModel _statisticModel;

        private StatisticBuilder(StatisticModel statisticModel)
        {
            _statisticModel = statisticModel;
        }

        public StatisticBuilder SetCost(double cost)
        {
            _statisticModel.Cost = cost;
            return this;
        }

        public StatisticBuilder SetMultiplyer(double multiplyer)
        {
            _statisticModel.Multiplyer = multiplyer;
            return this;
        }

        public StatisticBuilder SetValue(double value)
        {
            _statisticModel.Value = value;
            return this;
        }

        public StatisticBuilder SetStatisticType(StatisticTypes type)
        {
            _statisticModel.Type = type;
            return this;
        }

        public Statistic Build => new Statistic(_statisticModel);
    }
}