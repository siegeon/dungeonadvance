using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Items.Weapons;
using Assets.Code.Utilities;

namespace Assets.Code.SlashNet.Factories
{


    public class WeaponBuilder : ItemBuilder<Weapon, WeaponBuilder>
    {
        private readonly WeaponModel _weaponModel;
        
        public static WeaponBuilder Configure(WeaponModel model) => new WeaponBuilder(model);
        public static WeaponBuilder Configure(Action<WeaponBuilder> spec)
        {
            var builder = Configure(new WeaponModel());
            spec(builder);
            return builder;
        }
        
        private WeaponBuilder(WeaponModel model)
        {
            _weaponModel = model;
        }
        
        public WeaponBuilder SetWeaponType(WeaponType weaponType)
        {
            weaponType.GuardEnum(nameof(weaponType));
            _weaponModel.WeaponType = weaponType;
            return this;
        }
        
        public WeaponBuilder SetAttacks(Action<AttackBuilder> attackSpecification)
        {
            attackSpecification.GuardObjectNull(nameof(attackSpecification));

            var attackBuilder = AttackBuilder.Configure(attackSpecification);
            
            _weaponModel.AttacksModel = new AttacksModel
            {
                AttackModels = new List<AttackModel>
                {
                    attackBuilder.Build.ToModel
                }
            };
            return this;
        }

        public WeaponBuilder SetStatistics(Action<StatisticsBuilder> statisticsSpecification)
        {
            statisticsSpecification.GuardObjectNull(nameof(statisticsSpecification));

            _weaponModel.StatisticsModel = StatisticsBuilder.Configure(statisticsSpecification).Build.ToModel;

            return this;
        }

        public override Weapon Build() => new Weapon(new WeaponModel
        {
            Id = _weaponModel.Id,
            Name = Name,
            Weight = Weight,
            StatisticsModel = _weaponModel.StatisticsModel,
            WeaponType = _weaponModel.WeaponType,
            AttacksModel = _weaponModel.AttacksModel
        });
    }
}