using Assets.Code.SlashNet.Items;
using Assets.Code.Utilities;

namespace Assets.Code.SlashNet.Factories
{
    public abstract class ItemBuilder<TItem, TBuilder> where TItem : Item
        where TBuilder : ItemBuilder<TItem, TBuilder>
    {
        private readonly TBuilder _builder;

        protected string Name = "";
        protected int Weight = 1;

        protected ItemBuilder()
        {
            _builder = (TBuilder) this;
        }

        public TBuilder SetName(string name)
        {
            name.GuardString(nameof(name));
            Name = name;
            return _builder;
        }

        public TBuilder SetWeight(int weight)
        {
            weight.GuardObjectNull(nameof(weight));
            Weight = weight;
            return _builder;
        }


        public abstract TItem Build();
    }
}