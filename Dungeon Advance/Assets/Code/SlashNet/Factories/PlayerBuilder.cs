using System;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Factories.Specifications;
using Assets.Code.SlashNet.Items.Armor;
using Assets.Code.SlashNet.Items.Weapons;
using Assets.Code.SlashNet.Player;
using Assets.Code.Utilities;

namespace Assets.Code.SlashNet.Factories
{
    public class PlayerBuilder
    {
        private readonly PlayerModel _playerModel;
        private Weapon _weapon;
        private Armor _armor;

        public static PlayerBuilder Configure(PlayerModel model) => new PlayerBuilder(model);
        public static PlayerBuilder Configure(Action<PlayerBuilder> spec)
        {
            var playerBuilder = new PlayerBuilder(new PlayerModel());
            spec(playerBuilder);
            return playerBuilder;
        }

        private PlayerBuilder(PlayerModel playerModel)
        {
            _playerModel = playerModel;
        }

        public PlayerBuilder SetName(string name)
        {
            _playerModel.Name = name;
            return this;
        }

        public PlayerBuilder SetLevel(int level)
        {
            _playerModel.Level = level;
            return this;
        }

        public PlayerBuilder SetHealth(int health)
        {
            _playerModel.CurrentHealth = health;
            _playerModel.MaxHealth = health;
            return this;
        }

        public PlayerBuilder SetExp(int exp)
        {
            _playerModel.Exp = exp;
            return this;
        }

        public PlayerBuilder SetExpToNextLevel(int exp)
        {
            _playerModel.ExpToNextLevel = exp;
            return this;
        }

        public PlayerBuilder SetDungeonLevel(int dungeonLevel)
        {
            _playerModel.DungeonLevel = dungeonLevel;
            return this;
        }

        public PlayerBuilder SetWeapon(Action<WeaponBuilder> weaponSpecification)
        {
            weaponSpecification.GuardObjectNull(nameof(weaponSpecification));

            var weaponbuilder = WeaponBuilder.Configure(weaponSpecification);

            _weapon = weaponbuilder.Build();

            return this;
        }

        public PlayerCharacter Build => new PlayerCharacter(_playerModel, _weapon, _armor);


        public PlayerBuilder AddArmor(Action<ArmorBuilder> armorSpecification)
        {
            armorSpecification.GuardObjectNull(nameof(armorSpecification));
            var armorBuilder = ArmorBuilder.Configure(armorSpecification);
            _armor = armorBuilder.Build();
            return this;
        }
    }
}