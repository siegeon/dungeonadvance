﻿using System;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Items.Armor;
using Assets.Code.SlashNet.Items.Weapons;
using Assets.Code.Utilities;

namespace Assets.Code.SlashNet.Factories
{
    public class ArmorBuilder : ItemBuilder<Armor, ArmorBuilder>
    {
        private readonly ArmorModel _armorModel;
        public static ArmorBuilder Configure(ArmorModel model) => new ArmorBuilder(model);
        public static ArmorBuilder Configure(Action<ArmorBuilder> spec)
        {
            var builder = Configure(new ArmorModel());
            spec(builder);
            return builder;
        }

        private ArmorBuilder(ArmorModel armorModel)
        {
            _armorModel = armorModel;
        }

        public ArmorBuilder SetArmorType(ArmorType armorType)
        {
            armorType.GuardEnum(nameof(ArmorType));
            _armorModel.ArmorType = armorType;
            return this;
        }


        public ArmorBuilder SetStatistics(Action<StatisticsBuilder> statisticsSpecification)
        {
            statisticsSpecification.GuardObjectNull(nameof(statisticsSpecification));

            _armorModel.StatisticsModel = StatisticsBuilder.Configure(statisticsSpecification).Build.ToModel;

            return this;
        }

        public override Armor Build() => new Armor(new ArmorModel
        {
            Id = _armorModel.Id,
            ArmorType = _armorModel.ArmorType,
            Name = Name,
            Weight = Weight,
            StatisticsModel = _armorModel.StatisticsModel
        });
    }
}