using System;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Combat.Attacks;
using Assets.Code.Utilities;
using SlashNet;

namespace Assets.Code.SlashNet.Factories
{
    /// <summary> A build class that allows for the creation and configurations of attacks. </summary>
    public class AttackBuilder
    {
        private readonly AttackModel _attackModel;

        public static AttackBuilder Configure(Action<AttackBuilder> attackSpecification)
        {
            var attackBuilder = new AttackBuilder(new AttackModel());
            attackSpecification(attackBuilder);
            return attackBuilder;
        }
        
        private AttackBuilder(AttackModel attackModel)
        {
            attackModel.GuardObjectNull(nameof(attackModel));
            _attackModel = attackModel;
        }

        public AttackBuilder SetDamage(int sides)
        {
            sides.GuardObjectNull(nameof(sides));
            _attackModel.Sides = sides;
            return this;
        }

        public AttackBuilder SetEffectType(EffectType effectType)
        {
            effectType.GuardEnum(nameof(effectType));
            _attackModel.EffectType = effectType;
            return this;
        }

        public Attack Build => new Attack(_attackModel);
        
    }
}