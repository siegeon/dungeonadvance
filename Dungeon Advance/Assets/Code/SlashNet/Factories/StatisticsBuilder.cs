using System;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Character_Stats;
using Assets.Code.Utilities;

namespace Assets.Code.SlashNet.Factories
{
    public class StatisticsBuilder
    {
        private StatisticsModel _statisticsModel;

        public static StatisticsBuilder Configure(StatisticsModel statisticsModel) => new StatisticsBuilder(statisticsModel);

        public static StatisticsBuilder Configure(Action<StatisticsBuilder> specification)
        {
            var builder = new StatisticsBuilder(new StatisticsModel());
            specification(builder);
            return builder;
        }

        private StatisticsBuilder(StatisticsModel statisticsModel)
        {
            _statisticsModel = statisticsModel;
        }

        public StatisticsBuilder AddStatistic(Action<StatisticBuilder> statsticSpecification)
        {
            statsticSpecification.GuardObjectNull(nameof(statsticSpecification));

            var model = StatisticBuilder.Configure(statsticSpecification).Build.ToModel;

            _statisticsModel.StatisticModels.Add(model);
            return this;
        }

        public Statistics Build => new Statistics(_statisticsModel);
    }
}