using System;
using Assets.Code.DataAccess;
using Assets.Code.SlashNet.Combat;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Dungeon;
using Assets.Code.SlashNet.Factories;
using Assets.Code.SlashNet.Factories.Specifications;
using Assets.Code.SlashNet.Monsters.Concrete;
using Assets.Code.SlashNet.Player;
using Assets.Dungeon;
using Assets.Structures;
using SlashNet;
using SlashNet.Dungeon;
using SlashNet.Dungeon.FOV;
using SlashNet.Dungeon.Generation;

namespace Assets.Code.SlashNet
{
    public class SlashNetGame
    {
        public static readonly Lazy<SlashNetGame> Lazy = new Lazy<SlashNetGame>(() => new SlashNetGame());
        public static SlashNetGame Instance => Lazy.Value;

        /// <summary> Constructor that prevents a default instance of this class from being created.  </summary>
        private SlashNetGame()
        {
            _dungeonPopulator = new DefaultDungeonPopulator();
            Player = DataServices.PlayerRepository.GetFirst().Case(
                some: playerModel => playerModel,
                none: () => PlayerBuilder.Configure(PlayerSpecifications.Default).Build);
        }

        #region Static

        public Action<ICombatResult> DealWithCombatResult { get; set; }

        public PlayerCharacter Player { get; private set; }

        #endregion

        #region Fields

        private IFOVCalculator _FOV;
        private ISlashNetUI _ui;

        #endregion

        /// <summary> Creates a new dungeon.  </summary>
        public void NewDungeon() => NewDungeon(DungeonStyle.Crawl, 10, 70, 1, 40, 40);
        private void NewDungeon(DungeonStyle style, int dirMod, int sparseMod, int deadMod, int width, int height)
        {
            Dungeon = Generator
                .Generate(width, height, dirMod, sparseMod, deadMod, global::SlashNet.Random.Next(5, 10), 2, 5, 2, 5)
                .ExpandToDungeon(style);
            Player.Location = Dungeon.Start;
            _dungeonPopulator.Populate(Dungeon);
        }

        #region Controller

        public void Register(ISlashNetUI ui)
        {
            _ui = ui;
        }

        public bool MoveTo(Point n)
        {
            if (_InventoryActive)
                return false;

            GetTowards(n);

            if (!Dungeon.Contains(n) || Dungeon.IsObstacle(n))
                return false;

            if (Dungeon[n].Monster != null)
            {
                ManageCombat(Player, Dungeon[n].Monster);
                EndTurn();
                return false;
            }

            Player.Location = n;
            Dungeon.Visit(n);
            EndTurn();

            return false;
        }

        private void ManageCombat(IBeing attacker, IBeing defender)
        {

            var attack1 = attacker.OnAttacks(defender);
            var attack2 = defender.OnAttacks(attacker);
            DealWithCombatResult.Invoke(attack1);
            DealWithCombatResult.Invoke(attack2);
        }

        private void EndTurn()
        {
            foreach (Monster monster in Dungeon)
            {
                monster.Move();
            }
        }

        private Point GetTowards(Point p)
        {
            return new Point(p.X - Player.Location.X, p.Y - Player.Location.Y);
        }

        #endregion

        #region Properties

        public void LogMessage(string message)
        {
            //UnityEngine.Debug.Log(message);
            //_ui?.Pline(message);
        }

        public IDungeon Dungeon { get; private set; }

        #endregion

        private bool _InventoryActive = false;
        private readonly DefaultDungeonPopulator _dungeonPopulator;

        public ItemStack Inventory => Player.Inventory;
        public bool PopupOpen { get; set; }

        public void CombatResultCallback(Action<ICombatResult> dealWithCombatResult)
        {
            DealWithCombatResult = dealWithCombatResult;
        }

        public void SavePlayer() => DataServices.PlayerRepository.AddOrUpdate(Player);

        public static void Reset()
        {
            var playerId = SlashNetGame.Instance.Player.Id;
            var weaponId = SlashNetGame.Instance.Player.Wielded.Id;
            var armorId = SlashNetGame.Instance.Player.Worn.Id;

            Instance.Player = PlayerBuilder.Configure(PlayerSpecifications.Default).Build;
            Instance.Player.Id = playerId;
            Instance.Player.Wielded.Id = weaponId;
            Instance.Player.Worn.Id = armorId;

            Instance.SavePlayer();
        }
    }
}
