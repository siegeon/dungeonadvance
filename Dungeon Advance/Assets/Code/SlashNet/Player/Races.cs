using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Player
{
    public class Races
    {
        private static readonly Lazy<Races> Lazy = new Lazy<Races>(() => new Races());
        public static Races Instance => Lazy.Value;
        private Races() { }

        public Race this[string id] => _data[id];

        private readonly Dictionary<string, Race> _data = new Dictionary<string, Race>
        {
            {
                "h", new Race("human", "human", "humanity",
                    new int[] {3, 3, 3, 3, 3, 3},
                    new int[] {18, 18, 18, 18, 18, 18},
                    new Advancement(2, 0, 0, 2, 1, 0),
                    new Advancement(1, 0, 2, 0, 2, 0),
                    new EffectType[] { })
            }
        };
    }
}
