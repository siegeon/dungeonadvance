namespace SlashNet.Player
{
    public struct Advancement
    {
        internal int Infix, Inrnd;
        internal int Lofix, Lornd;
        internal int Hifix, Hirnd;

        public Advancement(int infix, int inrnd, int lofix, int lornd, int hifix, int hirnd)
        {
            Infix = infix;
            Inrnd = inrnd;
            Lofix = lofix;
            Lornd = lornd;
            Hifix = hifix;
            Hirnd = hirnd;
        }
    }
}