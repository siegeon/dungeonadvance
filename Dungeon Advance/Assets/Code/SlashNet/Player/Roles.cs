using System;
using System.Collections.Generic;
namespace SlashNet.Player
{

    public class Roles
    {
        private static readonly Lazy<Roles> Lazy = new Lazy<Roles>(() => new Roles());
        public static Roles Instance => Lazy.Value;
        private Roles() { }
        public Role this[string id] => _data[id];

        private readonly Dictionary<string, Role> _data =  new Dictionary<string, Role>
        {
            {
                "b", new Role("barbarian",
                    "Mitra", "Crom", "Set", 10,
                    new string[]
                    {
                        "Barbarian", "Plunderer", "Pillager", "Bandit",
                        "Raider", "Reaver", "Slayer", "Chieftain", "Conqueror"
                    },
                    new Advancement(14, 0, 0, 10, 2, 0),
                    new Advancement(1, 0, 0, 1, 0, 1),
                    new int[] {16, 7, 7, 15, 16, 6},
                    new int[] {30, 6, 7, 20, 30, 7},
                    new Dictionary<int, EffectType[]>
                    {
                        {1, new[] {EffectType.Poison}},
                        {7, new[] {EffectType.Speed}},
                        {15, new[] {EffectType.Stealth}}
                    })
            }
        };
    }
}