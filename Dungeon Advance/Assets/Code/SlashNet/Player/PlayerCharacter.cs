using System;
using System.Collections;
using Assets.Code.DataAccess;
using Assets.Code.MathHelpers;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Combat;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Factories;
using Assets.Code.SlashNet.Factories.Specifications;
using Assets.Code.SlashNet.Items.Armor;
using Assets.Code.SlashNet.Items.Weapons;
using Assets.SlashNet.Common;
using Random = SlashNet.Random;

namespace Assets.Code.SlashNet.Player
{
    public class PlayerCharacter : Being
    {
        public PlayerCharacter(PlayerModel model, Weapon weapon, Armor armor) :this(model)
        {
            Wield(weapon);
            Wear(armor);
        }

        public PlayerCharacter(PlayerModel model)
        {
            _model = model;
            Dead = false;

            Inventory = new ItemStack();

            FromModel(model);
        }

        public bool Dead { get; set; }

        public double Money
        {
            get { return _model.Money; }
            set { _model.Money = value; }
        }

        public PlayerModel ToModel
        {
            get
            {
                _model.WieldedId = Wielded.Id;
                _model.WornId = Worn.Id;
                _model.Id = Id;
                return _model;
            }
        }

        public override ICombatResult OnAttacked(ICombatResult combatResult)
        {
            if (LoseHP(combatResult.Damage))
                Dead = true;

            return combatResult;
        }

        public override ICombatResult OnAttacks(IBeing defender)
        {
            var combatResult = CombatManager.InitiateCombat(this, defender);

            if (combatResult.Killed)
                Money += defender.GetLoot();

            return combatResult;
        }

        public void ResetFromDeath()
        {
            var playerCharacter = DataServices.PlayerRepository.GetFirst().Case(
                player => player,
                () => PlayerBuilder.Configure(PlayerSpecifications.Default).Build);
            Dead = false;
            Health = MaxHealth;
            Experience = playerCharacter.Experience;
            ExperienceForNextLevel = playerCharacter.ExperienceForNextLevel;
            Money = playerCharacter.Money;
            Level = playerCharacter.Level;
            Wield(DataServices.WeapoRepository.Get(playerCharacter.Wielded.Id).Case(
                weapon => weapon,
                () => { throw new ArgumentException("Unable to reset players weapon after death"); }));
        }

        #region xp

        private double _attackBonus;

        private double _damageBonus;

        private PlayerModel _model;

        public double Experience
        {
            get { return _model.Exp; }
            set
            {
                _model.Exp = value;
                if (value >= ExperienceForNextLevel)
                    LevelUp();
            }
        }

        public double ExperienceForNextLevel { get { return _model.ExpToNextLevel; }
            set { _model.ExpToNextLevel = value; }
        }

        public double Level
        {
            get { return _model.Level; }
            protected set { _model.Level = value; }
        }

        internal void LevelUp()
        {
            Level+=1;
            MaxHealth = IdleMath.GetPlayerDie(50, Level);
            Health = MaxHealth;
            ExperienceForNextLevel += Experience * (float) Random.D(10, 1) / 10f + .1f;
            Experience = 0;
            _attackBonus = IdleMath.GetPlayerDie(3.5f, Level);
        }

        #endregion xp

        #region Being

        public override double Health
        {
            get { return _model.CurrentHealth; }
            protected set { _model.CurrentHealth = value; }
        }

        public override double MaxHealth
        {
            get { return _model.MaxHealth; }
            protected set { _model.MaxHealth = value; }
        }

        #endregion Being

        #region inventory

        public ItemStack Inventory { get; }

        #endregion inventory
        #region name

        public string Name
        {
            get { return _model.Name; }
            set { _model.Name = value; }
        }

        public double DungeonLevel {
            get { return _model.DungeonLevel; }
            set { _model.DungeonLevel = value; }
        }

        public float PercentToNextLevel => 1 - (float)(Experience / ExperienceForNextLevel);

        #endregion name

        public override double GetLoot()
        {
            var temp = _model.Money;
            _model.Money = 0;
            return temp;
        }

        private void FromModel(PlayerModel model)
        {
            Id = model.Id;
            _model = model;
            _attackBonus = IdleMath.GetPlayerDie(2, Level);
            _damageBonus = IdleMath.GetPlayerDie(2, Level);
        }

        public new void Wield(Weapon weapon)
        {
            Inventory.Add(weapon);
            base.Wield(weapon);
        }

        public new void Wear(Armor armor)
        {
            Inventory.Add(armor);
            base.Wear(armor);
        }

        public void ResetFromDungeonLevelUp()
        {
            DungeonLevel += 1;
            Health = MaxHealth;
        }
    }
}