﻿#if NETFX_CORE

#define TASK_AVAILABLE

using System.Threading.Tasks;

#endif

using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Assets.EZThread
{
    public class EzThread : MonoBehaviour
    {
        /// <summary>
        /// Encapsulates a fully running thread
        /// </summary>
        public class EzThreadRunner
        {
            private bool _running = true;
            private readonly Action _action;

            private void ThreadFunction()
            {
                while (_running)
                {
                    _action();
                }
            }

            private void ThreadFunctionSync()
            {
                while (_running)
                {
                    _action();
                    SyncEvent.WaitOne(100);
                }
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="action">Action</param>
            /// <param name="synchronizeWithUpdate">Whether to sync with the main thread update loop</param>
            internal EzThreadRunner(Action action, bool synchronizeWithUpdate)
            {
                _action = action;
                _running = true;
                if (synchronizeWithUpdate)
                {
                    SyncEvent = new System.Threading.AutoResetEvent(true);
                }

#if TASK_AVAILABLE

                if (synchronizeWithUpdate)
                {
                    Task.Factory.StartNew(ThreadFunctionSync);
                }
                else
                {
                    Task.Factory.StartNew(ThreadFunction);
                }

#else

                var w = synchronizeWithUpdate ? (s => 
                    ThreadFunctionSync()) : 
                    new WaitCallback(s => ThreadFunction());
                ThreadPool.QueueUserWorkItem(w);

#endif

            }

            /// <summary>
            /// Stop and remove the thread
            /// </summary>
            public void Stop()
            {
                _running = false;
            }

            public AutoResetEvent SyncEvent { get; private set; }
        }

        private static GameObject _singletonObject;
        private static EzThread _singleton;
        private readonly List<KeyValuePair<Action, float>> _mainThreadActions = new List<KeyValuePair<Action, float>>();
        private readonly Queue<KeyValuePair<object, Action<object>>> _mainThreadCompletions = new Queue<KeyValuePair<object, Action<object>>>();
        private readonly List<EzThreadRunner> _threads = new List<EzThreadRunner>();

        private static void EnsureCreated()
        {
            if (_singleton != null)
            {
                return;
            }

            _singletonObject = new GameObject("EZTHREAD")
            {
                hideFlags = HideFlags.HideAndDontSave
            };
            _singleton = _singletonObject.AddComponent<EzThread>();
            DontDestroyOnLoad(_singleton);

#if !TASK_AVAILABLE

            System.Threading.ThreadPool.SetMinThreads(Environment.ProcessorCount, Environment.ProcessorCount);
            System.Threading.ThreadPool.SetMaxThreads(Environment.ProcessorCount, Environment.ProcessorCount);

#endif

        }

        private static void InternalExecute(Func<object> a, Action<object> completion)
        {

#if TASK_AVAILABLE

            Task.Factory.StartNew(() =>
            {
                object result = a();
                if (completion != null)
                {
                    ExecuteOnMainThread(() => completion(result));
                }
            });

#else

            ThreadPool.QueueUserWorkItem((s) =>
            {
                var result = a();
                if (completion != null)
                {
                    ExecuteOnMainThread(() => completion(result));
                }
            });

#endif

        }

        private void RunMainThreadActions()
        {
            KeyValuePair<Action, float> a;
            KeyValuePair<object, Action<object>> c;
            int index = 0;

            while (true)
            {
                lock (_mainThreadActions)
                {
                    if (index >= _mainThreadActions.Count)
                    {
                        break;
                    }
                    a = _mainThreadActions[index];
                    float timeRemaining = a.Value - Time.deltaTime;
                    if (timeRemaining > 0.0f)
                    {
                        // update the entry with the new time remaining value
                        _mainThreadActions[index++] = new KeyValuePair<Action, float>(a.Key, timeRemaining);
                        continue;
                    }

                    // action is ready to be run, remove it
                    _mainThreadActions.RemoveAt(index);
                }
                a.Key();
            }
            while (true)
            {
                lock (_mainThreadCompletions)
                {
                    if (_mainThreadCompletions.Count == 0)
                    {
                        break;
                    }
                    c = _mainThreadCompletions.Dequeue();
                }
                c.Value(c.Key);
            }
        }

        private void NotifyThreads()
        {
            lock (_threads)
            {
                foreach (var thread in _threads)
                {
                    thread.SyncEvent?.Set();
                }
            }
        }

        private void Start()
        {
            UnityEngine.SceneManagement.SceneManager.sceneUnloaded += SceneManagerSceneUnloaded;
        }

        private void SceneManagerSceneUnloaded(UnityEngine.SceneManagement.Scene arg0)
        {
            Reset();
        }

        private void Update()
        {
            RunMainThreadActions();
            NotifyThreads();
        }

        private void OnDestroy()
        {
            Reset();
        }

        private void OnApplicationQuit()
        {
            Reset();
        }

        /// <summary>
        /// Reset all state - stop all threads, remove all queued actions, etc. Called automatically when the app quits or a new scene loads.
        /// </summary>
        public void Reset()
        {
            lock (_threads)
            {
                foreach (EzThreadRunner thread in _threads)
                {
                    thread.Stop();
                }
                _threads.Clear();
            }
            lock (_mainThreadActions)
            {
                _mainThreadActions.Clear();
            }
            lock (_mainThreadCompletions)
            {
                _mainThreadCompletions.Clear();
            }
        }

        /// <summary>
        /// Run an action on a background thread. The action only runs once.
        /// </summary>
        /// <param name="action">Action to run</param>
        public static void ExecuteInBackground(Action action)
        {
            EnsureCreated();
            InternalExecute(() =>
            {
                action();
                return null;
            }, null);
        }

        /// <summary>
        /// Run an action and return the result on a background thread. The action only runs once.
        /// </summary>
        /// <param name="action">Action</param>
        /// <param name="completion">Action to run on the main thread with the result of the action</param>
        public static void ExecuteInBackground(Func<object> action, Action<object> completion)
        {
            EnsureCreated();
            InternalExecute(action, completion);
        }

        /// <summary>
        /// Queue an action to run as soon as possible on the main thread. The action only runs once.
        /// </summary>
        /// <param name="action">Action to run</param>
        public static void ExecuteOnMainThread(Action action)
        {
            EnsureCreated();
            lock (_singleton._mainThreadActions)
            {
                _singleton._mainThreadActions.Add(new KeyValuePair<Action, float>(action, 0.0f));
            }
        }

        /// <summary>
        /// Queue an action to run after a delay on the main thread. The action only runs once.
        /// </summary>
        /// <param name="action">Action to run</param>
        /// <param name="delaySeconds">Delay in seconds before running action</param>
        public static void ExecuteOnMainThread(Action action, float delaySeconds)
        {
            EnsureCreated();
            lock (_singleton._mainThreadActions)
            {
                _singleton._mainThreadActions.Add(new KeyValuePair<Action, float>(action, delaySeconds));
            }
        }

        /// <summary>
        /// Begin running a thread. For short, one time background operations, Execute is usually a better choice.
        /// The specified action will be called over and over until EndThread is called.
        /// </summary>
        /// <param name="action">Action to run over and over</param>
        /// <returns>Running thread instance. Call EndThread to end the thread.</returns>
        public static EzThreadRunner BeginThread(Action action)
        {
            return BeginThread(action, true);
        }

        /// <summary>
        /// Begin running a thread. For short, one time background operations, Execute is usually a better choice.
        /// The specified action will be called over and over until EndThread is called.
        /// </summary>
        /// <param name="action">Action to run over and over</param>
        /// <param name="synchronizeWithUpdate">Whether to sync with Unitys update function. Generally you want this to be true.</param>
        /// <returns>Running thread instance. Call EndThread to end the thread.</returns>
        public static EzThreadRunner BeginThread(Action action, bool synchronizeWithUpdate)
        {
            EnsureCreated();
            EzThreadRunner thread = new EzThreadRunner(action, synchronizeWithUpdate);
            lock (_singleton._threads)
            {
                _singleton._threads.Add(thread);
            }
            return thread;
        }

        /// <summary>
        /// Remove a running thread
        /// </summary>
        /// <param name="thread">Thread to stop and remove</param>
        public static void EndThread(EzThreadRunner thread)
        {
            thread.Stop();
            lock (_singleton._threads)
            {
                _singleton._threads.Remove(thread);
            }
        }

        /// <summary>
        /// Singleton instance of EZThread script
        /// </summary>
        public EzThread Instance { get { return _singleton; } }
    }
}
