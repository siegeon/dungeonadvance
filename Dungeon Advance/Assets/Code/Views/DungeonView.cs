﻿using Assets.Code.Extensions;
using Assets.Code.MVC.Models;
using Assets.GUI.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.MVC.Views
{
    public class DungeonView : MonoBehaviour
    {
        public Text Coins;
        public Text Level;
        public Text DungeonLevel;
        public ProgressBar XpProgressBar;
        public ProgressBar HealthProgressBar;

        public double CurrentXp = 0;
        public double MaxXp = 1000;
        public double MaxHealth = 100;
        public double CurrentHealth = 100;

        void Start()
        {
            XpProgressBar = GameObjectExtensions.GetComponentFromGameObjectWithName<ProgressBar>("Progress Bar Xp");
            HealthProgressBar = GameObjectExtensions.GetComponentFromGameObjectWithName<ProgressBar>("Progress Bar Health");
            Level = GameObjectExtensions.GetComponentFromGameObjectWithName<Text>("Text Lv");
            DungeonLevel = GameObjectExtensions.GetComponentFromGameObjectWithName<Text>("TextDungeonLevel");
        }

        void OnGUI()
        {
            XpProgressBar.UpdateProgress((int)CurrentXp, (int)MaxXp);
            HealthProgressBar.UpdateProgress((int)CurrentHealth, (int)MaxHealth);
        }

        internal void UpdateModel(PlayerModel playerModel)
        {
            Coins.text = playerModel.MoneyText;
            CurrentXp = playerModel.Exp;
            MaxXp = playerModel.ExpToNextLevel;
            Level.text = playerModel.Level.ToString();
            CurrentHealth = playerModel.CurrentHealth;
            MaxHealth = playerModel.MaxHealth;
            DungeonLevel.text = playerModel.DungeonLevel.ToString();
        }
    }
}
