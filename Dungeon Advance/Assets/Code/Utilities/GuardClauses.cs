using System;

namespace Assets.Code.Utilities
{
    /// <summary> A collection of guard clauses used to validate paramaters in a uniform way. </summary>
    public static class GuardClauses
    {
        public static void GuardObjectNull(this object argumentValue, string argumentName)
        {
            if (argumentValue == null)
                throw new ArgumentNullException(argumentName);
        }

        public static void GuardString(this string argumentValue, string argumentName)
        {
            if(string.IsNullOrWhiteSpace(argumentValue))
                throw new ArgumentNullException(argumentName, "The string may not be null, empty, or whitespace.");
        }

        public static void GuardEnum<TEnum>(this TEnum argumentValue, string argumentName)
        {
            if(!Enum.IsDefined(typeof(TEnum), argumentValue))
                throw new ArgumentNullException(argumentName, $"The enum value {argumentValue} is not valid.");
        }

    }
}