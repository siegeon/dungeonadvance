﻿using System;
using UnityEngine;

namespace Assets.Utilities
{
    class ResourceLoader : ScriptableObject, IResourceLoader
    {
        public GameObject LoadManagerPrefab()
        {
            return LoadResource("Prefabs/ManagerPrefab");
        }

        private GameObject LoadResource(string resource)
        {
            var go = (GameObject)Instantiate(Resources.Load(resource));
            if(go == null)
                throw new ArgumentNullException($"{resource} was not found.");

            return go;
        }

    }
}
