﻿using UnityEngine;

namespace Assets.Utilities
{
    public interface IResourceLoader
    {
        GameObject LoadManagerPrefab();
    }
}