using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Editor.MasterData;

namespace BusinessTycoonSim
{
    public static class GenericExtensions
    {
        public static void SetValue<T>(this T item, PropertyInfoForDisplay info, object newPertyValue)
            where T : class
        {
            var propertyInfo = item.GetType().GetProperty(info.Name);
            if (propertyInfo != null)
                propertyInfo.SetValue(item, Convert.ChangeType(newPertyValue, info.Type), null);
        }

        public static object GetValue(this object item, string propertyName)
        {
            if (item == null)
                return null;

            var obj = item;

            // Split property name to parts (propertyName could be hierarchical, like obj.subobj.subobj.property
            var propertyNameParts = propertyName.Split('.');

            foreach (var propertyNamePart in propertyNameParts)
            {
                if (obj == null) return null;

                // propertyNamePart could contain reference to specific 
                // element (by index) inside a collection
                if (!propertyNamePart.Contains("["))
                {
                    var pi = obj.GetType().GetProperty(propertyNamePart);
                    if (pi == null) return null;
                    obj = pi.GetValue(obj, null);
                }
                else
                {
                    // propertyNamePart is areference to specific element 
                    // (by index) inside a collection
                    // like AggregatedCollection[123]
                    //   get collection name and element index
                    var indexStart = propertyNamePart.IndexOf("[") + 1;
                    var collectionPropertyName = propertyNamePart.Substring(0, indexStart - 1);
                    var collectionElementIndex =
                        int.Parse(propertyNamePart.Substring(indexStart, propertyNamePart.Length - indexStart - 1));
                    //   get collection object
                    var pi = obj.GetType().GetProperty(collectionPropertyName);
                    if (pi == null) return null;
                    var unknownCollection = pi.GetValue(obj, null);
                    //   try to process the collection as array
                    if (unknownCollection.GetType().IsArray)
                    {
                        object[] collectionAsArray = unknownCollection as Array[];
                        obj = collectionAsArray[collectionElementIndex];
                    }
                    else
                    {
                        //   try to process the collection as IList
                        var collectionAsList = unknownCollection as IList;
                        if (collectionAsList != null)
                        {
                            obj = collectionAsList[collectionElementIndex];
                        }
                    }
                }
            }

            return obj;
        }

        public static List<PropertyInfoForDisplay> GetPropertyInformationForDisplay<T1>(this T1 item)
        {
            var props = typeof(T1).GetProperties();
            var propInfos = new List<PropertyInfoForDisplay>();
            foreach (var prop in props)
                if (prop.PropertyType.IsGenericType)
                {
                    //propertyType = pi.PropertyType.GetGenericArguments()[0];
                    //var expression = GetMemberInfo(property);
                    //string path = string.Concat(expression.Member.DeclaringType.FullName,
                    //    ".", expression.Member.Name);
                }
                else
                {
                    var attribute = Attribute.GetCustomAttribute(prop, typeof(EditorAttribute)) as EditorAttribute;
                    attribute =  attribute ?? new EditorAttribute(0, 1);

                    propInfos.Add(new PropertyInfoForDisplay
                    {
                        Name = prop.Name,
                        Type = prop.PropertyType,
                        //Value = item.GetValue(prop.Name),
                        DrawOrder = attribute.Order,
                        RowHeight = attribute.Height
                    });
                }
            return propInfos.OrderBy(x => x.DrawOrder).ToList();
        }
    }
}