using System;

namespace BusinessTycoonSim
{
    public class PropertyInfoForDisplay
    {
        public string Name { get; set; }
        public Type Type { get; set; }
        public object Value { get; set; }
        public int DrawOrder { get; set; }
        public int RowHeight { get; set; }

        public void PopulateValue(object item)
        {
            Value = item.GetValue(Name);
        }
    }
}