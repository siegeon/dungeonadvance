﻿using System;
using UnityEngine;

namespace Assets.Editor.MasterData
{
    [AttributeUsage(AttributeTargets.All)]
    public class EditorAttribute : Attribute
    {
        public static EditorAttribute DefaultAttribute = new EditorAttribute(0,1);

        public readonly int Order;
        public readonly int Height;

        public EditorAttribute(int order, int height)
        {
            Order = order;
            Height = height;
        }
        

    }
}