﻿using System;
using UnityEngine;

namespace Assets.Databases
{
    public interface IManager
    {
        int Cost { get; set; }
        string Name { get; set; }
        Texture2D Image { get; set; }
        Guid Id { get; set; }
    }
}