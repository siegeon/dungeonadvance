﻿using System;
using UnityEngine;

namespace Assets.Databases
{
    public interface IStore
    {
        Guid Id { get; set; }
        string Name { get; set; }
        Texture2D Image { get; set; }
        int BaseStoreCost { get; set; }
        int BaseStoreProfit { get; set; }
        int StoreTimer { get; set; }
        float StoreMultiplier { get; set; }
        int StoreTimerDivision { get; set; }
    }
}