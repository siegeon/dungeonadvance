﻿using System;
using System.Runtime.Remoting.Messaging;
using Assets.Code.MVC.Models;

namespace Assets.Code.MathHelpers
{

    internal class IdleMath
    {
        /// <summary> Gets player die. </summary>
        ///
        /// <param name="baseDie"> The base die. </param>
        /// <param name="level">   The level. </param>
        ///
        /// <returns> The player die. </returns>
        public static double GetPlayerDie(float baseDie, double level) =>
            baseDie * level;


        /// <summary> Gets monster die. </summary>
        ///
        /// <param name="baseDie"> The base die. </param>
        /// <param name="level">   The level. </param>
        /// <param name="rate">    The rate. </param>
        ///
        /// <returns> The monster die. </returns>
        public static double GetMonsterDie(int baseValue, double level, float rate) =>
            baseValue *  Math.Pow(level, rate);

        public static int CostForX(int baseCost, int current, int multiplyer, int count)
        {
            double expr1 = Math.Pow(multiplyer, count) * (Math.Pow(multiplyer, count) - 1);
            return (int)(baseCost * (expr1 / (multiplyer - 1)));
        }

        public static int CostForX(float baseCost, int current, float multiplyer, int count)
        {
            double expr1 = Math.Pow(multiplyer, count) * (Math.Pow(multiplyer, count) - 1);
            return (int)(baseCost * (expr1 / (multiplyer - 1)));
        }
    }

    internal static class StatisticExtensions
    {
        public static double CostForX(this StatisticModel model, double number)
        {
            return new double();
            //BigInteger expr1 = BigInteger.Pow(model.Multiplyer, model.Value) * (BigInteger.Pow(model.Multiplyer, number) - 1);
            //return model.Cost * (expr1 / (model.Multiplyer - 1));
        }

        public static double CostForNext(this StatisticModel model)
        {
            return model.Cost * Math.Pow(model.Value, model.Multiplyer);
        }
    }
}
