﻿using System.Globalization;

namespace Assets.Code.MathHelpers
{
    /// <summary> The collection of string formatting extensions </summary>
    internal static class StringFormatters
    {
        public static string ToExponentString(this double @double) => @double.ToString("G2", CultureInfo.InvariantCulture);
    }
}