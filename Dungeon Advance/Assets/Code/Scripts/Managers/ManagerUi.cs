using Assets.Code.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace BusinessTycoonSim
{
    public class ManagerUi
    {
        private GameObject _managerPrefab;
        private readonly Manager _manager;
        const string ManagerButtonPurchaseStr = "PURCHASED";
        const string ManagerUnlockButtonStr = "Unlock ";

        public Button ManagerButton;
        public Text ManagerButtonText;
        public Text ManagerNameText;


        public ManagerUi(GameObject managerPrefab, Manager manager, string storeName)
        {
            _managerPrefab = managerPrefab;
            _manager = manager;

            // Find the ManagerButton in our prefab and text in the freab and set the button text to include the cost of the upgrade
            ManagerButton = managerPrefab.transform.Find("UnlockManagerButton").GetComponent<Button>();
            ManagerButtonText = ManagerButton.transform.Find("UnlockManagerButtonText").GetComponent<Text>();
            ManagerNameText = _managerPrefab.transform.Find("ManagerNameText").GetComponent<Text>();

            // TODO: Upgrade to use the big number function
            ManagerButtonText.text = ManagerUnlockButtonStr + GameManager.FormatNumber(manager.Cost);

            // Set the parent of the prefab to the manager panel
            _managerPrefab.transform.SetParent(UIManager.instance.ManagerListPanel.transform);

            // Set the name of the manager text to the name of the store
            // You could upgrade this to set a name for the manager
            ManagerNameText.text = storeName;
            
            // TODO: Upgrade to use the big number function
            ManagerButtonText.text = ManagerUnlockButtonStr + GameManager.FormatNumber(_manager.Cost);

            // Add a listener to the Manager button so that the UnlockManager method is called inside the correct store
            ManagerButton.onClick.AddListener(manager.UnlocakManager);
        }

        public void ResetManagerButton()
        {
            ManagerButtonText.text = ManagerUnlockButtonStr + GameManager.FormatNumber(_manager.Cost);
        }

        public void UpdateUI()
        {
            if (_manager.Unlocked)
            {
                ManagerButtonText.text = ManagerButtonPurchaseStr;
                ManagerButton.interactable = false;
            }
            else
            {
                // Update manager button if store can be afforded.
                if (GameManager.CanBuy(_manager.Cost))
                    ManagerButton.interactable = true;
                else
                    ManagerButton.interactable = false;
            }
        }

    }
}