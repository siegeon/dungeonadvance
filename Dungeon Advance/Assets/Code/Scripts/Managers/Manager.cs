using Assets.Code.Scripts;
using UnityEngine;

namespace BusinessTycoonSim
{
    [System.Serializable]
    public class Manager
    {
        // Mangers
        [SerializeField] private readonly string _name;

        [SerializeField] private readonly float _cost;

        [SerializeField] private bool _unlocked;
        
        public string Name { get { return _name; } }
        public float Cost { get { return _cost; } }
        public bool Unlocked { get { return _unlocked; } }

        public Manager(float cost, string name)
        {
            _cost = cost;
            _name = name;

        }
        
        public void UnlocakManager()
        {
            UnlockManager(false);
        }

        public bool UnlockManager(bool force = false)
        {
            if (!force)
            {
                if (_unlocked)
                    return true;

                if (!GameManager.CanBuy(_cost))
                    return false;
            }

            GameManager.AddToBalance(-_cost);
            _unlocked = true;
            return true;
        }

        public void Reset()
        {
            _unlocked = false;
        }
    }
}