﻿using System;
using System.Collections;
using System.Linq;
using BusinessTycoonSim;
using UnityEngine;

// Unique and shared namespace for all scripts protects from conflicts with other libraries
namespace Assets.Code.Scripts
{
    // This is the main game manager class
    public class GameManager : MonoBehaviour
    {
        // Constants
        // TODO: We don't really want to hardcode this in our exe
        // perhaps a .dat or .config file to better implement?
        public static string CurrencyFormatString => "C2";

        private const double ShareHolderBaseStart = 150d;
        private const float UIUpdateFrequency = .25f;

        // Example of Observer design pattern
        // These events update all subscriber objects that require it when the balance changes
        public delegate void UpdateBalance();

        public static event UpdateBalance OnUpdateBalance;

        public enum State
        {
            Loading,
            Running,
            Quitting
        }

        // Lifetime earnings - This is used for the calculations for shareholders (angel investors)

        // The current state the interface is in

        // Used for Singleton design to hold one and only one instance of the GameManager
        public static GameManager Instance;

        // Holds the most important value for the entire game... the amount of money you have

        // A simple string to hold the company name

        public static float ShareholderMultiplier;

        // Game Name and Company Name

        // Buy multiplier... keeps track of x1, x5, x10, x25 to buy multiple stores without carpel tunel

        // This is the total EPS for all stores updated by store * count * storemultiplierfromupgrades

        // This array holds the list of currency names
        //    loaded from CSV file in data folder (bignumbers.csv)
        // Data source: http://www.olsenhome.com/bignumbers/

        // The GameManager should for the most part have easy access to the major collections in the game
        // We load these lists in the LoadGameData script

        // Reference to our UIManager

        // If you have a saved game, this stores the profits from the time you were last idle

        // This flag is used as an easy way to tell the engine not to save the game when OnApplicationQuit() is called
        // Primarily used to reset the game and clear all store counts, upgrades, cash, etc.

        // We provide access to the class through these
        //TODO: remove unnecessary setters & getters

        #region Getters & Setters

        public static string CompanyName { get; set; }

        public static ArrayList CurrencyArray { get; set; }

        public static int CurrentBuyMultiplier { get; set; }

        public static double EarningsPerSecond { get; set; }

        public static ArrayList StoreUpgrades { get; set; }

        public static double Idleprofits { get; set; }

        public static bool DontSave { get; set; }

        public static State CurrentState { get; set; }

        public static double LifeTimeEarnings { get; set; }

        public static int TotalShareholders { get; private set; }

        public static int ActiveShareholders { get; set; }

        public static string GameName { get; set; }

        public static double CurrentBalance { get; set; }

        public static double StartingBalance { get; set; }

        public static int FirstStoreCount { get; set; }

        #endregion Getters & Setters

        private void OnEnable()
        {
            // We don't use the start() method to actually start the game as we want
            // to make sure everything is loaded, setup, and ready to go.
            // This is when the UIManager announces that the UI has completed loading.
            UIManager.OnLoadUIComplete += StartGame;
        }

        public void StartGame()
        {
            LoadPlayerDataPrefs.LoadSavedGame();
            //UIManager.instance.UpdateUI();
            // If we made money while we were gone... then show us the money!
            if (Idleprofits > 0)
                UIManager.instance.onLoadGame();
            CurrentState = State.Running;
            StartCoroutine(GameUpdate());
        }

        private static IEnumerator GameUpdate()
        {
            while (CurrentState == State.Running)
            {
                CalculateEps();
                UIManager.instance.UpdateUI();
                yield return new WaitForSeconds(UIUpdateFrequency);
            }
        }

        public static void ResetGame()
        {
            PlayerPrefs.DeleteAll();
            LifeTimeEarnings = 0;
            TotalShareholders = 0;
            ActiveShareholders = 0;

            RestartGame();
        }

        public static void RestartGame()
        {
            // TODO: We have started attempting to reset the game without
            // quitting ... needs more testing
            CurrentBalance = StartingBalance;

            ResetStores();

            ClearUpgrades();
        }

        public static void ClearUpgrades()
        {
            foreach (storeupgrade storeUpgrade in StoreUpgrades) storeUpgrade.UpgradeUnlocked = false;
        }

        public void QuitGame()
        {
#if UNITY_EDITOR
            // Application.Quit() does not work in the editor so
            // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
//            EditorApplication.isPlaying = false;
//#else
                             Application.Quit();
#endif
        }

        // Example of singleton design pattern
        // When we awake check if the instance is null. If it is then we assign instance to this.
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        public void ProcessShareholders()
        {
            ActiveShareholders = TotalShareholders;

            RestartGame();
        }

        public void OnApplicationQuit()
        {
            CurrentState = State.Quitting;
            SaveGameData.Save();
        }

        // This method bypasses shareholder bonuses and adding to LifeTimeEarnings
        public static void SetStartingBalance(double amt)
        {
            if (amt > 0)
                CurrentBalance += amt;
        }

        // We call this function anytime we need to add to the balance of the game.
        // Note we send a negative amount to take away money... We don't need a subtractFromBalance
        public static void AddToBalance(double amt)
        {
            if (amt > 0) // We made profit
            {
                if (ActiveShareholders > 1) amt = amt + CalculateShareHolderBonus(amt);
                LifeTimeEarnings += amt;

                TotalShareholders = (int) Math.Floor(ShareHolderBaseStart * Math.Sqrt(LifeTimeEarnings / 1.0e+15f));
            }

            CurrentBalance += amt;

            CalculateEps();

            // Example of Observer pattern
            // Notify all observers that we have updated the game balance
            // This is how the interface knows to update without using updates
            OnUpdateBalance?.Invoke();
        }

        // Returns true if we have enough money to buy an item costing 'AmtToSpend'
        public static bool CanBuy(double amtToSpend)
        {
            return !(amtToSpend > CurrentBalance);
        }

        // Returns the current balance. An example of how to protect CurrentBalance from manipulation outside this class.
        public static double GetCurrentBalance()
        {
            return CurrentBalance;
        }

        // This function will format a large number to have a descripiton (ie million, billion, trillion, etc)
        public static string FormatNumber(double numberToFormat)
        {
            // By defaut we will return the number formated as currency (ie no currency name sufix)
            var returnString = numberToFormat.ToString(CurrencyFormatString);
            // This is where we convert the very large number into a log10 format that we look up as the key value

            // If the currentbalance isn't greater than 0 let's not waste our time (example of defensive programming)
            if (numberToFormat > 0)
            {
                // LINQ query example... Maybe a little heavy if performance is critical but can be very useful for readable code
                // We sort our key values descending and only take 1 in order to get the correct key value
                var query = (from CurrencyItem findItem in CurrencyArray
                    where findItem.KeyVal <= Math.Log10(numberToFormat)
                    orderby findItem.KeyVal descending
                    select findItem).Take(1);

                // While we only should find one this is a clean way to process
                foreach (var s in query)

                    //Debug.Log(s.KeyVal);

                    // We format our current string by dividing our large number by the ExpVal property we have set
                    // when we loaded the data from XML. We then append the appropriate currency name.
                    // TODO: Have an option to display alternative short descriptions (ie mil, bil, tri, etc)
                    returnString = (numberToFormat / s.ExpVal).ToString(CurrencyFormatString) + " " + s.CurrencyName;
            }

            return returnString;
        }

        // This function is a wrapper for Format number so the calling methods do not need to reference the CurrentBalance
        public static string GetFormattedBalance()
        {
            return FormatNumber(CurrentBalance);
        }

        public static void CalculateEps()
        {
            var eps = Stores.EPS();

            if (ActiveShareholders > 1) eps = eps + CalculateShareHolderBonus(eps);
            EarningsPerSecond = (float) eps;
        }

        public static double CalculateShareHolderBonus(double amt)
        {
            return amt * ActiveShareholders * ShareholderMultiplier;
        }

        #region Stores

        private static readonly Stores Stores;

        static GameManager()
        {
            CurrentBuyMultiplier = 1;
            ShareholderMultiplier = .02f;
            Idleprofits = 0f;
            DontSave = false;
            StartingBalance = 0f;
            FirstStoreCount = 1;
            Stores = new Stores();
            StoreUpgrades = new ArrayList();
        }

        public static void UpdateStoreUIs()
        {
            Stores.UpdateUI();
        }

        public static void AddStore(store store)
        {
            Stores.AddStore(store);
        }

        private static void ResetStores()
        {
            Stores.Reset();
        }

        public static void SaveStores()
        {
            Stores.Save();
        }

        public static void LoadStores(float idleTime)
        {
            Stores.Load(idleTime);
        }

        #endregion Stores
    }
}