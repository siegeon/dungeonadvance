using System;

namespace BusinessTycoonSim
{
    [Serializable]
    public class StoreData
    {
        public int StoreCount;
        public float StoreMultiplier;
        public float StoreCurrentTimer;
        public float StoreTimer;
        public bool StoreTimerActive;
        public bool StoreManagerUnlocked;
        public bool StoreUnlocked;
        public int Id;
    }
}