﻿using UnityEngine;
using System.Collections;
using System;
using Assets.Code.MathHelpers;
using Assets.Code.Scripts;
using Assets.Utilities;


namespace BusinessTycoonSim
{
    // Primary class for each store in the game
    // One instance is created for each store in the gamedata XML
    // The instance is attached as component of the store prefab
    public class store : MonoBehaviour
    {
        // IdleSlack... This allows us to give a production penalties (or rewards if you wish to go that way) for idle vs active play
        const float IdleSlackPercent = 1f;

        const float IdleSlackFixed = 1f;

        public UIStore StoreUI;

        public int Id;
        // Name of the store... like all these properties they are populated by the LoadGameData class
        private string storeName;

        // Name of the store... like all these properties they are populated by the LoadGameData class
        private string storeImage;

        // This is the cost to buy one unit of the store... this is just the base.. each store you buy costs more based on the StoreMultiplier
        private float baseStoreCost;

        // This is how much the store makes each time the store timer runs out...this also is just the base... upgrades will change the profit
        private float baseStoreProfit;

        // How long (in seconds) does it take for the store to make its profit
        [SerializeField]
        private float storeTimer;

        [SerializeField]
        private float baseTimer;

        // How many of this store does the player own
        private int storeCount;

        // Current multiplier for the store.. Starts out at 1f and increases with store upgrades
        private float storeMultiplier;

        // Has the store been unlocked?
        private bool storeUnlocked;


        // Every X number of stores the timer is cut in half... This value determines that division
        private int storeTimerDivision;


        // Keeps track of the current timer for the store. When the timer runs out the profit is posted and the timer starts over
        private float currentTimer = 0;

        // Keeps track of the timer stop/started
        public bool StartTimer;

        // Mangers
        [SerializeField] private Manager _manager;

        public Manager Manager
        {
            get { return _manager; }
        }

        public ArrayList Upgrades = new ArrayList();

        // Our currentmultplier from upgrades
        private float currentMultiplier;

        // Profit per second
        private double profitPerSecond;

        //public GameObject ManagerPrefab;
        
        #region Property Getters & Setters
        //---------------------------
        // Property Getters & Setters
        //---------------------------
        public string StoreName
        {
            get
            {
                return storeName;
            }

            set
            {
                storeName = value;
            }
        }


        public float BaseStoreCost
        {
            get
            {
                return baseStoreCost;
            }

            set
            {
                baseStoreCost = value;
            }
        }

        public float BaseStoreProfit
        {
            get
            {
                return baseStoreProfit;
            }

            set
            {
                baseStoreProfit = value;
            }
        }

        public float StoreTimer
        {
            get
            {
                return storeTimer;
            }

            set
            {
                storeTimer = value;
            }
        }

        public int StoreCount
        {
            get
            {
                return storeCount;
            }

            set
            {
                storeCount = value;
            }
        }

        public float StoreMultiplier
        {
            get
            {
                return storeMultiplier;
            }

            set
            {
                storeMultiplier = value;
            }
        }

        public bool StoreUnlocked
        {
            get
            {
                return storeUnlocked;
            }

            set
            {
                storeUnlocked = value;
            }
        }

        public int StoreTimerDivision
        {
            get
            {
                return storeTimerDivision;
            }

            set
            {
                storeTimerDivision = value;
            }
        }

        public float CurrentMultiplier
        {
            get
            {
                return currentMultiplier;
            }

            set
            {
                currentMultiplier = value;
            }
        }

        public string StoreImage
        {
            get
            {
                return storeImage;
            }

            set
            {
                storeImage = value;
            }
        }

        public double ProfitPerSecond
        {
            get
            {
                return profitPerSecond;
            }

            set
            {
                profitPerSecond = value;
            }
        }

        public float BaseTimer
        {
            get
            {
                return baseTimer;
            }

            set
            {
                baseTimer = value;
            }
        }

        public float CurrentTimer
        {
            get
            {
                return currentTimer;
            }

            set
            {
                currentTimer = value;
            }
        }

        //Access method to reduce coupling
        public bool ManagerUnlocked { get { return _manager.Unlocked; } }

        #endregion


        // Use this for initialization
        void Start()
        {

            // They haven't clicked yet! So the timer isn't started
            //StartTimer = false;

            // No upgrades.. mulipler should just be 1
            CurrentMultiplier = 1f;

            // There is no need to process our model every frame... instead we can
            // Determine here how often to check the timers and post profits & udpate UI
            // The only update method in the entire game is for the progress bars on the stores
            // and to delta time to track those timers.

          
            StartCoroutine(ProcessTick());
        }

        public void UpdateUI()
        {
            StoreUI.UpdateUI();
        }

        // Just protecting CurrentTimer...
        public float GetCurrentTimer()
        {
            return CurrentTimer;
        }


        public double GetNextStoreCost()
        {
            return baseStoreCost * System.Math.Pow(StoreMultiplier, StoreCount);
            //return NextStoreCost;
        }


        // Return the current timer
        // Once again we have protected the private variable StoreTimer
        public float GetStoreTimer()
        {
            return StoreTimer;
        }

        private void Update()
        {
            // This is how we increment the timer. We use Time.deltatime as it knows how much time went by since the last frame
            //Debug.Log("Timer Going..." + CurrentTimer.ToString() + " Store Timer= " + StoreTimer.ToString());
            CurrentTimer += Time.deltaTime;
        }

        // Update is called once per frame
        // TODO: Refactor to use Involking or other method to avoid code in the Update

         IEnumerator  ProcessTick()
        {
            while (GameManager.CurrentState ==  GameManager.State.Running)
            {

            
            // If the timer isn't running... there is nothing to update
            if (StartTimer && GameManager.CurrentState == GameManager.State.Running)
            {


                // Once our timer gets to the end we need to post the profit and start the timer over
                 
                if (GetCurrentTimer() > GetStoreTimer())
                {
                    // If we have not unlocked the manager turn off the timer... forcing the user to click to start it again
                    if (!Manager.Unlocked)
                        StartTimer = false;

                    // Reset the timer
                    CurrentTimer = 0f;

                    // Store makes its money... CurrentMultiplier will be increasing based on upgrades 
                    GameManager.AddToBalance(CalculateStoreProfit());
                    //Debug.Log("Add to Balance:" + CalculateStoreProfit().ToString());
                    
                }
            }
                if (!StartTimer)
                    CurrentTimer = 0f;

                
                //UIManager.instance.UpdateUI();
                yield return new WaitForSeconds(.1f);
            }

            
        }

        public double CalculateStoreProfit()
        {

            double Amt = BaseStoreProfit * StoreCount * CurrentMultiplier;
            return Amt;
        }
        public void CreateStoreManager(float ManagerCostVal, string ManagerNameString, IResourceLoader resourceLoader)
        {
            _manager = new Manager(ManagerCostVal, ManagerNameString);

            //GameObject NewManager = (GameObject)Resources.Load("ManagerPrefab");   
            GameObject NewManager = resourceLoader.LoadManagerPrefab();

            //Debug.Log("Create Manager" + NewManager.ToString());
            // We want to make it ultra easy to talk with the UIStore manager and vice versa
            StoreUI = transform.GetComponent<UIStore>();

            // Get a refrence to the UIManagerprefab for the store and associate our manager button to the store
            // This is how the UIStore can know exactly which store manager needs to be unlocked
            StoreUI.ManagerPrefab = NewManager;

        }
        // This is the routine that figures out how much profit was earned while the game was not running
        public void CalculateIdleProfit(float SecondsIdle, float LastTimerValue)
        {


            SecondsIdle = (SecondsIdle - IdleSlackFixed) * IdleSlackPercent;

            // Only give profit if we are idle longer than the store timer
            double IdleAmt = 0d;
            
            if (StartTimer && SecondsIdle > 0f)
            {
                if (SecondsIdle > StoreTimer - LastTimerValue)
                {
                    if (Manager.Unlocked)
                    {
                        IdleAmt = CalculateStoreProfit() * (double)(SecondsIdle / StoreTimer);
                        CurrentTimer = (SecondsIdle % (StoreTimer - LastTimerValue)) + LastTimerValue;
                        //this.transform.GetComponent<UIStore>().ManagerUnlocked();

                    }
                    else
                    {
                        IdleAmt = CalculateStoreProfit();
                        CurrentTimer = 0f;
                    }
                    GameManager.Idleprofits += IdleAmt + GameManager.CalculateShareHolderBonus(IdleAmt) ;
                    GameManager.AddToBalance(GameManager.Idleprofits);
                }
                else
                {
                    CurrentTimer = LastTimerValue + SecondsIdle;
                }
            }
            
            //Debug.Log(StoreName + " made " + IdleAmt.ToString() + " while idle.");
        }

     
        public double CalculateProfitPerSecond()
        {
            // To update the master we will try an efficient trick
            // Instead of adding up all the stores in an update we will attempt to first subtract out the EPS for this store
            // from the global EPS value and then add it back in once we have recalculated

            if (StartTimer)
            {
                 
                ProfitPerSecond = CalculateStoreProfit() / StoreTimer;
                 
            }
            else
                ProfitPerSecond = 0;
            
            return ProfitPerSecond;
        }

        // We have bought a store!!!
        // Let's update the required values
        public void BuyStores()
        {
            TipRotator.instance.DisplayTip("produce");
            // Try to buy the amount of stores the player wants to buy
            double StoreBuyCost = IdleMath.CostForX(BaseStoreCost, StoreCount, StoreMultiplier, GameManager.CurrentBuyMultiplier);
            if (StoreBuyCost < GameManager.CurrentBalance)
            {
                GameManager.CurrentBalance -= StoreBuyCost;
                StoreCount += GameManager.CurrentBuyMultiplier;
            }
            else  // if they can't buy that many then buy as many as they possibly can
            {
                int NumStoresToBuy = BuyMax();
                StoreBuyCost = IdleMath.CostForX(BaseStoreCost, StoreCount, StoreMultiplier, NumStoresToBuy);
                GameManager.CurrentBalance -= StoreBuyCost;
                StoreCount += NumStoresToBuy;
            
            }

      
            StoreTimer = BaseTimer / (Mathf.Floor(StoreCount / StoreTimerDivision)+1);
            

        }


        // This method calculates the maximum number of stores that can be purchased
        public int BuyMax()
        {

            // Forumla source: http://blog.kongregate.com/the-math-of-idle-games-part-i/
            double expr1 = GameManager.CurrentBalance * (StoreMultiplier - 1);
            double expr2 = baseStoreCost * Math.Pow(StoreMultiplier, StoreCount);
            double expr3 = Math.Log((expr1 / expr2) + 1);
            double expr4 = expr3 / Math.Log(StoreMultiplier);

            return (int)Math.Floor(expr4);


        }
        public void OnStartTimer()
        {
           
            // Some defensive programming just to make sure we have at least one store
            if (!StartTimer && StoreCount > 0) { 
                StartTimer = true;
                TipRotator.instance.DisplayTip("purchasemore");
        }
        }

        // Unlock the store manager (automates store timers so you don't have to click to make money!)
        public void UnlockManager()
        {
            // Defensive programming... already unlocked we don't need to do anything
            Manager.UnlockManager();
        }

        public void LoadManager(int state)
        {
            if (state == 1)
            {
                Manager.UnlockManager(true);
            }
        }


        public void LoadStore(StoreData storeData, float idleTime)
        {
            if (storeData.Id == 0)
            {
                if (Id == 1)
                {
                    Reset();
                }
            }
            else
            {
                Id = storeData.Id;
                StoreCount = storeData.StoreCount;
                storeUnlocked = storeData.StoreUnlocked;
                StartTimer = storeData.StoreTimerActive;
                StoreMultiplier = storeData.StoreMultiplier;
                CalculateIdleProfit(idleTime, storeData.StoreCurrentTimer);
                Manager.UnlockManager(storeData.StoreManagerUnlocked);
            }

        }

        public StoreData GetData()
        {
            return new StoreData
            {
                Id = Id,
                StoreCount = storeCount,
                StoreUnlocked = storeUnlocked,
                StoreCurrentTimer = GetCurrentTimer(),
                StoreTimer = GetStoreTimer(),
                StoreTimerActive = StartTimer,
                StoreManagerUnlocked = _manager.Unlocked,
                StoreMultiplier = StoreMultiplier
            };
        }

        public void Reset()
        {
            if(Id == 1)
            {
                StoreCount = 1;
                StoreUnlocked = true;
            }
            else
            {
                StoreUnlocked = false;
                StoreCount = 0;
            }
            CurrentMultiplier = 1f;
            StoreTimer = BaseTimer;
            StartTimer = false;
            CurrentTimer = 0f;

            Manager.Reset();
            StoreUI.ResetManagerButton();
        }
    }
}
