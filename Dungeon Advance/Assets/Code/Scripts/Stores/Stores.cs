﻿using System.Collections.Generic;
using Assets.Code.DataAccess.Base;

namespace BusinessTycoonSim
{
    public class Stores
    {
        private readonly List<store> _stores = new List<store>();
        private readonly StoreRepository _storeRepository = new StoreRepository();

        public Stores()
        {
            
        }

        public void AddStore(store store)
        {
            _stores.Add(store);
        }

        public void UpdateUI()
        {
            foreach (var store in _stores)
            {
                store.UpdateUI();
            }
        }

        public void Reset()
        {
            foreach (var store in _stores)
            {
                store.Reset();
            }
        }

        public double EPS()
        {
            double EPS = 0;
            foreach (var store in _stores)
            {
                EPS = EPS + store.CalculateProfitPerSecond();
            }

            return EPS;
        }

        public void Save()
        {
            foreach (var store in _stores)
            {
                _storeRepository.Save(store.GetData());
            }
        }

        public void Load(float idleTime)
        {
            foreach (var store in _stores)
            {
                store.LoadStore(_storeRepository.Load(store.Id), idleTime);
            }
        }
    }
}