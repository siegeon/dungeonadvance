﻿using System;
using Assets.Code.DataAccess.Base;
using BusinessTycoonSim;
using UnityEngine;

namespace Assets.Code.Scripts
{
    public static class LoadPlayerDataPrefs
    {
        // Date Time if loading from saved game
        static DateTime currentDate;
        static DateTime oldDate;
        static StoreRepository storeRepository = new StoreRepository();

        public static void LoadSavedGame()
        {

            // Let's just assume 0f idle time if we can't get anything out of the file
            // But we will be checking anyway and not call any load functions if we don't have a valid file

            // We use a try here as it is an easy way to see if we have a valid playerprefs file
            // If we don't... then we use the catch to get out of the load game function.

            if (IsGameSaved())
            {
           

                // Get the idle time that has passed since the game has been loaded
                float getIdleTime = GetIdleTime();



                GameManager.ActiveShareholders = PlayerPrefs.GetInt("ActiveShareholders");

                // This method loads the store data from the playerpref file
                LoadSavedStoreData(getIdleTime);

                // Load the upgrades from the playerpref file
                LoadStoreUpgrades();

                // Get the saved cash through the game manager!
                GameManager.SetStartingBalance(double.Parse(PlayerPrefs.GetString("Cash")));

                GameManager.LifeTimeEarnings = double.Parse(PlayerPrefs.GetString("LifeTimeEarnings"));


            }

            else
            {
                GameManager.AddToBalance(GameManager.StartingBalance);
            }
            
        }

        // We use a simple int in the player prefs to determine if we have a saved game to load
        public static bool IsGameSaved()
        {
            // This is wrapped in a try block as the attempt to check GaameSavedflag will fail if we have never saved the game
            try
            {
                int GameSavedflag = PlayerPrefs.GetInt("GameSaved");
                if (GameSavedflag == 1)  // One way to reset the game on startup would be to store a 0 for this field in the playerprefs
                    return true;
                else
                    return false;
            }
            catch
            {
                Debug.Log("Can't read 'GameSaved' key in PlayerPrefs. Start a new game.");
                return false;
            }
        }

        // We need the idle time to calculate how much the player earned
        private static float GetIdleTime()
        {
            float IdleTime = 0;

            // Defensive programming... Once again we wrap this in a try block to handle invalid data/calculations
            try
            {

                // This gets the stored string into a 64bit integer
                long temp = Convert.ToInt64(PlayerPrefs.GetString("SaveDateTime"));

                //Convert the old time from binary to a DataTime variable
                DateTime oldDate = DateTime.FromBinary(temp);

                //Use the Subtract method and store the result as a timespan variable
                currentDate = System.DateTime.Now;
                TimeSpan difference = currentDate.Subtract(oldDate);

                // Save the idle time in seconds so we can calculate profits when loading the 
                IdleTime = (float)difference.TotalSeconds;

            }
            catch (FormatException)
            {
                // Something went wrong with the saved data
                Debug.Log("exception caught...  We will start a new game.");
                

            }
            return IdleTime;
        }

        // This method goes through all the StoreUpgrades and determines which ones the player has unlocked
        private static void LoadStoreUpgrades()
        {

            // We need this counter to keep a unique key for each upgrade
            int counter = 1;
            foreach (storeupgrade StoreUpgrade in GameManager.StoreUpgrades)
            {
                string stringKeyName = "storeupgradeunlocked_" + counter.ToString();
                int Unlocked = PlayerPrefs.GetInt(stringKeyName);
                // Debug.Log("Get StoreUpgrade for key-" + stringKeyName + " - " + StoreUpgrade.Store.StoreName + " - " + StoreUpgrade.UpgradeName + " Unlock Value=" + Unlocked.ToString());

                if (Unlocked == 1)
                {
                
                    StoreUpgrade.UpgradeUnlocked = true;
                }
                else
                    StoreUpgrade.UpgradeUnlocked = false;
               
                counter++;
            }
        }

        // Load the store data that has been saved in the playerpref file
        private static void LoadSavedStoreData(float IdleTime)
        {
            GameManager.LoadStores(IdleTime);
            
        }
    }
}