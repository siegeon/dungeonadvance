﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Code.Maybe
{
    /// <summary>
    ///     With Maybe, we have wrapped an array in an object wrapper. There is significance in using an array, instead
    ///     of a bare value and a flag stating where there is a value. Maybe is actually a special case of list types (such as
    ///     IEnumerable in .Net). Instead of the type containing zero-to-many items, it contains either zero or one. it's
    ///     impossible for an option type to be assigned null. The type system simply won't allow it. To provide a value for
    ///     the option we use the Some constructor. Otherwise, we use None.
    /// </summary>
    public static class Maybe
    {
        public static Maybe<T> Some<T>(T value)
        {
            return Maybe<T>.Some(value);
        }
    }

    /// <summary>
    ///     With Maybe, we have wrapped an array in an object wrapper. There is significance in using an array, instead
    ///     of a bare value and a flag stating where there is a value. Maybe is actually a special case of list types (such as
    ///     IEnumerable in .Net). Instead of the type containing zero-to-many items, it contains either zero or one. it's
    ///     impossible for an option type to be assigned null. The type system simply won't allow it. To provide a value for
    ///     the option we use the Some constructor. Otherwise, we use None.
    /// </summary>
    /// <typeparam name="T"> Generic type parameter. </typeparam>
    public struct Maybe<T> : IEquatable<Maybe<T>>
    {
        private readonly IEnumerable<T> _values;

        /// <summary> Somes the given value. </summary>
        ///
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ///
        /// <param name="value"> The value. </param>
        ///
        /// <returns> A Maybe&lt;T&gt; </returns>
        public static Maybe<T> Some(T value)
        {
            if (value == null)
                throw new InvalidOperationException();

            return new Maybe<T>(new[] {value});
        }

        public static Maybe<T> None => new Maybe<T>(new T[0]);

        private Maybe(IEnumerable<T> values)
        {
            _values = values;
        }

        public bool HasValue => _values != null && _values.Any();

        public T Value
        {
            get
            {
                if (HasValue)
                    return _values.Single();
                throw new InvalidOperationException("Maybe does not have a value");
            }
        }

        public T ValueOrDefault(T @default)
        {
            return !HasValue ? @default : _values.Single();
        }

        public T ValueOrThrow(Exception e)
        {
            if (!HasValue)
                throw e;
            return Value;
        }

        public TU Case<TU>(Func<T, TU> some, Func<TU> none)
        {
            return HasValue
                ? some(Value)
                : none();
        }

        public void Case(Action<T> some, Action none)
        {
            if (!HasValue)
                none();
            else
                some(Value);
        }

        public void IfSome(Action<T> some)
        {
            if (!HasValue)
                return;
            some(Value);
        }

        public Maybe<TU> Map<TU>(Func<T, Maybe<TU>> map)
        {
            return HasValue
                ? map(Value)
                : Maybe<TU>.None;
        }

        public Maybe<TU> Map<TU>(Func<T, TU> map)
        {
            return HasValue
                ? Maybe.Some(map(Value))
                : Maybe<TU>.None;
        }

        public bool Equals(Maybe<T> other)
        {
            return Equals(_values, other._values);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Maybe<T> && Equals((Maybe<T>) obj);
        }

        public override int GetHashCode()
        {
            return (_values != null ? _values.GetHashCode() : 0);
        }

        public static bool operator ==(Maybe<T> left, Maybe<T> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Maybe<T> left, Maybe<T> right)
        {
            return !left.Equals(right);
        }
    }
}