using System;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.SlashNet.Items.Weapons;
using SlashNet.Items;

namespace Assets.Code.MVC.Models
{
    public class WeaponModel : IEntity, IEquipment
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public int Weight { get; set; }
        public AttacksModel AttacksModel { get;set; }
        public WeaponType WeaponType { get; set; }
        public StatisticsModel StatisticsModel { get; set; }
    }

    public interface IEquipment
    {
        StatisticsModel StatisticsModel { get; set; }
    }
}