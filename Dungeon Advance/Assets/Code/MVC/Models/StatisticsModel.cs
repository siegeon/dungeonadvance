using System.Collections.Generic;

namespace Assets.Code.MVC.Models
{
    public class StatisticsModel
    {
        public List<StatisticModel> StatisticModels { get; set; } = new List<StatisticModel>();
    }
}