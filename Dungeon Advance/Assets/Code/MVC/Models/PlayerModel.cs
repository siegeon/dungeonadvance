using System;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.MathHelpers;
using Newtonsoft.Json;

namespace Assets.Code.MVC.Models
{
    [System.Serializable]
    public class PlayerModel : IEntity
    {
        public double Money { get; set; } = 0;
        public double ExpToNextLevel { get; set; } = 3000;
        public string Name { get; set; } = "Siegeon";
        public double Level { get; set; } = 1;
        public double MaxHealth { get; set; } = 100;
        public double CurrentHealth { get; set; } = 100;
        public double Exp { get; set; } = 1;
        public double DungeonLevel { get; set; } = 1;

        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid WieldedId { get; set; }
        public Guid WornId { get; set; }

        [JsonIgnore] public string MoneyText => Money.ToExponentString();
   
    }
}