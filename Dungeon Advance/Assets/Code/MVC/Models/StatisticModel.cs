using Assets.Code.MathHelpers;
using Assets.Code.SlashNet.Character_Stats;
using Newtonsoft.Json;

namespace Assets.Code.MVC.Models
{
    public class StatisticModel
    {
        public StatisticTypes Type { get; set; } = StatisticTypes.None;
        public double Value { get; set; } = 0;
        public double Cost { get; set; } = 0;
        public double Multiplyer { get; set; }

        [JsonIgnore] public string ValueText =>  Value.ToExponentString();
        [JsonIgnore] public string NextCostText => this.CostForNext().ToExponentString();
        [JsonIgnore] public string TypeText => Type.ToString();
    }
}