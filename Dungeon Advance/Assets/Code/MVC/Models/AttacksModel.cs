using System.Collections.Generic;

namespace Assets.Code.MVC.Models
{
    public class AttacksModel
    {
        public List<AttackModel> AttackModels { get; set; }
    }
}