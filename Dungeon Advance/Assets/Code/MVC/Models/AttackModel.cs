using SlashNet;

namespace Assets.Code.MVC.Models
{
    public class AttackModel
    {
        public EffectType EffectType { get; set; }
        public int Sides { get; set; }
    }
}