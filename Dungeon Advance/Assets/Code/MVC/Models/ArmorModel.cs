using System;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.SlashNet.Items.Weapons;

namespace Assets.Code.MVC.Models
{
    public class ArmorModel : IEntity, IEquipment
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public ArmorType ArmorType { get; set; }
        public int Weight { get; set; }
        public string Name { get; set; }
        public StatisticsModel StatisticsModel { get; set; }
    }
}