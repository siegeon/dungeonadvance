﻿using Assets.Code.MVC.Models;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.MVC.Views
{
    public class StatView : MonoBehaviour
    {
        public Text StatName;
        public Slider Progress;

        public void ShowStat(StatisticModel statistic)
        {
            StatName.text = statistic.Type.ToString();
            Progress.maxValue = 10;
            //Progress.value = statistic.Value;
        }
    }
}