﻿using System;
using Assets.Code.MathHelpers;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet;
using SlashNet;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.MVC.Views
{

    public class StatisticItemView : MonoBehaviour
    {
        public Text StatName;
        public Text ValueText;
        public Text NextCost;
        public Slider Progress;
        private StatisticModel _stat;

        public void Set(StatisticModel statistic)
        {
            _stat = statistic;
            StatName.text = statistic.TypeText;
            ValueText.text = statistic.ValueText;
            NextCost.text = statistic.NextCostText;
            Progress.minValue = 0;
            Progress.maxValue = 20;
            //Progress.value = statistic.Value;
        }

        public void Purchase()
        {
            var upgradeCost = _stat.CostForNext();
            if (upgradeCost < SlashNetGame.Instance.Player.Money)
            {
                SlashNetGame.Instance.Player.Money -= upgradeCost;
                _stat.Value += 1;
                NextCost.text = _stat.NextCostText;
                StatName.text = _stat.TypeText;
                ValueText.text = _stat.ValueText;
                Progress.value++;
            }
        }
    }
}