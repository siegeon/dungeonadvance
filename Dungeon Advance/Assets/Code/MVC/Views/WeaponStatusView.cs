﻿using System;
using Assets.Code.MVC.Models;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.MVC.Views
{
    public class WeaponStatusView : MonoBehaviour
    {
        public Text WeaponText;
        public Image WeaponImage;
        public GameObject WeaponStats;
        public GameObject StatViewPrefab;
        private bool visable;

        private void Start()
        {

        }

        internal void Initialize(WeaponModel weaponModel)
        {
            WeaponText.text = weaponModel.Name;
            foreach (var wd in weaponModel.StatisticsModel.StatisticModels)
            {
                var stat = Instantiate(StatViewPrefab);
                stat.transform.SetParent(WeaponStats.transform, false);
                stat.GetComponent<StatView>().ShowStat(wd);

            }
        }

        internal void ToggleWeapon()
        {
            visable = !visable;
            gameObject.SetActive(visable);
        }
    }
}