﻿using System.Collections.Generic;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet;
using SlashNet;
using UnityEngine;

namespace Assets.Code.MVC.Views
{
    public class StatScrollView : MonoBehaviour
    {
        public GameObject ListItemPrefab;
        public GameObject Container;
        private List<GameObject> stats = new List<GameObject>();

        private void Start()
        {

        }

        internal void Initialize(IEquipment weaponModel)
        {
            foreach (var stat in stats)
            {
                Destroy(stat);
            }
            stats.Clear();

            foreach (var wd in weaponModel.StatisticsModel.StatisticModels)
            {
                var stat = Instantiate(ListItemPrefab);
                stat.transform.SetParent(Container.transform, false);
                stat.GetComponent<StatisticItemView>().Set(wd);
                stats.Add(stat);
            }
        }

    }
}