﻿using Assets.Code.Extensions;
using Assets.Code.MVC.Views;
using Assets.Code.SlashNet;
using UnityEngine;
using UnityEngine.UI;

public class CharacterPopupView : MonoBehaviour
{
    public Button WeaponButton;
    public Text CharacterNameTextBox;
    public Text AttactText;

    private StatScrollView _scrollView;
    private Slider360 _expSlider;

    // Use this for initialization
    private void Start()
    {
        var player = SlashNetGame.Instance.Player;

        CharacterNameTextBox = CharacterNameTextBox ?? GameObjectExtensions.GetComponentFromGameObjectWithName<Text>("CharacterNameText");
        WeaponButton = GameObjectExtensions.GetComponentFromGameObjectWithName<Button>("WeaponButton");
        AttactText = GameObjectExtensions.GetComponentFromGameObjectWithName<Text>("AttackAmountText");

        _expSlider = transform.parent.gameObject.GetComponentInChildren<Slider360>(true);
        _scrollView = transform.parent.gameObject.GetComponentInChildren<StatScrollView>(true);



        //WeaponStatusView.Initialize(SlashNetGame.Instance.Player.Wielded.ToModel);
        AttactText.text = "1d6";
        CharacterNameTextBox.text = SlashNetGame.Instance.Player.Name;
        _expSlider.ShowProgress(player.PercentToNextLevel, "Exp");

    }

    // Update is called once per frame
    public void ShowWeapon()
    {
        _scrollView.Initialize(SlashNetGame.Instance.Player.Wielded.ToModel);
    }

    public void ShowArmor()
    {
        _scrollView.Initialize(SlashNetGame.Instance.Player.Worn.ToModel);
    }

}