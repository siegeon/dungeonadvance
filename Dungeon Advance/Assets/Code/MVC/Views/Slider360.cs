﻿using Assets.Code.SlashNet;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.MVC.Views
{
    public class Slider360 : MonoBehaviour
    {
        public Image Image;
        public Text Amount;
        public Text Title;

        public void ShowProgress(float percent, string title)
        {
            Image.fillAmount = percent;
            Amount.text = $"{(1 - percent).ToString().Substring(2)}%" ;
            Title.text = "Exp";
        }


    }
}