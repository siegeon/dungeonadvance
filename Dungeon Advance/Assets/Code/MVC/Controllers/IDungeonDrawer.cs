﻿namespace Assets.Code.MVC.Controllers
{
    public interface IDungeonDrawer
    {
        void Draw();
    }
}