﻿using Assets.Code.SlashNet;
using Assets.Code.SlashNet.Common;
using Assets.Code.SlashNet.Dungeon;
using Assets.Code.SlashNet.Monsters;
using Assets.Code.Utilities;
using Assets.Extensions;
using Assets.Structures;
using Assets.Unity3dInterfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.MVC.Controllers
{
    public class TextureDungeonDrawer : IDungeonDrawer
    {
        private readonly IDungeon _dungeon;
        private readonly RawImage _image;
        private readonly ITexture2D _dungeonTexture;
        private readonly Color32[] _dungeonColors;

        public TextureDungeonDrawer(IDungeon dungeon, RawImage image)
        {
            dungeon.GuardObjectNull(nameof(dungeon));
            image.GuardObjectNull(nameof(image));

            _dungeonTexture = _dungeonTexture = Texture2DFactory.New(dungeon.Width, dungeon.Height);
            _dungeon = dungeon;
            _image = image;

            _dungeonColors = new Color32[_dungeon.Height * _dungeon.Width];

            Draw();
        }
        
        public void Draw()
        {
            for (var y = 0; y < _dungeon.Height; y++)
            for (var x = 0; x < _dungeon.Width; x++)
            {
                _dungeonColors[CalculateIndex(y, x)] = Texture2DExtensions.GetTileColor(_dungeon[x, y]);
            }

            _dungeonColors[CalculateIndex(SlashNetGame.Instance.Player.Location)] = Color.green;

            foreach (var monster in SlashNetGame.Instance.Dungeon)
            {
                _dungeonColors[CalculateIndex(monster.Location)] = Color.red;
            }

            _dungeonTexture.SetPixels(_dungeonColors);
            _dungeonTexture.Apply();
            _image.texture = _dungeonTexture.Texture;
        }

        private int CalculateIndex(Point location) => 
            CalculateIndex(location.Y, location.X);
        private int CalculateIndex(int x, int y) => 
            x * _dungeon.Height + y;
    }
}