﻿using Assets.Code.DataAccess;
using Assets.Code.Dungeon;
using Assets.Code.Dungeon.Walkers;
using Assets.Code.MathHelpers;
using Assets.Code.MVC.Views;
using Assets.Code.SlashNet;
using Assets.Code.SlashNet.Combat;
using Assets.Code.SlashNet.Dungeon;
using Assets.Unity3dInterfaces;
using SlashNet;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.MVC.Controllers
{
    public class DungeonController : MonoBehaviour
    {
        public RawImage Image;
        public float WalkSpeed = 1;
        public DungeonView DungeonView;
        public Sprite BlockSprite;
        
        private IDungeonWalker _playerDungeonWalker;
        private IDungeon _dungeon;
        private ITexture2D _dungeonTexture;
        private IDungeonDrawer _dungeonDrawer;

        // Use this for initialization
        private void Start()
        {
            DungeonView = GetComponentInParent<DungeonView>();
            GenerateNewDungeon();
        }

        private void DealWithCombatResult(ICombatResult combatResult)
        {
            if (SlashNetGame.Instance.Player.Dead)
            {
                SlashNetGame.Instance.Player.ResetFromDeath();
                GenerateNewDungeon();
                return;
            }

            UpdateDungeonViewModel();

            SlashNetGame.Instance.LogMessage(Grammar.MakeAttackString(combatResult.Attacker, combatResult.Defender, combatResult.Damage.ToExponentString()));
            if (combatResult.Killed)
                SlashNetGame.Instance.LogMessage(combatResult.Attacker == SlashNetGame.Instance.Player
                    ? $"You kill {combatResult.Defender}"
                    : $"The {combatResult.Attacker} kills ");
        }

        private void UpdateDungeonViewModel() => 
            DungeonView.UpdateModel(SlashNetGame.Instance.Player.ToModel);

        private void GenerateNewDungeon()
        {
            DataServices.PlayerRepository.AddOrUpdate(SlashNetGame.Instance.Player);
            SlashNetGame.Instance.CombatResultCallback(DealWithCombatResult);
            SlashNetGame.Instance.NewDungeon();
            _dungeon = SlashNetGame.Instance.Dungeon;

            _playerDungeonWalker = new PlayerDungeonWalker(WalkSpeed);

            _dungeonDrawer = new TextureDungeonDrawer(_dungeon, Image);
            _dungeonDrawer.Draw();
            
        }

        public void ResetGame()
        {
            SlashNetGame.Reset();
            GenerateNewDungeon();
        }

        // Update is called once per frame
        private void Update()
        {
            UpdateDungeonViewModel();

            if (SlashNetGame.Instance.PopupOpen)
                return;

            if (!_playerDungeonWalker.Walk())
            {
                SlashNetGame.Instance.Player.ResetFromDungeonLevelUp();
                GenerateNewDungeon();
            }

            _dungeonDrawer?.Draw();
        }
    }
}