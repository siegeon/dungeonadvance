using System;
using System.Collections.Generic;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.Maybe;
using BusinessTycoonSim;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Code.DataAccess.Base
{
    public class StoreRepository : IRepository<StoreData>
    {
        public StoreData Load(int i)
        {
            var key = $"{i}StoreData";
            var storeData = PlayerPrefs.GetString(key);
            return JsonConvert.DeserializeObject<StoreData>(storeData);
        }
        
        public List<StoreData> LoadAll()
        {
            throw new NotImplementedException();
        }
        
        public void Save(StoreData t)
        {
            var key = $"{t.Id}StoreData";
            var data = JsonConvert.SerializeObject(t);
            PlayerPrefs.SetString(key, data);
        }

        public int Count { get; }
        public StoreData GetOrNew(int id)
        {
            throw new NotImplementedException();
        }

        public StoreData Get(int index)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<StoreData> All()
        {
            throw new NotImplementedException();
        }

        public void Add(StoreData data)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, StoreData data)
        {
            throw new NotImplementedException();
        }

        public void Remove(StoreData data)
        {
            throw new NotImplementedException();
        }

        public void AddOrUpdate(StoreData entity)
        {
            throw new NotImplementedException();
        }

        public void Update(StoreData entity)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public void Replace(int index, StoreData data)
        {
            throw new NotImplementedException();
        }

        Maybe<StoreData> IReadOnlyRepository<StoreData>.Get(Guid index)
        {
            throw new NotImplementedException();
        }
    }
}