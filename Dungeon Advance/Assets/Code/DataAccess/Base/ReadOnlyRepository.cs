﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.Maybe;
using UnityEngine;

namespace Assets.Code.DataAccess.Base
{
    ///  <summary> This is a read only repository it is intented to load the assests that have been embedded into the assembly. </summary>
    ///  <typeparam name="T">         Generic type parameter. </typeparam>
    ///  <typeparam name="TDatabase"> Type of the database. </typeparam>
    public abstract class ReadOnlyRepository<T, TDatabase> :
        ScriptableObject,
        IReadOnlyRepository<T> where T : class, IEntity, new()
        where TDatabase : ScriptableObject
    {
        [SerializeField]
        [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local")]
        protected List<T> Database = new List<T>();

        public int Count => Database.Count;
        public Maybe<T> Get(Guid index) => Database
                .FirstOrDefault(x => x.Id == index)
                .ToMaybe();

      
        public static TDatabase GetDatabase(string dbpath, string dbName)
        {
            var dbFullPath = $@"{dbpath}/{dbName}";
            var db = Resources.Load(dbFullPath, typeof(TDatabase)) as TDatabase;

            if (db == null)
            {
                db = CreateInstance<TDatabase>();
            }

            return db;
        }
    }
}