using System;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.Maybe;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Assets.Code.DataAccess.Base
{
    /// <inheritdoc />
    ///  <summary> This repository manages a database written to the application path. It is a read write database and data elements will be updated any time the local cache changes. </summary>
    ///  <typeparam name="T"> Generic type parameter. </typeparam>
    public class JsonRepository<T> : IRepository<T> where T : class, IEntity, new()
    {
        

        private readonly IPersistance _persistance;

        private Dictionary<Guid, T> _data;

        private Dictionary<Guid, T> Data => _data ?? (_data = LoadDataFromDisk());

        public int Count => Data.Count;

        public JsonRepository(IPersistance persistance)
        {
            _persistance = persistance;
        }    

        private Dictionary<Guid, T> LoadDataFromDisk()
        {
            return _persistance.Load().Case(
                some: JsonConvert.DeserializeObject<Dictionary<Guid, T>>,
                none: () =>  new Dictionary<Guid, T>()
            );

        }

        /// <summary> Enumerates all in this collection. </summary>
        ///
        /// <returns> An enumerator that allows foreach to be used to process all in this collection.
        /// </returns>
        public IEnumerable<T> All() => Data.Values;

        /// <summary> Removes the given entity from the collection. </summary>
        ///
        /// <param name="entity"> The entity to add. </param>
        public void Remove(T entity)
        {
            Data.Remove(entity.Id);
            Save();
        }

        /// <summary> Updates the given entity. </summary>
        ///
        /// <param name="entity"> The entity to update. </param>
        public void AddOrUpdate(T entity)
        {
            if (Data.ContainsKey(entity.Id))
            {
                Data[entity.Id] = entity;
            }
            else
            {
                Data.Add(entity.Id, entity);
            }
            Save();
        }

        private void Save()
        {
            _persistance.Save(JsonConvert.SerializeObject(_data, Formatting.Indented));
            _data = null;
        }

        /// <summary> Gets an entity by Id, if that entity can not be found then it will be created in its
        /// default state.
        /// </summary>
        ///
        /// <typeparam name="T"> Generic type parameter. </typeparam>
        /// <param name="index"> The identifier. </param>
        ///
        /// <returns> The or new. </returns>
        public Maybe<T> Get(Guid index) => Data.ContainsKey(index) ?
                Maybe<T>.Some(Data[index]) :
                Maybe<T>.None;
    }
}