using System.IO;
using Assets.Code.Maybe;
using Assets.Code.Utilities;
using UnityEngine;

namespace Assets.Code.DataAccess.Base
{
    public class FileWrighter : IPersistance
    {
        private readonly string _path;
        private readonly string _fullPath;

        public FileWrighter(string resourceName, string resourceType)
        {
        _path = $@"{Application.persistentDataPath}/{resourceType}";
        _fullPath = $@"{ _path}/{resourceName}_{resourceType}.txt";
        }

        public Maybe<string> Load()
        {
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);

            if (!System.IO.File.Exists(_fullPath))
                return Maybe<string>.None;

            return Maybe<string>.Some(System.IO.File.ReadAllText(_fullPath));
        }

        public void Save(string data)
        {
            data.GuardString(nameof(data));
            System.IO.File.WriteAllText(_fullPath, data);
        }
    }

    public interface IPersistance
    {
        Maybe<string> Load();
        void Save(string data);
    }
}