﻿using System;
using System.Collections.Generic;
using Assets.Code.DataAccess.Base;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.Maybe;
using Assets.Code.Utilities;

namespace Assets.Code.DataAccess.Repositories
{
    public class MappingRepository<TGameObject, TModel> : 
        IRepository<TGameObject> where TGameObject : 
        class where TModel : class, IEntity, new()
    {
        protected readonly JsonRepository<TModel> _dataAccess;
        protected readonly IModelConverter<TModel, TGameObject> _modelConverter;

        public MappingRepository(JsonRepository<TModel> dataAccess, IModelConverter<TModel, TGameObject> modelConverter)
        {
            _dataAccess = dataAccess;
            _modelConverter = modelConverter;
        }

        public int Count => _dataAccess.Count;

        public Maybe<TGameObject> Get(Guid id) =>
            _dataAccess.Get(id).Case(
                some: model => _modelConverter.ToGameObject(model).ToMaybe(),
                none: () => Maybe<TGameObject>.None);


        public IEnumerable<TGameObject> All() =>
            _modelConverter.ToGameObject(_dataAccess.All());

        public void Remove(TGameObject entity)
        {
            entity.GuardObjectNull(nameof(entity));
            _dataAccess.Remove(_modelConverter.ToModel(entity));
        }

        public void AddOrUpdate(TGameObject entity)
        {
            entity.GuardObjectNull(nameof(entity));
            _dataAccess.AddOrUpdate(_modelConverter.ToModel(entity));
        }
    }
}