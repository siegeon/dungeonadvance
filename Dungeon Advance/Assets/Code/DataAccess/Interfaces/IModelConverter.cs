﻿using System.Collections.Generic;

namespace Assets.Code.DataAccess.Interfaces
{
    /// <summary> Interface that defines methods to allow for conversions between game objects and models. </summary>
    ///
    /// <typeparam name="TModel">      Type of the model. </typeparam>
    /// <typeparam name="TGameObject"> Type of the game object. </typeparam>
    public interface IModelConverter<TModel, TGameObject>
    {
        TModel ToModel(TGameObject persisted);
        TGameObject ToGameObject(TModel model);
        IEnumerable<TModel> ToModel(IEnumerable<TGameObject> persisted);
        IEnumerable<TGameObject> ToGameObject(IEnumerable<TModel> model);
    }
}