using System;
using Assets.Code.Maybe;

namespace Assets.Code.DataAccess.Interfaces
{
    /// <summary>
    ///     Interface for read only repository. Read only repositories are used during run time to read .asset
    ///     database. These databases are part of the assembly and can not be modified at run time.
    /// </summary>
    /// <typeparam name="T"> Generic type parameter. </typeparam>
    public interface IReadOnlyRepository<T> where T : class
    {
        int Count { get; }
        Maybe<T> Get(Guid index);
    }
}