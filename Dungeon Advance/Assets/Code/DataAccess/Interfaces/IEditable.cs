﻿
using System;

namespace Assets.Code.DataAccess.Interfaces
{
    /// <summary> To be an entity in this game you must have an Id. This is important to anything that must be saved in a repository. </summary>
    public interface IEntity
    {
        Guid Id { get; }
    }
}