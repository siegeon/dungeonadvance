using System;
using System.Collections.Generic;
using Assets.Code.DataAccess.Interfaces;

namespace Assets.Code.DataAccess
{
    /// <inheritdoc />
    ///  <summary> Standard CRUD operations </summary>
    ///  <typeparam name="T"> Generic type parameter. </typeparam>
    public interface IRepository<T> : IReadOnlyRepository<T> where T : class 
    {
        IEnumerable<T> All(); 
        void Remove(T entity);
        void AddOrUpdate(T entity);
        
    }
}