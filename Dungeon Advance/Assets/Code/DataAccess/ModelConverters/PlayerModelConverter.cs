﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Player;

namespace Assets.Code.DataAccess.ModelConverters
{
    public class PlayerModelConverter : IModelConverter<PlayerModel, PlayerCharacter>
    {
        public PlayerModel ToModel(PlayerCharacter persisted) =>
            persisted.ToModel;

        public PlayerCharacter ToGameObject(PlayerModel model) =>
            new PlayerCharacter(model);


        public IEnumerable<PlayerModel> ToModel(IEnumerable<PlayerCharacter> persisted) =>
            persisted.Select(ToModel);


        public IEnumerable<PlayerCharacter> ToGameObject(IEnumerable<PlayerModel> model) =>
            model.Select(ToGameObject);

    }
}