﻿using System.Collections.Generic;
using System.Linq;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Items.Weapons;

namespace Assets.Code.DataAccess.ModelConverters
{
    public class WeaponModelConverter : IModelConverter<WeaponModel, Weapon>
    {
        public WeaponModel ToModel(Weapon persisted) =>
            persisted.ToModel;
            
        public Weapon ToGameObject(WeaponModel model) =>
            new Weapon(model);

        public IEnumerable<WeaponModel> ToModel(IEnumerable<Weapon> persisted) =>
            persisted.Select(ToModel);

        public IEnumerable<Weapon> ToGameObject(IEnumerable<WeaponModel> model) =>
            model.Select(ToGameObject);
    }
}