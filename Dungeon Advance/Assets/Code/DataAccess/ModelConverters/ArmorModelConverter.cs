﻿using System.Collections.Generic;
using System.Linq;
using Assets.Code.DataAccess.Interfaces;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Items.Armor;

namespace Assets.Code.DataAccess.ModelConverters
{
    public class ArmorModelConverter : IModelConverter<ArmorModel, Armor>
    {
        public ArmorModel ToModel(Armor persisted) =>
            persisted.ToModel;

        public Armor ToGameObject(ArmorModel model) =>
            new Armor(model);

        public IEnumerable<ArmorModel> ToModel(IEnumerable<Armor> persisted) =>
            persisted.Select(ToModel);

        public IEnumerable<Armor> ToGameObject(IEnumerable<ArmorModel> model) =>
            model.Select(ToGameObject);
    }
}