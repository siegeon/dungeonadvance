﻿using Assets.Code.DataAccess.Base;
using Assets.Code.SlashNet;
using Assets.Code.SlashNet.Monsters;
using Assets.Code.SlashNet.Monsters.Concrete;
using Assets.Code.SlashNet.Monsters.Types;

namespace Assets.Code.DataAccess.Repositories
{
    public class MonsterRepository : ReadOnlyRepository<MonsterTemplate, MonsterRepository>
    {
        public IMonster Random()
        {
            return new SimpleMonster(Database[UnityEngine.Random.Range(0, Database.Count)], SlashNetGame.Instance.Player.DungeonLevel);
        }
    }
}