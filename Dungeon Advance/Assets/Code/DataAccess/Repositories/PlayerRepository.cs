﻿using System;
using System.Linq;
using Assets.Code.DataAccess.Base;
using Assets.Code.DataAccess.ModelConverters;
using Assets.Code.Maybe;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Factories;
using Assets.Code.SlashNet.Factories.Specifications;
using Assets.Code.SlashNet.Items.Armor;
using Assets.Code.SlashNet.Items.Weapons;
using Assets.Code.SlashNet.Player;

namespace Assets.Code.DataAccess.Repositories
{
    public class PlayerRepository : MappingRepository<PlayerCharacter, PlayerModel>
    {
        private readonly IRepository<Weapon> _weaponRepository;
        private readonly IRepository<Armor> _armorRepository;

        public PlayerRepository(IPersistance persistance, IRepository<Weapon> weaponRepository, IRepository<Armor> armorRepository) : base(
            new JsonRepository<PlayerModel>(persistance),
            new PlayerModelConverter())
        {
            _weaponRepository = weaponRepository;
            _armorRepository = armorRepository;
        }

        public Maybe<PlayerCharacter> GetFirst() =>
            _dataAccess.All().Any() ? 
                Get(_dataAccess.All().First().Id) : 
                Get(Guid.Empty);


        public new void AddOrUpdate(PlayerCharacter playerCharacter)
        {
            _weaponRepository.AddOrUpdate(playerCharacter.Wielded);
            _armorRepository.AddOrUpdate(playerCharacter.Worn);
            base.AddOrUpdate(playerCharacter);
        }

        public new Maybe<PlayerCharacter> Get(Guid id) =>
            _dataAccess.Get(id)
                .Case(
                    some: model => model.ToMaybe(),
                    none: () => Maybe<PlayerModel>.None)
                .Map(model => Tuple.Create(model, _weaponRepository.Get(model.WieldedId), _armorRepository.Get(model.WornId)).ToMaybe())
                .Map(tuple =>
                {
                    var playerCharacter = _modelConverter.ToGameObject(tuple.Item1);
                    
                    playerCharacter.Wield(tuple.Item2.Case(
                        some: w => w,
                        none: () => WeaponBuilder.Configure(WeaponSpecifications.PlayerWeaponDefault).Build()));

                    playerCharacter.Wear(tuple.Item3.Case(
                        some: a => a, 
                        none:() => ArmorBuilder.Configure(ArmorSpecifications.Chest).Build()));

                    return playerCharacter;
                });




    }
}