﻿using Assets.Code.DataAccess.Base;
using Assets.Code.DataAccess.ModelConverters;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Items.Armor;

namespace Assets.Code.DataAccess.Repositories
{
    public class ArmorRepository : MappingRepository<Armor, ArmorModel>
    {
        public ArmorRepository(IPersistance persistance) : base(new JsonRepository<ArmorModel>(persistance), new ArmorModelConverter()) { }
    }
}