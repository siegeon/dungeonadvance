﻿using Assets.Code.DataAccess.Base;
using Assets.Editor.MasterData;

namespace Assets.Code.DataAccess.Repositories
{
    public class ManagerRepository : ReadOnlyRepository<ManagerMasterData, ManagerRepository>
    {

    }
}