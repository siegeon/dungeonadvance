﻿using Assets.Code.DataAccess.Base;
using Assets.Code.DataAccess.ModelConverters;
using Assets.Code.MVC.Models;
using Assets.Code.SlashNet.Items.Weapons;

namespace Assets.Code.DataAccess.Repositories
{
    public class WeaponRepository : MappingRepository<Weapon, WeaponModel>
    {
        public WeaponRepository(IPersistance persistance) : base (new JsonRepository<WeaponModel>(persistance), new WeaponModelConverter()){}
    }
}