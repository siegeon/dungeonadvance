﻿using System;
using Assets.Code.DataAccess.Base;
using Assets.Code.DataAccess.Repositories;
using StoreRepository = Assets.Code.DataAccess.Repositories.StoreRepository;

namespace Assets.Code.DataAccess
{
    public class DataServices
    {
        private static readonly Lazy<DataServices> Lazy = new Lazy<DataServices>(() => new DataServices());
        private readonly WeaponRepository _weaponRepository;
        private readonly PlayerRepository _playerRepository;
        private readonly ArmorRepository _armorRepository;


        private DataServices()
        {
            _armorRepository = new ArmorRepository(new FileWrighter("ArmorRerpository", "Database"));
            _weaponRepository = new WeaponRepository(new FileWrighter("WeaponRepository", "Database"));
            _playerRepository = new PlayerRepository(new FileWrighter("PlayerRepository", "Database"), _weaponRepository, _armorRepository);
        }   

        private static DataServices Instance => Lazy.Value;

        #region Read Wright Repositories

        /// <summary> Gets the player model repository. </summary>
        ///
        /// <value> The player model repository. </value>
        public static PlayerRepository PlayerRepository => Instance._playerRepository;

        /// <summary> Gets the weapon repository. </summary>
        ///
        /// <value> The weapo repository. </value>
        public static WeaponRepository WeapoRepository => Instance._weaponRepository;
        #endregion

        #region Read Only Repositories
        private readonly MonsterRepository _monstersDataDatabase = MonsterRepository.GetDatabase("Database", "MonsterDatabase");
        private readonly StoreRepository _stores = StoreRepository.GetDatabase("Database", "StoreDatabase.asset");
        private readonly ManagerRepository _managers = ManagerRepository.GetDatabase("Database", "ManagerDatabase.asset");
 
        /// <summary> Gets the monsters. </summary>
        ///
        /// <value> The monsters. </value>
        public static MonsterRepository Monsters => Instance._monstersDataDatabase;

        /// <summary> Gets the stores. </summary>
        ///
        /// <value> The stores. </value>
        public static StoreRepository Stores => Instance._stores;

        /// <summary> Gets the managers. </summary>
        ///
        /// <value> The managers. </value>
        public static ManagerRepository Managers => Instance._managers;

        #endregion







    }
}
