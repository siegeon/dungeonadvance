﻿using SlashNet.Dungeon;
using UnityEngine;

namespace Assets.Unity3dInterfaces
{
    public interface ITexture2D
    {
        void SetPixel(int x, int y, Color color);
        void SetPixels(Color32[] colors);
        void Apply();
        Texture Texture { get; }
    }
}
