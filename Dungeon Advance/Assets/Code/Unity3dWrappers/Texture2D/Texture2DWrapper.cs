﻿using Assets.Dungeon;
using SlashNet.Dungeon;
using UnityEngine;

namespace Assets.Unity3dInterfaces
{

    /// <summary> A texture 2d wrapper class that implements an interface for improved testability. </summary>
    public class Texture2DWrapper : ITexture2D
    {
        private readonly Texture2D _texture2D;

        public Texture2DWrapper(int width, int height)
        {
            _texture2D = new Texture2D(width, height, TextureFormat.ARGB32, false);
        }

        public void SetPixel(int x, int y, Color color)
        {
            _texture2D.SetPixel(x, y, color);
        }

        public void SetPixels(Color32[] colors)
        {
            _texture2D.SetPixels32(colors);
        }

        public void Apply()
        {
            _texture2D.Apply();
        }

        public Texture Texture
        {
            get { return _texture2D; }
        }
    }
}