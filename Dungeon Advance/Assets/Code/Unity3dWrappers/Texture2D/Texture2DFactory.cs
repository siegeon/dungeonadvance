﻿namespace Assets.Unity3dInterfaces
{
    public class Texture2DFactory
    {
        public static ITexture2D New(int width, int height)
        {
            return new Texture2DWrapper(width, height);
        }
    }
}