using System.Collections.Generic;
using Assets.Code.Dungeon.PathFinder;
using Assets.Structures;

namespace Assets.Dungeon.PathFinder.Interfaces
{
    internal interface IPathFinder
    {
        #region Properties
        bool Stopped
        {
            get;
        }

        HeuristicFormula Formula
        {
            get;
            set;
        }

        bool Diagonals
        {
            get;
            set;
        }

        bool HeavyDiagonals
        {
            get;
            set;
        }

        int HeuristicEstimate
        {
            get;
            set;
        }

        bool PunishChangeDirection
        {
            get;
            set;
        }

        bool ReopenCloseNodes
        {
            get;
            set;
        }

        bool TieBreaker
        {
            get;
            set;
        }

        int SearchLimit
        {
            get;
            set;
        }

        #endregion

        #region Methods
        void FindPathStop();
        List<PathFinderNode> FindPath(Point start, Point end);
        #endregion

    }
}
