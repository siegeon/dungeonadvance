using System;
using System.Collections.Generic;
using Assets.Dungeon.PathFinder.Algorithms;
using Assets.Dungeon.PathFinder.Interfaces;
using Assets.Structures;

namespace Assets.Code.Dungeon.PathFinder.Algorithms
{
    public class PathFinderFast : IPathFinder
    {
        #region Structs

        internal struct PathFinderNodeFast
        {
            #region Variables Declaration

            public int F; // f = gone + heuristic
            public int G;
            public ushort Px; // Parent
            public ushort Py;
            public byte Status;

            #endregion
        }

        #endregion

        #region Variables Declaration

        // Heap variables are initializated to default, but I like to do it anyway
        private byte[,] _mGrid;
        private PriorityQueueB<int> _mOpen;
        private readonly List<PathFinderNode> _mClose = new List<PathFinderNode>();
        private bool _mStop;
        private int _mHoriz;
        private bool _mDiagonals;
        private PathFinderNodeFast[] _mCalcGrid;
        private byte _mOpenNodeValue = 1;
        private byte _mCloseNodeValue = 2;

        //Promoted local variables to member variables to avoid recreation between calls
        private int _mH;
        private int _mLocation;
        private int _mNewLocation;
        private ushort _mLocationX;
        private ushort _mLocationY;
        private ushort _mNewLocationX;
        private ushort _mNewLocationY;
        private int _mCloseNodeCounter;
        private ushort _mGridX;
        private ushort _mGridY;
        private ushort _mGridXMinus1;
        private ushort _mGridYLog2;
        private bool _mFound;

        private sbyte[,] _mDirection = {{0, -1}, {1, 0},  {0, 1}, 
                                        {-1, 0},          {1, -1}, 
                                        {1, 1},  {-1, 1}, {-1, -1}};

        private int _mEndLocation;
        private int _mNewG;
        private readonly byte[,] _backupGrid;

        #endregion

        #region Constructors

        public PathFinderFast(byte[,] grid)
        {
            if (grid == null)
                throw new Exception("Grid cannot be null");
            _mGrid = grid;
            _backupGrid = grid;

            ResetGrid(grid);
        }

        private void ResetGrid(byte[,] grid)
        {
            _mGridX = (ushort) (_mGrid.GetUpperBound(0) + 1);
            _mGridY = (ushort) (_mGrid.GetUpperBound(1) + 1);
            if (Math.Log(_mGridX, 2) != (int) Math.Log(_mGridX, 2) || Math.Log(_mGridY, 2) != (int) Math.Log(_mGridY, 2))
            {
                var power2Grid = new byte[ToNextNearest(_mGridX), ToNextNearest(_mGridY)];
                for (var xIndex = 0; xIndex < _mGridX; xIndex++)
                for (var yIndex = 0; yIndex < _mGridY; yIndex++)
                    power2Grid[xIndex, yIndex] = grid[xIndex, yIndex];

                _mGrid = power2Grid;
                _mGridX = (ushort) (power2Grid.GetUpperBound(0) + 1);
                _mGridY = (ushort) (power2Grid.GetUpperBound(1) + 1);
            }


            _mGridXMinus1 = (ushort) (_mGridX - 1);
            _mGridYLog2 = (ushort) Math.Log(_mGridY, 2);


            if (_mCalcGrid == null || _mCalcGrid.Length != _mGridX * _mGridY)
                _mCalcGrid = new PathFinderNodeFast[_mGridX * _mGridY];

            _mOpen = new PriorityQueueB<int>(new ComparePfNodeMatrix(_mCalcGrid));
        }

        #endregion

        private int ToNextNearest(int x)
        {
            if (x < 0) return 0;
            --x;
            x |= x >> 1;
            x |= x >> 2;
            x |= x >> 4;
            x |= x >> 8;
            x |= x >> 16;
            return x + 1;
        }

        #region Properties

        public bool Stopped { get; private set; } = true;

        public HeuristicFormula Formula { get; set; } = HeuristicFormula.Manhattan;

        public bool Diagonals
        {
            get { return _mDiagonals; }
            set
            {
                _mDiagonals = value;
                _mDirection = _mDiagonals ? 
                    new sbyte[,] {{0, -1}, {1, 0}, {0, 1}, {-1, 0}, {1, -1}, {1, 1}, {-1, 1}, {-1, -1}} : 
                    new sbyte[,] {{0, -1}, {1, 0}, {0, 1}, {-1, 0}};
            }
        }

        public bool HeavyDiagonals { get; set; } = false;

        public int HeuristicEstimate { get; set; } = 2;

        public bool PunishChangeDirection { get; set; } = false;

        public bool ReopenCloseNodes { get; set; } = true;

        public bool TieBreaker { get; set; } = false;

        public int SearchLimit { get; set; } = 2000;

        #endregion

        #region Methods

        public void FindPathStop()
        {
            _mStop = true;
        }

        public List<PathFinderNode> FindPath(Point start, Point end)
        {
            lock (this)
            {
                // Is faster if we don't clear the matrix, just assign different values for open and close and ignore the rest
                // I could have user Array.Clear() but using unsafe code is faster, no much but it is.
                //fixed (PathFinderNodeFast* pGrid = tmpGrid) 
                //    ZeroMemory((byte*) pGrid, sizeof(PathFinderNodeFast) * 1000000);

                _mFound = false;
                _mStop = false;
                Stopped = false;
                _mCloseNodeCounter = 0;
                _mOpenNodeValue += 2;
                _mCloseNodeValue += 2;
                _mOpen.Clear();
                _mClose.Clear();

                _mLocation = (start.Y << _mGridYLog2) + start.X;
                _mEndLocation = (end.Y << _mGridYLog2) + end.X;
                _mCalcGrid[_mLocation].G = 0;
                _mCalcGrid[_mLocation].F = HeuristicEstimate;
                _mCalcGrid[_mLocation].Px = (ushort) start.X;
                _mCalcGrid[_mLocation].Py = (ushort) start.Y;
                _mCalcGrid[_mLocation].Status = _mOpenNodeValue;

                _mOpen.Push(_mLocation);
                while (_mOpen.Count > 0 && !_mStop)
                {
                    _mLocation = _mOpen.Pop();

                    //Is it in closed list? means this node was already processed
                    if (_mCalcGrid[_mLocation].Status == _mCloseNodeValue)
                        continue;

                    _mLocationX = (ushort) (_mLocation & _mGridXMinus1);
                    _mLocationY = (ushort) (_mLocation >> _mGridYLog2);

                    if (_mLocation == _mEndLocation)
                    {
                        _mCalcGrid[_mLocation].Status = _mCloseNodeValue;
                        _mFound = true;
                        break;
                    }

                    if (_mCloseNodeCounter > SearchLimit)
                    {
                        Stopped = true;

                        return null;
                    }

                    if (PunishChangeDirection)
                        _mHoriz = _mLocationX - _mCalcGrid[_mLocation].Px;

                    //Lets calculate each successors
                    for (var i = 0; i < (_mDiagonals ? 8 : 4); i++)
                    {
                        _mNewLocationX = (ushort) (_mLocationX + _mDirection[i, 0]);
                        _mNewLocationY = (ushort) (_mLocationY + _mDirection[i, 1]);
                        _mNewLocation = (_mNewLocationY << _mGridYLog2) + _mNewLocationX;

                        if (_mNewLocationX >= _mGridX || _mNewLocationY >= _mGridY)
                            continue;

                        if (_mCalcGrid[_mNewLocation].Status == _mCloseNodeValue && !ReopenCloseNodes)
                            continue;

                        // Unbreakeable?
                        if (_mGrid[_mNewLocationX, _mNewLocationY] == 0)
                            continue;

                        if (HeavyDiagonals && i > 3)
                            _mNewG = _mCalcGrid[_mLocation].G + (int) (_mGrid[_mNewLocationX, _mNewLocationY] * 2.41);
                        else
                            _mNewG = _mCalcGrid[_mLocation].G + _mGrid[_mNewLocationX, _mNewLocationY];

                        if (PunishChangeDirection)
                        {
                            if (_mNewLocationX - _mLocationX != 0)
                                if (_mHoriz == 0)
                                    _mNewG += Math.Abs(_mNewLocationX - end.X) + Math.Abs(_mNewLocationY - end.Y);
                            if (_mNewLocationY - _mLocationY != 0)
                                if (_mHoriz != 0)
                                    _mNewG += Math.Abs(_mNewLocationX - end.X) + Math.Abs(_mNewLocationY - end.Y);
                        }

                        //Is it open or closed?
                        if (_mCalcGrid[_mNewLocation].Status == _mOpenNodeValue ||
                            _mCalcGrid[_mNewLocation].Status == _mCloseNodeValue)
                            if (_mCalcGrid[_mNewLocation].G <= _mNewG)
                                continue;

                        _mCalcGrid[_mNewLocation].Px = _mLocationX;
                        _mCalcGrid[_mNewLocation].Py = _mLocationY;
                        _mCalcGrid[_mNewLocation].G = _mNewG;

                        switch (Formula)
                        {
                            case HeuristicFormula.Manhattan:
                                _mH = HeuristicEstimate *(Math.Abs(_mNewLocationX - end.X) + Math.Abs(_mNewLocationY - end.Y));
                                break;
                            case HeuristicFormula.MaxDXDY:
                                _mH = HeuristicEstimate * Math.Max(Math.Abs(_mNewLocationX - end.X),Math.Abs(_mNewLocationY - end.Y));
                                break;
                            case HeuristicFormula.DiagonalShortCut:
                                var hDiagonal = Math.Min(Math.Abs(_mNewLocationX - end.X),Math.Abs(_mNewLocationY - end.Y));
                                var hStraight = Math.Abs(_mNewLocationX - end.X) + Math.Abs(_mNewLocationY - end.Y);
                                _mH = HeuristicEstimate * 2 * hDiagonal + HeuristicEstimate * (hStraight - 2 * hDiagonal);
                                break;
                            case HeuristicFormula.Euclidean:
                                _mH = (int) (HeuristicEstimate * Math.Sqrt(Math.Pow(_mNewLocationY - end.X, 2) + Math.Pow(_mNewLocationY - end.Y, 2)));
                                break;
                            case HeuristicFormula.EuclideanNoSQR:
                                _mH = (int) (HeuristicEstimate * (Math.Pow(_mNewLocationX - end.X, 2) + Math.Pow(_mNewLocationY - end.Y, 2)));
                                break;
                            case HeuristicFormula.Custom1:
                                var dxy = new Point(Math.Abs(end.X - _mNewLocationX), Math.Abs(end.Y - _mNewLocationY));
                                var orthogonal = Math.Abs(dxy.X - dxy.Y);
                                var diagonal = Math.Abs((dxy.X + dxy.Y - orthogonal) / 2);
                                _mH = HeuristicEstimate * (diagonal + orthogonal + dxy.X + dxy.Y);
                                break;
                            default:
                                throw new ArgumentException("A huristinc formula must be chosen.", nameof(Formula));
                        }

                        if (TieBreaker)
                        {
                            var dx1 = _mLocationX - end.X;
                            var dy1 = _mLocationY - end.Y;
                            var dx2 = start.X - end.X;
                            var dy2 = start.Y - end.Y;
                            var cross = Math.Abs(dx1 * dy2 - dx2 * dy1);
                            _mH = (int) (_mH + cross * 0.001);
                        }

                        _mCalcGrid[_mNewLocation].F = _mNewG + _mH;

                        _mOpen.Push(_mNewLocation);

                        _mCalcGrid[_mNewLocation].Status = _mOpenNodeValue;
                    }

                    _mCloseNodeCounter++;
                    _mCalcGrid[_mLocation].Status = _mCloseNodeValue;
                }

                if (_mFound)
                {
                    _mClose.Clear();

                    var fNodeTmp = _mCalcGrid[(end.Y << _mGridYLog2) + end.X];
                    var fNode = new PathFinderNode
                    {
                        F = fNodeTmp.F,
                        G = fNodeTmp.G,
                        H = 0,
                        Px = fNodeTmp.Px,
                        Py = fNodeTmp.Py,
                        X = end.X,
                        Y = end.Y
                    };

                    while (fNode.X != fNode.Px || fNode.Y != fNode.Py)
                    {
                        _mClose.Add(fNode);
                        var posX = fNode.Px;
                        var posY = fNode.Py;
                        fNodeTmp = _mCalcGrid[(posY << _mGridYLog2) + posX];
                        fNode.F = fNodeTmp.F;
                        fNode.G = fNodeTmp.G;
                        fNode.H = 0;
                        fNode.Px = fNodeTmp.Px;
                        fNode.Py = fNodeTmp.Py;
                        fNode.X = posX;
                        fNode.Y = posY;
                    }

                    _mClose.Add(fNode);


                    Stopped = true;
                    ResetGrid(_backupGrid);
                    return _mClose;
                }

                Stopped = true;
                return null;
            }
        }

        #endregion

        #region Inner Classes

        internal class ComparePfNodeMatrix : IComparer<int>
        {
            #region Variables Declaration

            private readonly PathFinderNodeFast[] _mMatrix;

            #endregion

            #region Constructors

            public ComparePfNodeMatrix(PathFinderNodeFast[] matrix)
            {
                _mMatrix = matrix;
            }

            #endregion

            #region IComparer Members

            public int Compare(int a, int b)
            {
                if (_mMatrix[a].F > _mMatrix[b].F)
                    return 1;
                if (_mMatrix[a].F < _mMatrix[b].F)
                    return -1;
                return 0;
            }

            #endregion
        }

        #endregion
    }
}