using System;

namespace Assets.Code.Dungeon.PathFinder
{
    public struct PathFinderNode : IEquatable<PathFinderNode>
    {
        #region Variables Declaration

        public int G { get; set; }
        public int H { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Px { get; set; }
        public int Py { get; set; }

        public int F { get; set; }

        #endregion

        public bool Equals(PathFinderNode other) => G == other.G && H == other.H && X == other.X && Y == other.Y && Px == other.Px && Py == other.Py && F == other.F;

        public override bool Equals(object obj) => obj is PathFinderNode && Equals((PathFinderNode) obj);

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = G;
                hashCode = (hashCode * 397) ^ H;
                hashCode = (hashCode * 397) ^ X;
                hashCode = (hashCode * 397) ^ Y;
                hashCode = (hashCode * 397) ^ Px;
                hashCode = (hashCode * 397) ^ Py;
                hashCode = (hashCode * 397) ^ F;
                return hashCode;
            }
        }

        public static bool operator ==(PathFinderNode left, PathFinderNode right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(PathFinderNode left, PathFinderNode right)
        {
            return !left.Equals(right);
        }
    }
}