﻿using Assets.Code.DataAccess;
using Assets.Code.SlashNet.Dungeon;
using Assets.Code.SlashNet.Items;
using Assets.Code.SlashNet.Monsters;
using Assets.Structures;
using SlashNet.Dungeon;

namespace Assets.Dungeon
{
    interface IDungeonPopulator
    {
        void Populate(IDungeon dungeon);
    }

    class DefaultDungeonPopulator : IDungeonPopulator
    {
        public void Populate(IDungeon dungeon)
        {
            for (int i = 0; i < 10; i++)
            {
                var s = new Gem("green");
                Point p = dungeon.GetRandomOpenPoint();
                dungeon[p].Items.Add(s);
                dungeon[p].Items.Add(new Combestible(100, "banana"));
                IMonster monster = DataServices.Monsters.Random();
                monster.Location = p;
                dungeon.Add(monster);

            } 
        }
    }
}