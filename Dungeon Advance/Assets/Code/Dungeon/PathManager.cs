using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.Dungeon.PathFinder;
using Assets.Dungeon.PathFinder;
using Assets.Dungeon.PathFinder.Interfaces;
using Assets.Structures;


namespace Assets.Dungeon
{
    class PathManager
    {
        private readonly IPathFinder _pathFinder;
        private bool _working;
        private IPoints _path;
        private Point _from;
        private Point _to;

        public PathManager(IPathFinder pathFinder)
        {
            _pathFinder = pathFinder;
            _path = new Points();
        }
        public bool GetPath(Point from, Point to, out IPoints path)
        {
            _from = from;
            _to = to;
            path = new Points();

            //do we have a path?
            if (_path.Any())
            {
                path = _path;
                _path = new Points();
                return true;
            }

            //are we looking for one? 
            if (_working)
            {
                return false;
            }

            if (!_working)
            {
                _working = true;
                EZThread.EzThread.ExecuteInBackground(GetPathNodes, SetLocalPath);
            }

            return GetPath(from, to, out path);

        }

        private object GetPathNodes()
        {
            return _pathFinder.FindPath(_from, _to);
        }

        private void SetLocalPath(object pathNodes)
        {
            _working = false;
            List<PathFinderNode> nodes = (List<PathFinderNode>)pathNodes;
            _path = new Points(nodes.Select(x => new Point(x.X, x.Y))); 
        }
    }
}