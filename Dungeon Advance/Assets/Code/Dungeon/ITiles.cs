﻿using Assets.Code.Maybe;
using Assets.SlashNet.Dungeon;
using Assets.Structures;
using SlashNet.Dungeon;

namespace Assets.Dungeon
{
    public interface ITiles
    {
        ITiles GetWithMonsters { get; }
        ITiles GetNotVisited { get; }
        Maybe<Tile> GetClosest { get; }
        IPoints Points { get; }
        Maybe<Tile> GetRoomNotVisited { get; }
    }
}