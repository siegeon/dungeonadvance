﻿using System.Collections.Generic;
using System.Linq;
using Assets.Code.Maybe;
using Assets.Code.SlashNet;
using Assets.SlashNet.Dungeon;
using Assets.Structures;
using SlashNet.Dungeon;

namespace Assets.Dungeon
{
    class Tiles : ITiles
    {
        private IEnumerable<Tile> _tiles { get; }

        public Tiles(IEnumerable<Tile> tiles)
        {
            _tiles = tiles;
        }

        /// <summary> Gets the monsters. </summary>
        /// <remarks>
        ///     This method is closed under the set of all tiles objects. It is mapping one painter object into another painters object. This allows chaining.
        /// </remarks>
        /// 
        /// <returns> The monsters. </returns>
        public ITiles GetWithMonsters =>
            _tiles.All(x => x.HasMonster) ? //cpu cycles for memory
                this :
            new Tiles(_tiles.Where(x=>x.HasMonster));

        /// <summary> Gets the get not visited. </summary>
        ///  <remarks>
        ///     This method is closed under the set of all tiles objects. It is mapping one painter object into another painters object. This allows chaining.
        /// </remarks>
        ///
        /// <value> The get not visited. </value>
        public ITiles GetNotVisited =>
            _tiles.All(x => !x.Visited) ? //cpu cycles for memory
                this :
                new Tiles(_tiles.Where(x => !x.Visited));

        /// <summary> Gets the get closest. </summary>
        /// <remarks>
        ///     Map reduce function
        /// </remarks>
        ///
        /// <value> The get closest. </value>
        public Maybe<Tile> GetClosest => _tiles.FirstOrDefault().ToMaybe();

        public Maybe<Tile> GetRoomNotVisited => _tiles
            .Where(x => (x.Type == TileType.Floor || x.Type == TileType.Floor) && !x.Visited)
            .Where(y=>y.Point.Distance(SlashNetGame.Instance.Player.Location)<2)
            .FirstOrNone();

        public IPoints Points => new Points(_tiles.Select(x=>x.Point));
    }
}
