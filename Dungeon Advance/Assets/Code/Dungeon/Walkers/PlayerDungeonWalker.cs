using System.Linq;
using Assets.Code.Dungeon.PathFinder.Algorithms;
using Assets.Code.SlashNet;
using Assets.Dungeon;
using Assets.Dungeon.PathFinder.Algorithms;
using Assets.Structures;
using SlashNet;
using UnityEngine;

namespace Assets.Code.Dungeon.Walkers
{
    public class PlayerDungeonWalker : IDungeonWalker
    {
        private readonly IPoints _possibilities;
        
        private readonly float _walkingSpeed;
        private float _nextStepTime;
        private IPoints _path;
        private readonly PathManager _pathManager;

        public PlayerDungeonWalker(float walkingSpeed)
        {
            _possibilities = new Points();
            _walkingSpeed = (1/walkingSpeed);
            _nextStepTime = CalculateNextStep();
            _path = new Points();
            _pathManager = new PathManager(new PathFinderFast(SlashNetGame.Instance.Dungeon.Grid));
        }

        private float CalculateNextStep()
        {
            return Time.time + _walkingSpeed;
        }

        /// <summary> Walks the dungeon, unless it cant for some reason. </summary>
        ///
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Walk()
        {
            //if we cant yet step
            if (!(Time.time > _nextStepTime))
                return true;

            _nextStepTime = CalculateNextStep();

            return Step(SlashNetGame.Instance.Dungeon.GetPossibleMoves(SlashNetGame.Instance.Player.Location));
        }

        private bool Step(ITiles tiles)
        {
            _possibilities.AddUnique(tiles.GetNotVisited.Points);

            if (!_possibilities.Any())
                return false;

            //find closest monster 
            tiles.GetWithMonsters.GetClosest.Case(
                some: (tile) =>
                {
                    SlashNetGame.Instance.MoveTo(tile.Point);
                },
                //no monster
                none: () =>
                {
                    //do we have unvisited rooms?
                    tiles.GetRoomNotVisited.Case(
                        some: tile =>
                        {
                            //move to the unvisited room
                            SlashNetGame.Instance.MoveTo(tile.Point);
                            _possibilities.Remove(tile.Point);
                        },
                        none: () =>
                        {
                            //there was no unvisited room
                            var nextMove = GetNextMove();
                            SlashNetGame.Instance.MoveTo(nextMove);
                            _possibilities.Remove(nextMove);
                        });
                });

            return true;
        }

        private Point GetNextMove()
        {
            if (_path.Any())
                return GetNextPathMove();

            //there was no path, lets check stored possiblities.
            var nextMove = _possibilities.GetClosest(SlashNetGame.Instance.Player.Location);

            //distance is how many squares the element can move
            if (nextMove.Distance <= 2)
                return nextMove.Point;

            //nothing within distance, time to build a new path
            IPoints points;
            if (_pathManager.GetPath(SlashNetGame.Instance.Player.Location, nextMove.Point, out points))
            {
                _path = points;
            }
            else
            {
                //wait here while we think of the next place to go
                return SlashNetGame.Instance.Player.Location;
            }

            return GetNextMove();
        }

        private Point GetNextPathMove()
        {
            var nextStep = _path.GetClosest(SlashNetGame.Instance.Player.Location);

            _path.Remove(nextStep.Point);

            if (nextStep.Distance > 2)
                return GetNextMove();

            return nextStep.Point;
        }
    }
}