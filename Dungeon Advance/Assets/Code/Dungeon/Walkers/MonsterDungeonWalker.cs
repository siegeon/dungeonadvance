using Assets.Code.SlashNet;
using Assets.Code.SlashNet.Monsters;
using Assets.Code.SlashNet.Monsters.Concrete;
using Assets.Structures;
using SlashNet;

namespace Assets.Code.Dungeon
{
    public class MonsterDungeonWalker : IDungeonWalker
    {
        private readonly IMonster _monster;
        
        public MonsterDungeonWalker(IMonster monster)
        {
            _monster = monster;   
        }

        public bool Walk()
        {

            var target = SlashNetGame.Instance.Player.Location;
            if (target.ManhattanDistance(_monster.Location) < 8)
                if (target.X == _monster.Location.X)
                {
                    if (target.Y < _monster.Location.Y)
                    {
                        if (CanMove( _monster.Location.Translate(0, -1)))
                            MoveTo( _monster.Location.Translate(0, -1));
                        else if (CanMove( _monster.Location.Translate(1, -1)))
                            MoveTo( _monster.Location.Translate(1, -1));
                        else if (CanMove( _monster.Location.Translate(-1, -1)))
                            MoveTo( _monster.Location.Translate(-1, -1));
                        return true;
                    }

                    if (CanMove( _monster.Location.Translate(0, 1)))
                        MoveTo( _monster.Location.Translate(0, 1));
                    else if (CanMove( _monster.Location.Translate(1, 1)))
                        MoveTo( _monster.Location.Translate(1, 1));
                    else if (CanMove( _monster.Location.Translate(-1, 1)))
                        MoveTo( _monster.Location.Translate(-1, 1));
                    return true;
                }
                else if (target.Y == _monster.Location.Y)
                {
                    if (target.X < _monster.Location.X)
                    {
                        if (CanMove( _monster.Location.Translate(-1, 0)))
                            MoveTo( _monster.Location.Translate(-1, 0));
                        else if (CanMove( _monster.Location.Translate(-1, -1)))
                            MoveTo( _monster.Location.Translate(-1, -1));
                        else if (CanMove( _monster.Location.Translate(-1, 1)))
                            MoveTo( _monster.Location.Translate(-1, 1));
                        return true;
                    }

                    if (CanMove( _monster.Location.Translate(1, 0)))
                        MoveTo( _monster.Location.Translate(1, 0));
                    else if (CanMove( _monster.Location.Translate(1, -1)))
                        MoveTo( _monster.Location.Translate(1, -1));
                    else if (CanMove( _monster.Location.Translate(1, 1)))
                        MoveTo( _monster.Location.Translate(1, 1));
                    return true;
                }
                else
                {
                    if (target.Y < _monster.Location.Y)
                        if (target.X < _monster.Location.X)
                        {
                            if (CanMove( _monster.Location.Translate(-1, -1)))
                                MoveTo( _monster.Location.Translate(-1, -1));
                            else if (CanMove( _monster.Location.Translate(-1, 0)))
                                MoveTo( _monster.Location.Translate(-1, 0));
                            else if (CanMove( _monster.Location.Translate(0, -1)))
                                MoveTo( _monster.Location.Translate(0, -1));
                            return true;
                        }
                        else
                        {
                            if (CanMove( _monster.Location.Translate(1, -1)))
                                MoveTo( _monster.Location.Translate(1, -1));
                            else if (CanMove( _monster.Location.Translate(1, 0)))
                                MoveTo( _monster.Location.Translate(1, 0));
                            else if (CanMove( _monster.Location.Translate(0, -1)))
                                MoveTo( _monster.Location.Translate(0, -1));
                            return true;
                        }

                    if (target.X < _monster.Location.X)
                    {
                        if (CanMove( _monster.Location.Translate(-1, 1)))
                            MoveTo( _monster.Location.Translate(-1, 1));
                        else if (CanMove( _monster.Location.Translate(-1, 0)))
                            MoveTo( _monster.Location.Translate(-1, 0));
                        else if (CanMove( _monster.Location.Translate(0, 1)))
                            MoveTo( _monster.Location.Translate(0, 1));
                        return true;
                    }

                    if (CanMove( _monster.Location.Translate(1, 1)))
                        MoveTo( _monster.Location.Translate(1, 1));
                    else if (CanMove( _monster.Location.Translate(1, 0)))
                        MoveTo( _monster.Location.Translate(1, 0));
                    else if (CanMove( _monster.Location.Translate(0, 1)))
                        MoveTo( _monster.Location.Translate(0, 1));
                    return true;
                }

            var dx = Random.D(1, 3) - 2;
            var dy = Random.D(1, 3) - 2;

            var newPoint = _monster.Location.Translate(dx, dy);

            if (CanMove(newPoint))
            {
                SlashNetGame.Instance.Dungeon[_monster.Location].Monster = null;
                _monster.Location = newPoint;
                SlashNetGame.Instance.Dungeon[newPoint].Monster = _monster;
            }
            return true;
        }

        private static bool CanMove(Point newPoint) => SlashNetGame.Instance.Dungeon.IsOpen(newPoint);

        private void MoveTo(Point newPoint)
        {
            if (SlashNetGame.Instance.Player.Location == newPoint)
                return;

            SlashNetGame.Instance.Dungeon[_monster.Location].Monster = null;
            _monster.Location = newPoint;
            SlashNetGame.Instance.Dungeon[newPoint].Monster = _monster;
        }
    }
}