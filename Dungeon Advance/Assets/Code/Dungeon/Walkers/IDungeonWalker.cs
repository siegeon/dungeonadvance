namespace Assets.Code.Dungeon
{
    public interface IDungeonWalker
    {
        bool Walk();
    }
}