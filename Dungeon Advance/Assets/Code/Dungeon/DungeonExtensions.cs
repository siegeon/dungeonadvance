﻿using System.Collections.Generic;
using Assets.Code.SlashNet.Dungeon;
using Assets.Dungeon;
using Assets.SlashNet.Dungeon;
using Assets.Structures;

namespace Assets.Code.Dungeon
{
    public static class DungeonExtensions
    {
        public static ITiles GetPossibleMoves(this IDungeon dungeon, Point location) => 
            GetSurrounding(dungeon, location, 1);

        private static ITiles GetSurrounding(IDungeon dungeon, Point location, int range)
        {
            var moves = new List<Tile>(8);

            for (var dx = (location.X > 0 ? -range : 0); dx <= (location.X < dungeon.Width - range ? range : 0); ++dx)
            for (var dy = (location.Y > 0 ? -range : 0); dy <= (location.Y < dungeon.Height - range ? range : 0); ++dy)
            {
                if (dx == 0 && dy == 0)
                    continue;

                var tile = dungeon[location.X + dx, location.Y + dy];

                if (!dungeon.Contains(tile.Point))
                    continue;

                if (!dungeon.IsObstacle(tile.Point))
                    moves.Add(tile);
            }

            return new Tiles(moves);
        }
    }
}