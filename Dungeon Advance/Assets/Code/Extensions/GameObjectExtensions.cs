﻿using System;
using UnityEngine;

namespace Assets.Code.Extensions
{
    public static class GameObjectExtensions
    {
        public static T GetComponentFromGameObjectWithName<T>(string gameObjectName)
        {
            var gameObject = GameObject.Find(gameObjectName);
            if (gameObject == null)
                throw new Exception($"Unable to find a game object named {gameObjectName} in the canvas.");
            var component = gameObject.GetComponent<T>();
            if (component == null)
                throw new Exception($"The the {gameObjectName} did not have the {typeof(T)} component attached.");
            return component;
        }
    }
}