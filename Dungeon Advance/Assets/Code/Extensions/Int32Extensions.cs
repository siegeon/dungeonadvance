using System;

namespace Assets.Extensions
{
    public static class Int32Extensions
    {
        public static int Clamp(this int value, int min, int max)
        {
            if (min > max)
                throw new ArgumentOutOfRangeException("The minimum must be less than or equal to the maximum.");
            return Math.Max(min, Math.Min(max, value));
        }
    }
}