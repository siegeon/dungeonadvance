﻿using System.Collections.Generic;
using SlashNet;

namespace Assets.Extensions
{
    public static class ColorExtensions
    {
        private static readonly Dictionary<SNColor, UnityEngine.Color> ColorTranslation =
            new Dictionary<SNColor, UnityEngine.Color>
            {
                {SNColor.Black, UnityEngine.Color.black},
                {SNColor.Blue, UnityEngine.Color.blue},
                {SNColor.Brown, UnityEngine.Color.cyan},
                {SNColor.Gray, UnityEngine.Color.gray},
                {SNColor.Green, UnityEngine.Color.green},
                {SNColor.LightGray, UnityEngine.Color.gray},
                {SNColor.Orange, UnityEngine.Color.cyan},
                {SNColor.Purple, UnityEngine.Color.cyan},
                {SNColor.Red, UnityEngine.Color.red},
                {SNColor.White, UnityEngine.Color.white},
                {SNColor.Yellow, UnityEngine.Color.yellow},

            };


        public static UnityEngine.Color AsUnityColor(this SNColor color)
        {

            return ColorTranslation[color];
        }
    }
}
