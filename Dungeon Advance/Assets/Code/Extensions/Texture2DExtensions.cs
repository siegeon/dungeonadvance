using System;
using Assets.SlashNet.Dungeon;
using Assets.Unity3dInterfaces;
using SlashNet.Dungeon;
using UnityEngine;

namespace Assets.Extensions
{
    public static class Texture2DExtensions
    {
        public static Color GetTileColor(Tile tile)
        {
            if (tile == null)
            {
                throw new ArgumentNullException(nameof(tile));
            }

            switch (tile.Type)
            {
                case TileType.Rock:
                    return Color.clear;
                case TileType.Floor:
                    return tile.Visited ? Color.gray : Color.white;
                case TileType.Door:
                    return tile.Visited ? Color.blue : Color.black;
                case TileType.Invisible:
                    break;
                case TileType.VWall:
                    break;
                case TileType.HWall:
                    break;
                case TileType.NECorner:
                    break;
                case TileType.NWCorner:
                    break;
                case TileType.SECorner:
                    break;
                case TileType.SWCorner:
                    break;
                case TileType.NTWall:
                    break;
                case TileType.ETWall:
                    break;
                case TileType.STWall:
                    break;
                case TileType.WTWall:
                    break;
                case TileType.XWall:
                    break;
                case TileType.Corridor:
                    return Color.white;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        

            return Color.magenta;
        }
    }
}