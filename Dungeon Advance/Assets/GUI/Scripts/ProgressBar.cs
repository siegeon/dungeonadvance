﻿using Assets.Code.SlashNet.Common;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.GUI.Scripts
{
    public class ProgressBar : MonoBehaviour
    {
        public Slider ProgressSlider;
        private int _currentValue;
        private int _maxValue;

        // Use this for initialization
        void Start ()
        {
            _currentValue = 0;
            ProgressSlider.minValue = 0;
            ProgressSlider.value = _currentValue;
            UpdateMaxValue(100);
        }

        public void UpdateProgress(int current, int max)
        {
            //UnityEngine.Debug.Log(current + " " + max);
            if(_maxValue != max)
                UpdateMaxValue(max);
            UpdateCurrentValue(current);
        }

        private void UpdateMaxValue(int maxValue)
        {
            _maxValue = maxValue;
            ProgressSlider.maxValue = maxValue;
        }

        private void UpdateCurrentValue(int currentValue)
        {
            _currentValue = currentValue;
            if (_currentValue > currentValue)
                ProgressSlider.value = currentValue;

        }

        // Update is called once per frame
        void Update () {
            ProgressSlider.value = Mathf.MoveTowards(ProgressSlider.value, _currentValue, _currentValue*Time.deltaTime*1);
        }
    }
}
