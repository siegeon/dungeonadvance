using System;
using SlashNet;

namespace Assets.Structures
{
    public struct Point : IEquatable<Point>
    {
        public static Point Default = new Point(-1,-1);

        public readonly int X;
        public readonly int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point Translate(int x, int y)
        {
            return new Point(X + x, Y + y);
        }

        public Point Translate(Point p)
        {
            return Translate(p.X, p.Y);
        }

        public int ManhattanDistance(Point p)
        {
            return Math.Abs(p.X - X) + Math.Abs(p.Y - Y);
        }
        
        public Point Translate(DirectionType dir)
        {
            switch (dir)
            {
                case DirectionType.North:
                    return new Point(X, Y - 1);
                case DirectionType.South:
                    return new Point(X, Y + 1);
                case DirectionType.West:
                    return new Point(X - 1, Y);
                case DirectionType.East:
                    return new Point(X + 1, Y);
                default:
                    return new Point();
            }
            
        }

        internal double Distance(Point point) => Distance(point.X, point.Y);
        internal double Distance(int x, int y) => (Math.Abs(x - X) + Math.Abs(y - Y));

        public override string ToString() => $"{X},{Y}";

        #region IEquatable
        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Point && Equals((Point) obj);
        }

        public static bool operator ==(Point a, Point b)
        {
            return (a.Equals(b));
        }

        public static bool operator !=(Point a, Point b)
        {
            return (!a.Equals(b));
        }
        public bool Equals(Point other)
        {
            return X == other.X && Y == other.Y;
        }
        #endregion



    }
}