using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.Maybe;
using Assets.Code.SlashNet;
using SlashNet.Dungeon;

namespace Assets.Structures
{
    public class Points : IPoints
    {
        private List<Point> _points { get; }

        public Points() : this(new List<Point>()) { }

        public Points(IEnumerable<Point> points)
        {
            _points = points.ToList();
        }




        public IPoints AddUnique(IPoints points)
        {
            _points.AddRange(points.Where(point => !_points.Contains(point)));
            return this;
        }

        public void Remove(Point point)
        {
            _points.Remove(point);
        }

        public WeightedPoint GetClosest(Point instancePcPosition) =>
            _points.Where(x=>SlashNetGame.Instance.Dungeon[x].Type == TileType.Floor || SlashNetGame.Instance.Dungeon[x].Type == TileType.Door)
                .GroupBy(x => x.Distance(SlashNetGame.Instance.Player.Location))
                .OrderBy(x => x.Key)
                .SelectMany(x => x.Select(y => new WeightedPoint(y, x.Key)))
                .FirstOrNone()
                .Case(
                    some: point => point,
                    none: () =>
                    {
                        var point = _points.GroupBy(x => x.Distance(SlashNetGame.Instance.Player.Location))
                            .OrderBy(x => x.Key)
                            .SelectMany(x => x.Select(y => new WeightedPoint(y, x.Key)))
                            .FirstOrDefault();
                        return point;
                    });

        public Point GetNextMove { get; }

        public IEnumerator<Point> GetEnumerator()
        {
            return _points.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}