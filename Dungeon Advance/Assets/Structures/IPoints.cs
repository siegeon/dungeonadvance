using System.Collections.Generic;

namespace Assets.Structures
{
    public interface IPoints : IEnumerable<Point>
    {
        IPoints AddUnique(IPoints points);
        void Remove(Point point);
        WeightedPoint GetClosest(Point instancePcPosition);
    }
}