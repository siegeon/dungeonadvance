namespace Assets.Structures
{
    public class WeightedPoint
    {
        public readonly double Distance;
        public readonly Point Point;

        public WeightedPoint(int x, int y, double  distance) : this(new Point(x, y), distance) {}

        public WeightedPoint(Point point, double distance) 
        {
            Point = point;
            Distance = distance;
        }

        public override string ToString() => $"{nameof(Point)}:{Point} - {nameof(Distance)}:{Distance}";


    }
}