using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Monsters
{
    public class RustMonster : AMonster
    {
        public RustMonster(MonsterTemplate template)
            : base(template)
        {
            if (template.Name != "rust monster")
                throw new Exception("Tried to make a rust monster with a wrong template");
        }

        internal override bool OnAttacked(Being attacker)
        {
            if (attacker.Wielded != null)
                attacker.Wielded.ApplyEffect(EffectType.Rust);
            return true;
        }

    }
}
