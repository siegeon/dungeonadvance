using System;
using System.Collections.Generic;
namespace SlashNet.Monsters
{
    public enum MonsterClass
    {
        Insect, Blob, Cockatrice, Canine, SphereEye, Feline, GremlinGargoyle, Humanoid, MinorDemon, Jelly,
        Kobold, Leprechaun, Mimic, Nymph, Orc, Piercer, Quadruped, Rodent, Arachnid, Lurker, Equine, Vortex,
        Worm, FantasticalInsect, Light, Zruty, AngelicBeing, BatBird, Centaur, Dragon, ElementalStalker,
        Fungi, Gnome, LargeHumanoid, Jabberwock, KeystoneKop, Lich, Mummy, Naga, Ogre, Amoeboid, QuantumMechanic,
        RustDisenchanter, Snake, Troll, UmberHulk, Vampire, Wraith, Xorn, Apelike, Zombie, Golem, HumanElf, GhostShade,
        MajorDemon, SeaMonster, Lizard, QuestLeader, QuestNemesis, QuestGuardian
    }

    public class MonsterClassDictionary
    {
        private static MonsterClassDictionary _Instance;
        public static void Initialise()
        {
            if (_Instance != null)
                throw new Exception("Can't initialise more than once");
            _Instance = new MonsterClassDictionary();
        }
        public static MonsterClassDictionary Instance
        {
            get
            {
                return _Instance;
            }
        }

        private Dictionary<MonsterClass, string> _Data;

        private MonsterClassDictionary()
        {
            _Data = new Dictionary<MonsterClass, string>();
            _Data.Add(MonsterClass.Amoeboid, "P");
            _Data.Add(MonsterClass.AngelicBeing, "A");
            _Data.Add(MonsterClass.Apelike, "Y");
            _Data.Add(MonsterClass.Arachnid, "s");
            _Data.Add(MonsterClass.BatBird, "B");
            _Data.Add(MonsterClass.Blob, "b");
            _Data.Add(MonsterClass.Canine, "d");
            _Data.Add(MonsterClass.Centaur, "C");
            _Data.Add(MonsterClass.Cockatrice, "c");
            _Data.Add(MonsterClass.Dragon, "D");
            _Data.Add(MonsterClass.ElementalStalker, "E");
            _Data.Add(MonsterClass.Equine, "u");
            _Data.Add(MonsterClass.FantasticalInsect, "x");
            _Data.Add(MonsterClass.Feline, "f");
            _Data.Add(MonsterClass.Fungi, "F");
            _Data.Add(MonsterClass.GhostShade, " ");
            _Data.Add(MonsterClass.Gnome, "G");
            _Data.Add(MonsterClass.Golem, "'");
            _Data.Add(MonsterClass.GremlinGargoyle, "g");
            _Data.Add(MonsterClass.HumanElf, "@");
            _Data.Add(MonsterClass.Humanoid, "h");
            _Data.Add(MonsterClass.Insect, "a");
            _Data.Add(MonsterClass.Jabberwock, "J");
            _Data.Add(MonsterClass.Jelly, "j");
            _Data.Add(MonsterClass.KeystoneKop, "K");
            _Data.Add(MonsterClass.Kobold, "k");
            _Data.Add(MonsterClass.LargeHumanoid, "H");
            _Data.Add(MonsterClass.Leprechaun, "l");
            _Data.Add(MonsterClass.Lich, "L");
            _Data.Add(MonsterClass.Light, "y");
            _Data.Add(MonsterClass.Lizard, ":");
            _Data.Add(MonsterClass.Lurker, "t");
            _Data.Add(MonsterClass.MajorDemon, "&");
            _Data.Add(MonsterClass.Mimic, "m");
            _Data.Add(MonsterClass.MinorDemon, "i");
            _Data.Add(MonsterClass.Mummy, "M");
            _Data.Add(MonsterClass.Naga, "N");
            _Data.Add(MonsterClass.Nymph, "n");
            _Data.Add(MonsterClass.Ogre, "O");
            _Data.Add(MonsterClass.Orc, "o");
            _Data.Add(MonsterClass.Piercer, "p");
            _Data.Add(MonsterClass.Quadruped, "q");
            _Data.Add(MonsterClass.QuantumMechanic, "Q");
            _Data.Add(MonsterClass.QuestGuardian, "@");
            _Data.Add(MonsterClass.QuestLeader, "@");
            _Data.Add(MonsterClass.QuestNemesis, null);
            _Data.Add(MonsterClass.Rodent, "r");
            _Data.Add(MonsterClass.RustDisenchanter, "R");
            _Data.Add(MonsterClass.SeaMonster, ";");
            _Data.Add(MonsterClass.Snake, "S");
            _Data.Add(MonsterClass.SphereEye, "e");
            _Data.Add(MonsterClass.Troll, "T");
            _Data.Add(MonsterClass.UmberHulk, "U");
            _Data.Add(MonsterClass.Vampire, "V");
            _Data.Add(MonsterClass.Vortex, "v");
            _Data.Add(MonsterClass.Worm, "w");
            _Data.Add(MonsterClass.Wraith, "W");
            _Data.Add(MonsterClass.Xorn, "X");
            _Data.Add(MonsterClass.Zombie, "Z");
            _Data.Add(MonsterClass.Zruty, "z");
            

        }

        public string this[MonsterClass c]
        {
            get
            {
                return _Data[c];
            }
        }
    }
}