using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Monsters
{




    /// <summary>
    /// The physical types of attack that occur.
    /// These are namely for display purposes.
    /// </summary>
    public enum AttackType
    {
        Claw, Bite, Kick, Zap, Touch, BareHands, Weapon
    }

    /// <summary>
    /// What can happen as the result of an attack.
    /// </summary>
    internal enum AttackResult
    {
        /// <summary>
        /// The attack killed the target.
        /// </summary>
        Kill, 

        /// <summary>
        /// The attack mised completely.
        /// </summary>
        Miss, 

        /// <summary>
        /// The attack hit (and therefore must have done damage).
        /// </summary>
        Hit, 

        /// <summary>
        /// Nothing happened.
        /// </summary>
        Nothing
    }

    public struct Attack
    {
        public static Attack BareHanded = new Attack(AttackType.BareHands, EffectType.Physical, new Die(2));
        public static Attack Weapon = new Attack(AttackType.Weapon, EffectType.Physical, new Die(-1));

        IRollable _Damage;

        private AttackType _Type;
        private EffectType _Effect;

        public AttackType Type
        {
            get
            {
                return _Type;
            }
        }

        public EffectType Effect
        {
            get
            {
                return _Effect;
            }
        }

        public int Damage
        {
            get
            {
                return _Damage.Roll();
            }
        }

        public Attack(AttackType type, EffectType effect, IRollable damage)
        {
            _Effect = effect;
            _Damage = damage;
            _Type = type;
        }

    }
}
