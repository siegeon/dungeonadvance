using System;
using System.Collections.Generic;
using System.Text;
using SlashNet.Player;

namespace SlashNet.Monsters
{
    public class FireAnt : AMonster
    {
        public FireAnt(MonsterTemplate template)
            : base(template)
        {
        }

        internal override AttackResult OnAttacks(Being defender, int attack)
        {
            if (attack == 0)
                return base.OnAttacks(defender, attack);
            
            //Only fire on the second attack, if the player resists, don't do damage!
            if (Random.D(21) >= ToHit(defender))
            {
                return AttackResult.Miss;
            }
            else
            {
                int dmg = _Attacks[1].Damage + DamageBonus(defender);
                SlashNetGame.Instance.UI.Pline(
                        Grammar.MakeAttackString(this, defender, _Attacks[attack].Type));
                if (defender.Apply(EffectType.Fire))
                {
                    if (defender.LoseHP(dmg))
                        return AttackResult.Kill;
                    return AttackResult.Hit;
                }
                return AttackResult.Nothing;
            }

        }

    }

    public class SoldierAnt : AMonster
    {
        public SoldierAnt(MonsterTemplate template)
            : base(template)
        {
        }

        //Sometimes poison the player.
        internal override AttackResult OnAttacks(Being defender, int attack)
        {
            AttackResult result = base.OnAttacks(defender, 0);
            if (result == AttackResult.Hit)
            {
                if (Random.D(20) > 17)
                {
                    defender.Apply(EffectType.Poison);
                }
            }
            return result;


        }

    }
}
