using System;
using System.Collections.Generic;
using System.Text;

using System.Reflection;

using SlashNet.Dungeon;

namespace SlashNet.Monsters
{
    public abstract class AMonster : Being, ILocatable
    {

        protected int _Level;

        protected Tile _Home;

        public Tile Home
        {
            get
            {
                return _Home;
            }
            set
            {
                _Home = value;
            }
        }

        protected int _Speed;
        private int _Experience;
        protected int _MovePoints;

        public int Level
        {
            get { return _Level; }
        }

        protected AMonster(MonsterTemplate template)
        {
            AdjustAttributes(template);
        }

        public override string ToString()
        {
            return _Name;
        }

        private int _AC;
        private string _Name;

        protected virtual void AdjustAttributes(MonsterTemplate template)
        {
            //_Permonst = template;

            _View = template.View;
            _Name = template.Name;
            _Attacks = template.Attacks;

            _MovePoints = 0;
            _Speed = template.Speed;

            AdjustLevel(template);

            AdjustHealth(template);

            _Experience = template.Experience;
            _AC = template.AC;

        }

        protected virtual void AdjustHealth(MonsterTemplate template)
        {
            if (_Level == 0)
                _Health = _MaxHealth = Random.D(1, 4);
            else
                _Health = _MaxHealth = Random.D(_Level, 4);
        }

        protected virtual void AdjustLevel(MonsterTemplate template)
        {
            int tmp, tmp2;
            int xp = template.Experience;

            tmp = template.Level;
            tmp2 = SlashNetGame.Instance.CurrentLevel - tmp;

            if (tmp2 < 0)
            {
                tmp--;
                xp -= (2 * tmp) + 1;
                if (tmp <= 8)
                    xp -= 50;
            }
            else
            {
                tmp += (tmp2 / 5);
                xp += (2 * tmp) + 1;
                if (tmp > 8)
                    xp += 50;
            }
            _Experience = xp;

            tmp2 = SlashNetGame.Instance.Player.XLevel - template.Level;
            if (tmp2 > tmp) tmp += tmp2 / 4;

            tmp2 = (3 * template.Level) / 2;
            if (tmp2 > 49) tmp2 = 49;

            if (tmp > tmp2)
                _Level = tmp2;
            else
            {
                if (tmp > 0)
                    _Level = tmp;
                else
                    _Level = 0;
            }
        }

        internal override bool Apply(EffectType effectType)
        {
            bool applies = base.Apply(effectType);
            if (!applies)
            {
                SlashNetGame.Instance.UI.Pline(Grammar.MakePrefix(this) + "isn't affected");
            }
            else
            {
                switch (effectType)
                {
                    case EffectType.Poison:
                        SlashNetGame.Instance.UI.Pline(Grammar.MakePrefix(this) + "looks weaker");
                        break;
                    case EffectType.Fire:
                        SlashNetGame.Instance.UI.Pline(Grammar.MakePrefix(this) + "is on fire");
                        break;
                    case EffectType.Ice:
                        SlashNetGame.Instance.UI.Pline(Grammar.MakePrefix(this) + "looks pretty cold");
                        break;
                }
            }
            return applies;
        }

        private SNPoint _Location;



        private Drawable _View;

        public Drawable View
        {
            get
            {
                return _View;
            }
        }

        internal override bool OnAttacked(Being attacker)
        {
            return true;
        }
        internal override AttackResult OnAttacks(Being defender)
        {
            for (int i = 0; i < _Attacks.Length; i++)
            {
                AttackResult res = OnAttacks(defender, i);
                switch (res)
                {
                    case AttackResult.Kill:
                        defender.Die(this);
                        return AttackResult.Kill;
                    case AttackResult.Miss:
                        SlashNetGame.Instance.UI.Pline(Grammar.MakeMissString(this, defender));
                        defender.OnAttacked(this);
                        break;
                }
            }
            return AttackResult.Nothing;
        }
        internal override AttackResult OnAttacks(Being defender, int attack)
        {
            if (Random.D(20 + attack) >= ToHit(defender))
            {
                return AttackResult.Miss;
            }
            else
            {
                int dmg = _Attacks[attack].Damage + DamageBonus(defender);
                SlashNetGame.Instance.UI.Pline(
                        Grammar.MakeAttackString(this, defender, _Attacks[attack].Type));
                defender.OnAttacked(this);
                if (defender.LoseHP(dmg))
                    return AttackResult.Kill;
                else
                    return AttackResult.Hit;
            }
        }

        #region ILocatable Members

        public SNPoint Location
        {
            get
            {
                return _Location;
            }
            set
            {
                _Location = value;
            }
        }

        #endregion

        internal void Tick()
        {
            _MovePoints += _Speed;
        }

        internal void Move(IDungeon dungeon)
        {
            if (_MovePoints < 12)
                return;
            while (_MovePoints >= 12)
            {
                //A little unfair, if a monster doesn't do 
                //anything it still gives up its move points.
                _MovePoints -= 12;

                SNPoint target = SlashNetGame.Instance.Player.Location;
                if (target.ManhattanDistance(_Location) < 8)
                {
                    if (target.X == _Location.X)
                    {
                        if (target.Y < _Location.Y)
                        {
                            if (CanMove(dungeon, _Location.Translate(0, -1)))
                                MoveTo(dungeon, _Location.Translate(0, -1));
                            else if (CanMove(dungeon, _Location.Translate(1, -1)))
                                MoveTo(dungeon, _Location.Translate(1, -1));
                            else if (CanMove(dungeon, _Location.Translate(-1, -1)))
                                MoveTo(dungeon, _Location.Translate(-1, -1));
                            return;
                        }
                        else
                        {
                            if (CanMove(dungeon, _Location.Translate(0, 1)))
                                MoveTo(dungeon, _Location.Translate(0, 1));
                            else if (CanMove(dungeon, _Location.Translate(1, 1)))
                                MoveTo(dungeon, _Location.Translate(1, 1));
                            else if (CanMove(dungeon, _Location.Translate(-1, 1)))
                                MoveTo(dungeon, _Location.Translate(-1, 1));
                            return;
                        }
                    }
                    else if (target.Y == _Location.Y)
                    {
                        if (target.X < _Location.X)
                        {
                            if (CanMove(dungeon, _Location.Translate(-1, 0)))
                                MoveTo(dungeon, _Location.Translate(-1, 0));
                            else if (CanMove(dungeon, _Location.Translate(-1, -1)))
                                MoveTo(dungeon, _Location.Translate(-1, -1));
                            else if (CanMove(dungeon, _Location.Translate(-1, 1)))
                                MoveTo(dungeon, _Location.Translate(-1, 1));
                            return;
                        }
                        else
                        {
                            if (CanMove(dungeon, _Location.Translate(1, 0)))
                                MoveTo(dungeon, _Location.Translate(1, 0));
                            else if (CanMove(dungeon, _Location.Translate(1, -1)))
                                MoveTo(dungeon, _Location.Translate(1, -1));
                            else if (CanMove(dungeon, _Location.Translate(1, 1)))
                                MoveTo(dungeon, _Location.Translate(1, 1));
                            return;
                        }
                    }
                    else
                    {
                        if (target.Y < _Location.Y)
                        {
                            if (target.X < _Location.X)
                            {
                                if (CanMove(dungeon, _Location.Translate(-1, -1)))
                                    MoveTo(dungeon, _Location.Translate(-1, -1));
                                else if (CanMove(dungeon, _Location.Translate(-1, 0)))
                                    MoveTo(dungeon, _Location.Translate(-1, 0));
                                else if (CanMove(dungeon, _Location.Translate(0, -1)))
                                    MoveTo(dungeon, _Location.Translate(0, -1));
                                return;
                            }
                            else
                            {
                                if (CanMove(dungeon, _Location.Translate(1, -1)))
                                    MoveTo(dungeon, _Location.Translate(1, -1));
                                else if (CanMove(dungeon, _Location.Translate(1, 0)))
                                    MoveTo(dungeon, _Location.Translate(1, 0));
                                else if (CanMove(dungeon, _Location.Translate(0, -1)))
                                    MoveTo(dungeon, _Location.Translate(0, -1));
                                return;
                            }
                        }
                        else
                        {
                            if (target.X < _Location.X)
                            {
                                if (CanMove(dungeon, _Location.Translate(-1, 1)))
                                    MoveTo(dungeon, _Location.Translate(-1, 1));
                                else if (CanMove(dungeon, _Location.Translate(-1, 0)))
                                    MoveTo(dungeon, _Location.Translate(-1, 0));
                                else if (CanMove(dungeon, _Location.Translate(0, 1)))
                                    MoveTo(dungeon, _Location.Translate(0, 1));
                                return;
                            }
                            else
                            {
                                if (CanMove(dungeon, _Location.Translate(1, 1)))
                                    MoveTo(dungeon, _Location.Translate(1, 1));
                                else if (CanMove(dungeon, _Location.Translate(1, 0)))
                                    MoveTo(dungeon, _Location.Translate(1, 0));
                                else if (CanMove(dungeon, _Location.Translate(0, 1)))
                                    MoveTo(dungeon, _Location.Translate(0, 1));
                                return;
                            }
                        }

                    }
                }
                else
                {
                    int dx = Random.D(1, 3) - 2;
                    int dy = Random.D(1, 3) - 2;

                    SNPoint newPoint = Location.Translate(dx, dy);

                    if (CanMove(dungeon, newPoint))
                    {
                        dungeon[Location].Monster = null;
                        Location = newPoint;
                        dungeon[newPoint].Monster = this;
                    }
                }

            }
        }

        private void MoveTo(IDungeon dungeon, SNPoint newPoint)
        {

            if (SlashNetGame.Instance.PCPosition == newPoint)
            {
                OnAttacks(SlashNetGame.Instance.Player);

            }
            else
            {
                dungeon[Location].Monster = null;
                _Location = newPoint;
                dungeon[newPoint].Monster = this;
            }
        }

        public override int DamageBonus(Being versus)
        {
            return 0;
        }

        internal sealed override void Die(Being attacker)
        {
            string prefix = attacker == SlashNetGame.Instance.Player ? "You kill " : "The " + attacker + " kills ";
            SlashNetGame.Instance.UI.Pline(prefix + this);

            SlashNetGame.Instance.Player.Experience += _Experience;
            SlashNetGame.Instance.Dungeon.Remove(this);
        }

        public sealed override int ToHit(Being versus)
        {
            int toHit = versus.AC + 10;
            toHit += _Level;
            if (toHit < 1)
                toHit = 1;
            return toHit;
        }

        public sealed override int AC
        {
            get
            {
                if (_AC < 0)
                {
                    return Random.D(-_AC);
                }
                else
                    return _AC;
            }
        }

        private static bool CanMove(IDungeon dungeon, SNPoint newPoint)
        {
            return dungeon.Contains(newPoint) && !dungeon.IsObstacle(newPoint)
                                && dungeon[newPoint].Monster == null;
        }
    }


    public class MonsterTemplate
    {
        public readonly int Level;
        public readonly int Difficulty;
        public readonly int Speed;
        public readonly Attack[] Attacks;
        public readonly Drawable View;
        public readonly String Name;
        /// <summary>
        /// The type of the monster, used for construction.
        /// </summary>
        public readonly Type Type;
        /// <summary>
        /// How much experience the monster gives.
        /// </summary>
        public readonly int Experience;
        /// <summary>
        /// What the monster is resistant to?
        /// </summary>
        public EffectType[] Resistances;
        public readonly int AC;

        /// <summary>
        /// Constructs a new Monster template with the given set of attributes.
        /// </summary>
        /// <param name="level">The base level of the monster, adjusts other attributes</param>
        /// <param name="difficulty">The difficulty of the monster, decides generation eligibility</param>
        /// <param name="speed">The speed of the monster - 12 is normal</param>
        /// <param name="attacks">The attacks the monster makes in a turn</param>
        /// <param name="view">What the monster looks like, character + color</param>
        /// <param name="name">The name of the monster</param>
        /// <param name="exp">The base amount of experience the monster gives</param>
        /// <param name="type">The type used to create this monster, MUST be a subclass of AMonster</param>
        /// <param name="resistances">All the resistances this monster has</param>
        internal MonsterTemplate(MonsterClass monsterClass, SNColor color, int level, int difficulty, int speed,
            Attack[] attacks, string name, int exp, Type type,
            EffectType[] resistances, int ac)
        {
            if (!(type.IsSubclassOf(typeof(AMonster))))
                throw new Exception("Can't make a monster Template with a type that isn't a monster");

            Level = level;
            Difficulty = difficulty;
            Speed = speed;
            Attacks = attacks;
            View = new Drawable(MonsterClassDictionary.Instance[monsterClass], color);
            Name = name;
            Type = type;
            Experience = exp;
            Resistances = resistances;
            AC = ac;
        }

        internal MonsterTemplate(MonsterClass monsterClass, SNColor color, int level, int difficulty, int speed,
            Attack[] attacks, string name, int exp, Type type, int ac)
            : this(monsterClass, color, level, difficulty, speed, attacks, name, exp, type, new EffectType[] { }, ac)
        {
        }

    }

    public class SimpleMonster : AMonster
    {
        public SimpleMonster(MonsterTemplate template)
            : base(template)
        {
        }
    }





}
