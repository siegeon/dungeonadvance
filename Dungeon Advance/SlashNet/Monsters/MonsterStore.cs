using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Monsters
{
    class MonsterStore
    {

        private static MonsterStore _Instance;

        public static void Initialise()
        {
            _Instance = new MonsterStore();
        }

        public static MonsterStore Instance
        {
            get
            {
                return _Instance;
            }
        }


        private List<MonsterTemplate> _Templates;


        private MonsterStore()
        {
            _Templates = new List<MonsterTemplate>();

            /*
             * Remember MonsterTemplate goes:
             * base level,  difficulty,  speed,   attacks,  drawable,  name,  exp,  type.
             * type should probably be `typeof(SimpleMonster)' for most things, but if the
             * monster has _anything_ special, subclass AMonster and define type as 
             * `typeof(MyNewSuperSpecialAwesomeMonster)'.
             */

            //TODO: d with breath - winter wolf [pup] / hell hound/pup
            #region canines

            _Templates.Add(new MonsterTemplate(
                            MonsterClass.Canine, SNColor.Brown,
                            0, 1, 12, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new Die(2)) },
                            "jackal", 1, typeof(SimpleMonster), 7));

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Canine, SNColor.Brown,
                1, 2, 12, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new Die(4)) },
                "coyote", 11, typeof(SimpleMonster), 7));

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Canine, SNColor.Red,
                0, 1, 15, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new Die(3)) },
                "fox", 4, typeof(SimpleMonster), 7));

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Canine, SNColor.Yellow,
                        4, 5, 16, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new Die(6)) },
                "dingo", 44, typeof(SimpleMonster), 5));

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Canine, SNColor.Brown,
                5, 6, 12, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new CompositeDice(0, 4, 4)) },
                "wolf", 59, typeof(SimpleMonster), 4));

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Canine, SNColor.Brown,
                7, 8, 12, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new CompositeDice(0, 6, 6)) },
                "jackal", 95, typeof(SimpleMonster), 4));

            #endregion

            #region ants

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Insect, SNColor.Brown,
                2, 4, 18, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new Die(4)) },
                "giant ant", 22, typeof(SimpleMonster), 3));

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Insect, SNColor.Blue,
                3, 6, 18, new Attack[] { new Attack(AttackType.Bite, EffectType.Poison, new CompositeDice(0, 4, 4)) },
                "soldier ant", 39, typeof(SoldierAnt), 3));

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Insect, SNColor.Red,
                3, 6, 18, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new CompositeDice(0, 4, 4) ),
                new Attack(AttackType.Bite, EffectType.Fire, new CompositeDice(0, 4, 4)) },
                "fire ant", 36, typeof(FireAnt), 3));


            #endregion

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Lizard, SNColor.Yellow,
                0, 1, 6, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new Die(2)) },
                "newt", 1, typeof(SimpleMonster), 8));



            _Templates.Add(new MonsterTemplate(
                MonsterClass.FantasticalInsect, SNColor.Purple,
                0, 1, 12, new Attack[] { new Attack(AttackType.Zap, EffectType.Lightning, new Die(1)) },
                "grid bug", 1, typeof(SimpleMonster), 9));

            _Templates.Add(new MonsterTemplate(
                MonsterClass.Rodent, SNColor.Brown,
                0, 1, 12, new Attack[] { new Attack(AttackType.Bite, EffectType.Physical, new Die(3)) },
                "sewer rat", 1, typeof(SimpleMonster), 7));



            _Templates.Add(new MonsterTemplate(
                MonsterClass.RustDisenchanter, SNColor.Brown,
                5, 8, 18, new Attack[] { new Attack(AttackType.Touch, EffectType.Rust, Die.Zero), 
                    new Attack(AttackType.Touch, EffectType.Rust, Die.Zero) },
                "rust monster", 68, typeof(RustMonster), 2));






        }

        public AMonster GenerateRandom()
        {

            int i = Random.Instance.Next(_Templates.Count - 1);

            MonsterTemplate t = _Templates[i];

            return t.Type.GetConstructor(new Type[] { typeof(MonsterTemplate) }).Invoke(new object[] { t }) as AMonster;

        }


        public MonsterTemplate this[short id]
        {
            get
            {
                return _Templates[id];
            }
        }

    }
}
