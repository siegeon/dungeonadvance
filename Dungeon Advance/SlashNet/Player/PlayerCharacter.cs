using System;
using System.Collections.Generic;
using System.Text;

using SlashNet.Items;
using SlashNet.Monsters;

namespace SlashNet.Player
{

    public struct Advancement
    {
        internal int Infix, Inrnd;
        internal int Lofix, Lornd;
        internal int Hifix, Hirnd;

        public Advancement(int infix, int inrnd, int lofix, int lornd, int hifix, int hirnd)
        {
            Infix = infix;
            Inrnd = inrnd;
            Lofix = lofix;
            Lornd = lornd;
            Hifix = hifix;
            Hirnd = hirnd;
        }
    }

    

    

    public class PlayerCharacter : Being, ILocatable
    {

        private Role _Role;
        private Race _Race;

        internal Role Role
        {
            get
            {
                return _Role;
            }
        }
        internal Race Race
        {
            get
            {
                return _Race;
            }
        }

        

        internal bool HasIntrinsic(EffectType type)
        {
            return _Intrinsics.Contains(type);
        }

        private int _Hallucinating = 0;

        internal bool Hallucinating
        {
            get { return _Hallucinating > 0; }
        }

        #region attributes

        private int _Strength, _Dexterity, _Constitution, _Intelligence, _Wisdom, _Charisma;

        public int Strength
        {
            get { return _Strength; }
            set { _Strength = value; }
        }
        public int Dexterity
        {
            get { return _Dexterity; }
            set { _Dexterity = value; }
        }
        public int Constitution
        {
            get { return _Constitution; }
            set { _Constitution = value; }
        }
        public int Intelligence
        {
            get { return _Intelligence; }
            set { _Intelligence = value; }
        }
        public int Wisdom
        {
            get { return _Wisdom; }
            set { _Wisdom = value; }
        }
        public int Charisma
        {
            get { return _Charisma; }
            set { _Charisma = value; }
        }

        #endregion

        #region xp

        protected virtual int NewHP()
        {
            int hp, conplus;

            //Initial hp.
            if (_XLevel == 0)
            {
                //Fixed + random.
                hp = _Role.Health.Infix + _Race.Health.Infix;
                if (_Race.Health.Inrnd > 0) hp += Random.D(_Race.Health.Inrnd);
                if (_Role.Health.Inrnd > 0) hp += Random.D(_Role.Health.Inrnd);

                return hp;
            }
            else
            {
                //If we are at the lower end of our role.
                if (_XLevel < _Role.XLevel)
                {
                    hp = _Role.Health.Lofix + _Race.Health.Lofix;
                    if (_Race.Health.Lornd > 0) hp += Random.D(_Race.Health.Lornd);
                    if (_Role.Health.Lornd > 0) hp += Random.D(_Role.Health.Lornd);
                }
                //Otherwise we're at the higher end.
                else
                {
                    hp = _Role.Health.Hifix + _Race.Health.Hifix;
                    if (_Race.Health.Hirnd > 0) hp += Random.D(_Race.Health.Hirnd);
                    if (_Role.Health.Hirnd > 0) hp += Random.D(_Role.Health.Hirnd);
                }
            }


            //Calculate bonus based on constitution.
            if (_Constitution <= 3) conplus = -2;
            else if (_Constitution <= 6) conplus = -1;
            else if (_Constitution <= 14) conplus = 0;
            else if (_Constitution <= 16) conplus = 1;
            else if (_Constitution == 17) conplus = 2;
            else if (_Constitution == 18) conplus = 3;
            else conplus = 4;

            hp += conplus;

            //Never lose hp on level up!
            return (hp < 1 ? 1 : hp);       
        }
        internal void LevelUp(bool incremental)
        {
            if (!incremental)
            {
            }
            else
            {
                _XLevel++;
                if (_Experience >= xpForNextLevel())
                    _Experience = xpForNextLevel() - 1;
                _Health += NewHP();
                _MaxHealth += NewHP();
            }
        }

        public int XLevel
        {
            get { return _XLevel; }
            set { _XLevel = value; }
        }
        private int xpForNextLevel()
        {
            if (_XLevel < 10) return (10 * (1 << _XLevel));
            if (_XLevel < 20) return (10000 * (1 << (_XLevel - 10)));
            return (10000000 * ((_XLevel - 19)));
        }
        private int _XLevel;
        private int _Experience;
        public int Experience
        {
            get
            {
                return _Experience;
            }
            set
            {
                _Experience = value;
                if (value >= xpForNextLevel())
                    LevelUp(true);                
            }
        }

        #endregion

        #region movement

        private int _Speed;
        private int _MovePoints;
        public int Speed
        {
            get { return _Speed; }
            set { _Speed = value; }
        }

        public int MovePoints
        {
            get
            {
                return _MovePoints;
            }
        }

        #endregion

        #region Being

        internal override void Die(Being attacker)
        {
            SlashNetGame.Instance.DYWYPI();
        }

        public override int AC
        {
            get { return 10; }
        }

        public override int ToHit(Being versus)
        {
            int toHit = versus.AC + 10;
            if (_Strength < 6)
                toHit -= 2;
            else if (_Strength < 8)
                toHit -= 1;
            else if (_Strength < 17)
                toHit += 0;
            else
                toHit += 1;

            toHit += (_XLevel < 3 ? 1 : 0);


            return toHit;
        }

        public override int DamageBonus(Being versus)
        {
            int dmg = 0;

            if (_Strength < 6)
                dmg -= 1;
            else if (_Strength == 16 || _Strength == 17)
                dmg += 1;
            else if (_Strength == 18)
                dmg += 2;

            return dmg;
        }

        #endregion

        #region inventory

        private ItemStack _Inventory;

        public ItemStack Inventory
        {
            get { return _Inventory; }
        }

        #endregion

        #region ILocatable

        private SNPoint _Location;
        
        public SNPoint Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        #endregion

        #region name

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        #endregion


        public string Status
        {
            get
            {
                return _Name + "    HP:" + _Health + "(" + _MaxHealth + ")   " + "XP:" + _XLevel + "/" + _Experience;
            }
        }

        public PlayerCharacter(string name, Race race, Role role)
            : base()
        {
            _Race = race;
            _Role = role;
            _Health = _MaxHealth = NewHP();
            _Attacks = new Attack[] { new Attack(AttackType.BareHands, EffectType.Physical, new Die(2)) };
            _XLevel = 1;
            _MovePoints = 0;
            _Intrinsics = new List<EffectType>();
            if(_Role.Intrinsics.ContainsKey(1))
                _Intrinsics.AddRange(_Role.Intrinsics[1]);
            _Intrinsics.AddRange(_Race.Intrinsics);
            
            //_Speed = speed;
            //_Constitution = cons;
            //_MaxHealth = maxHP;
            //_Health = maxHP;
            //_Strength = str;
            _Name = name;
            _Inventory = new ItemStack();
        }


        

        internal void Tick()
        {
            _MovePoints += _Speed;
        }

        internal void Moved()
        {
            //Eat some move points.
            _MovePoints -= 12;

            //Do health generation, a la nethack.
            if(_XLevel > 9)
            {
                if(SlashNetGame.Instance.Ticks % 3 == 0)
                {
                    int heal = _Constitution <= 12 ? 1 : Random.D(_Constitution);
                    if (heal > _XLevel - 9)
                        heal = _XLevel - 9;
                    _Health += heal;
                    if (_Health > _MaxHealth)
                        _Health = _MaxHealth;
                }
            }
            else
            {

                if (SlashNetGame.Instance.Ticks % ((SlashNetGame.MAXULEV + 12) / (_XLevel + 2) + 1) == 0)
                {
                    _Health++;
                    if (_Health > _MaxHealth)
                        _Health = _MaxHealth;
                }
            }
        }

        internal override bool OnAttacked(Being attacker)
        {
            return true;
        }
        internal override AttackResult OnAttacks(Being defender)
        {
            if (Random.D(20) >= ToHit(defender))
            {
                SlashNetGame.Instance.UI.Pline(Grammar.MakeMissString(this, defender));
                defender.OnAttacked(this);
                return AttackResult.Miss;
            }
            else
            {
                int dmg;
                if (_Attacks[0].Type != AttackType.Weapon)
                    dmg = _Attacks[0].Damage + DamageBonus(defender);
                else
                    dmg = _Wielded.Damage(defender) + DamageBonus(defender);
                SlashNetGame.Instance.UI.Pline(Grammar.MakeAttackString(this, defender, _Attacks[0].Type));
                defender.OnAttacked(this);
                if (defender.LoseHP(dmg))
                {
                    defender.Die(this);
                    return AttackResult.Kill;
                }
                else
                    return AttackResult.Hit;
            }
        }
        internal override AttackResult OnAttacks(Being defender, int attack)
        {
            throw new Exception("The player never gets more than one attack.");
        }


        
    }
}
