namespace SlashNet.Player
{
    public class Race
    {
        internal readonly string Noun, Adjective, Collective;
        internal readonly int[] AttrBase, AttrDist;

        internal EffectType[] Intrinsics;
        
        internal readonly Advancement Health, Energy;

        public Race(string noun, string adj, string coll, int[] attrBase, int[] attrDist,
            Advancement hp, Advancement en, EffectType[] intrinsincs)
        {
            Health = hp;
            Energy = en;
            Noun = noun;
            Adjective = adj;
            Collective = coll;
            AttrBase = attrBase;
            AttrDist = attrDist;
            Intrinsics = intrinsincs;
        }

    }
}