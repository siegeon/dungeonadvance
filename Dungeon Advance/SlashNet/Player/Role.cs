using System.Collections.Generic;
namespace SlashNet.Player
{
    public class Role
    {
        internal readonly Advancement Health, EnergyAdv;
        internal readonly string Name;

        private readonly string[] _Ranks;

        internal readonly int XLevel;

        internal readonly int[] AttrBase, AttrDist;

        internal readonly string GodL, GodN, GodC;

        internal readonly Dictionary<int, EffectType[]> Intrinsics;

        internal string Rank(int level)
        {
            return "";
        }

        public Role(string name, string godL, string godN, string godC, int xlevel,
            string[] ranks, Advancement hp, Advancement en, int[] attrBase, int[] attrDist,
            Dictionary<int, EffectType[]> intrinsics)
        {

            Name = name;

            _Ranks = ranks;

            Intrinsics = intrinsics;

            GodL = godL;
            GodN = godN;
            GodC = godC;

            XLevel = xlevel;

            AttrBase = attrBase;
            AttrDist = attrDist;

            Health = hp;
            EnergyAdv = en;
        }

    }
}