using System.Collections.Generic;
namespace SlashNet.Player
{

    public class Roles
    {

        private static Roles _Instance;

        public static void Initialise()
        {
            _Instance = new Roles();
        }

        private Roles()
        {
            _Data = new Dictionary<string, Role>();



            Dictionary<int, EffectType[]> intrinsics = new Dictionary<int, EffectType[]>();
            intrinsics.Add(1, new EffectType[] { EffectType.Poison });
            intrinsics.Add(7, new EffectType[] { EffectType.Speed });
            intrinsics.Add(15, new EffectType[] { EffectType.Stealth });
            Role barbarian = new Role("barbarian",
                "Mitra", "Crom", "Set", 10,
                new string[] { "Barbarian", "Plunderer", "Pillager", "Bandit",
                                "Raider", "Reaver", "Slayer", "Chieftain", "Conqueror" },
                new Advancement(14, 0, 0, 10, 2, 0),
                new Advancement(1, 0, 0, 1, 0, 1),
                new int[] { 16, 7, 7, 15, 16, 6 },
                new int[] { 30, 6, 7, 20, 30, 7 },
                intrinsics);

            _Data.Add("b", barbarian);


        }

        public static Roles Instance
        {
            get
            {
                return _Instance;
            }
        }

        public Role this[string id]
        {
            get
            {
                return _Data[id];
            }
        }

        private Dictionary<string, Role> _Data;
    }
}