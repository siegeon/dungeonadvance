using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Player
{
    public class Races
    {

        private static Races _Instance;

        public static void Initialise()
        {
            _Instance = new Races();
        }

        private Races()
        {
            _Data = new Dictionary<string, Race>();

            Race human = new Race("human", "human", "humanity",
                new int[] { 3, 3, 3, 3, 3, 3 },
                new int[] { 18, 18, 18, 18, 18, 18 },
                new Advancement(2, 0, 0, 2, 1, 0),
                new Advancement(1, 0, 2, 0, 2, 0),
                new EffectType[] { } );

            _Data.Add("h", human);


        }

        public static Races Instance
        {
            get
            {
                return _Instance;
            }
        }

        public Race this[string id]
        {
            get
            {
                return _Data[id];
            }
        }
        
        private Dictionary<string, Race> _Data;









    }
}
