using System;
using System.Collections.Generic;
using System.Text;
using SlashNet.Monsters;

namespace SlashNet.Dungeon
{
    

    public class GenericDungeon : IDungeon, IEnumerable<AMonster>
    {

        private Tile[,] _Board;
        private bool[,] _Visited;

        public const int WIDTH = 80;
        public const int HEIGHT = 40;

        #region IDungeon

        #region Size

        public int Width { get { return WIDTH; } }
        public int Height { get { return HEIGHT; } }

        #endregion

        #region Visit

        public bool Visit(SNPoint p)
        {
            return Visit(p.X, p.Y);
        }

        public bool Visit(ILocatable loc)
        {
            return Visit(loc.Location);
        }

        public bool Visit(int x, int y)
        {
            _Visited[x, y] = true;
            _Board[x, y].Visible = true;
            _Board[x, y].Seen = true;
            return IsObstacle(x, y);
        }

        #endregion

        #region Contains

        public bool Contains(int x, int y)
        {
            return x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT;
        }

        public bool Contains(SNPoint p)
        {
            return Contains(p.X, p.Y);
        }

        public bool Contains(ILocatable loc)
        {
            return Contains(loc.Location);
        }

        #endregion

        #region IsObstacle

        public bool IsObstacle(int x, int y)
        {
            return _Board[x, y].Type != TileType.Floor 
                && _Board[x, y].Type != TileType.Door
                && _Board[x, y].Type != TileType.Corridor;
        }

        public bool IsObstacle(SNPoint p)
        {
            return IsObstacle(p.X, p.Y);
        }

        public bool IsObstacle(ILocatable loc)
        {
            return IsObstacle(loc.Location);
        }

        #endregion

        #region Tile
        
        public Tile this[int x, int y]
        {
            get
            {
                return _Board[x, y];
            }
        }

        public Tile this[SNPoint p]
        {
            get
            {
                return this[p.X, p.Y];
            }
        }

        public Tile this[ILocatable loc]
        {
            get
            {
                return this[loc.Location];
            }
        }

        #endregion

        #region reset

        public void Reset()
        {
            for (int i = 0; i < WIDTH; i++)
            {
                for (int j = 0; j < HEIGHT; j++)
                {
                    _Board[i, j].Visible = false;
                }
            }
            _Visited = new bool[WIDTH, HEIGHT];
        }

        #endregion

        #endregion


        private SNPoint _Start;
        public SNPoint Start
        {
            get
            {
                return _Start;
            }
        }

        public GenericDungeon(TileType[,] tmpMap, SNPoint start)
        {
            _Start = start;
            _Visited = new bool[WIDTH, HEIGHT];
            //for (int i = 0; i < 80; i++)
            //{
            //    for (int j = 0; j < 40; j++)
            //    {
            //        _Visited[i, j] = true;
            //    }
            //}
            _Board = new Tile[WIDTH, HEIGHT];
            for (int i = 0; i < WIDTH; i++)
            {
                for (int j = 0; j < HEIGHT; j++)
                {
                    if (i < WIDTH - 1 && j < HEIGHT - 1)
                        _Board[i, j] = new Tile(tmpMap[i, j]);
                    else
                        _Board[i, j] = new Tile(TileType.Rock);

                }
            }
        }


        #region IDungeon Members

        private Dictionary<int, AMonster> _Monsters = new Dictionary<int, AMonster>();

        public void Remove(AMonster monster)
        {
            this[monster].Monster = null;
            _Monsters.Remove(monster.ID);
        }

        public void Add(AMonster monster)
        {
            _Monsters.Add(monster.ID, monster);
            this[monster].Monster = monster;
        }

        #endregion

        #region IEnumerable<AMonster> Members

        public IEnumerator<AMonster> GetEnumerator()
        {
            foreach (AMonster m in _Monsters.Values)
                yield return m;
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}

