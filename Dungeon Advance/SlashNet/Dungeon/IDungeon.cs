using System;
using System.Collections.Generic;
using System.Text;
using SlashNet.Monsters;

namespace SlashNet.Dungeon
{
    public interface IDungeon : IEnumerable<AMonster>
    {
        bool Contains(int x, int y);
        bool Contains(SNPoint p);
        bool Contains(ILocatable loc);
        
        bool IsObstacle(int x, int y);
        bool IsObstacle(ILocatable loc);
        bool IsObstacle(SNPoint p);

        SNPoint Start { get; }

        bool Visit(int x, int y);
        bool Visit(ILocatable loc);
        bool Visit(SNPoint p);

        void Reset();

        Tile this[int x, int y]
        {
            get; 
        }

        Tile this[SNPoint point]
        {
            get;
        }

        Tile this[ILocatable loc]
        {
            get;
        }


        int Width
        {
            get;
        }
        int Height
        {
            get;
        }

        void Remove(AMonster monster);
        void Add(AMonster monster);
    }
}
