namespace SlashNet.Dungeon
{
    public enum TileType
    {
        Rock,
        Floor,
        Door,
        Invisible,
        VWall,
        HWall,
        NECorner,
        NWCorner,
        SECorner,
        SWCorner,
        NTWall,
        ETWall,
        STWall,
        WTWall,
        XWall,
        Corridor
    };
}
