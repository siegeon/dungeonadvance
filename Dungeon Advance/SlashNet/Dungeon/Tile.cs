using System;
using System.Collections.Generic;
using System.Text;

using SlashNet.Items;
using SlashNet.Monsters;

namespace SlashNet.Dungeon
{
    public class Tile
    {
        private TileType _Type;

        private AMonster _Monster;

        public AMonster Monster
        {
            get { return _Monster; }
            set { _Monster = value; }
        }

        public void Update()
        {
            if (_Monster != null)
                _Monster.Tick();
        }

        public TileType Type { get { return _Type; } }

        public Drawable View
        {
            get
            {
                if (_Monster != null && Visible)
                    return _Monster.View;
                if (_Items.Count  == 0)
                    return DataStore.Instance[this, _Type];
                return DataStore.Instance[this, _Items[0]];
            }
        }

        private ItemStack _Items = new ItemStack();

        public Tile(TileType type)
        {
            _Type = type;
            Visible = false;
        }

        public bool Visible;
        public bool Seen;

        internal ItemStack Items { get { return _Items; } }
        
    }
}
