using System;
using System.Collections.Generic;



namespace SlashNet.Dungeon.Generation
{
    internal class Room : CellGrid
    {
        private SNPoint _Location;


        internal SNPoint Location
        {
            get
            {
                return _Location;
            }
            set
            {
                _Location = value;
                _Bounds = new Rectangle(value.X, value.Y, Width, Height);
            }
        }

        internal Room(int width, int height)
            : base(width, height)
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Cell cell = _Cells[x, y];
                    cell[DirectionType.West] = (x == 0) ? SideType.Wall : SideType.Empty;
                    cell[DirectionType.East] = (x == Width - 1) ? SideType.Wall : SideType.Empty;
                    cell[DirectionType.North] = (y == 0) ? SideType.Wall : SideType.Empty;
                    cell[DirectionType.South] = (y == Height - 1) ? SideType.Wall : SideType.Empty;
                    
                }
            }
        }

    }
}
