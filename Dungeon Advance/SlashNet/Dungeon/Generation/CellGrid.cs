using System;
using System.Collections.Generic;
using System.Text;
//using Microsoft.Xna.Framework;

namespace SlashNet.Dungeon.Generation
{
    internal abstract class CellGrid
    {
        protected Rectangle _Bounds;
        protected readonly Cell[,] _Cells;

        internal Rectangle Bounds
        {
            get
            {
                return _Bounds;
            }
        }

        internal Cell this[int x, int y]
        {
            get
            {
                return _Cells[x, y];
            }
        }

        internal Cell this[SNPoint p]
        {
            get
            {
                return _Cells[p.X, p.Y];
            }
        }

        internal int Width
        {
            get
            {
                return _Cells.GetUpperBound(0) + 1;
            }
        }

        internal int Height
        {
            get
            {
                return _Cells.GetUpperBound(1) + 1;
            }
        }
        internal IEnumerable<SNPoint> CellLocations
        {
            get
            {
                for (int x = 0; x < Width; x++)
                    for (int y = 0; y < Height; y++)
                        yield return new SNPoint(x, y);
            }
        }

        protected CellGrid(int width, int height)
        {
            _Cells = new Cell[width, height];
            _Bounds = new Rectangle(0, 0, width, height);
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    _Cells[x, y] = new Cell();
                }
            }
        }

    }
}
