using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Dungeon.Generation
{
    internal class Generator
    {
        internal static Map Generate(int width, int height, 
            int directionModifier, int sparsenessModifier, int deadEndRemovalModifier,
            int numRooms, int minRoomWidth, int maxRoomWidth, int minRoomHeight, int maxRoomHeight)
        {
            Map map = new Map(width, height);
            map.MarkCellsUnvisited();
            SNPoint currentPoint = map.PickRandomCellAndMarkItVisited();
            DirectionType previous = DirectionType.North;

            while (!map.AllCellsVisited)
            {

                DirectionPicker picker = new DirectionPicker(directionModifier, previous);
                DirectionType direction = picker.GetNextDirection();

                while (!map.HasAdjacentCellInDirection(currentPoint, direction) ||
                    map.AdjacentCellInDirectionIsVisited(currentPoint, direction))
                {
                    if (picker.HasNextDirection)
                        direction = picker.GetNextDirection();
                    else
                    {
                        currentPoint = map.GetRandomVisitedCell(currentPoint);
                        picker = new DirectionPicker(directionModifier, previous);
                        direction = picker.GetNextDirection();
                    }
                }

                currentPoint = map.CreateCorridor(currentPoint, direction);
                //Console.WriteLine("Created: {0}/{1}", ++counter, map.Width*map.Height);
                map.FlagCellAsVisited(currentPoint);
                previous = direction;
            }
            //TODO: Use the valid direction picked for next step in algorithm
            SparsifyMaze(map, sparsenessModifier);
            RemoveDeadEnds(map, deadEndRemovalModifier);
            PlaceRooms(map, numRooms, minRoomWidth, maxRoomWidth, minRoomHeight, maxRoomHeight);
            map.PlaceDoors();
            return map;
        }

        internal static void SparsifyMaze(Map map, int sparsenessModifier)
        {
            int noOfDeadEndCellsToRemove = 
                (int)Math.Ceiling((double)sparsenessModifier / 100 * (map.Width * map.Height));

            IEnumerator<SNPoint> enumerator = map.DeadEndCellLocations.GetEnumerator();

            for (int i = 0; i < noOfDeadEndCellsToRemove; i++)
            {
                if (!enumerator.MoveNext())
                {
                    enumerator = map.DeadEndCellLocations.GetEnumerator();
                    if (!enumerator.MoveNext())
                        break;
                }

                SNPoint p = enumerator.Current;
                DirectionType d = map[p].CalculateDeadEndCorridorDirection();
                map.CreateWall(p, d);
                map[p].IsCorridor = false;
                //map[p.Translate(d)].IsCorridor = false;
            }
            
        }

        internal static bool ShouldRemoveDeadend(int deadEndRemovalModifier)
        {
            return Random.Instance.Next(1, 99) < deadEndRemovalModifier;
        }

        internal static void RemoveDeadEnds(Map map, int deadEndRemovalModifier)
        {
            foreach(SNPoint p in map.DeadEndCellLocations)
            {
                SNPoint current = p;
                do
                {
                    DirectionPicker picker = new DirectionPicker(100, map[current].CalculateDeadEndCorridorDirection());
                    DirectionType direction = picker.GetNextDirection();

                    while(!map.HasAdjacentCellInDirection(current, direction))
                    {
                        if(picker.HasNextDirection)
                            direction = picker.GetNextDirection();
                        else
                            throw new InvalidOperationException("This should not happen");
                    }

                    current = map.CreateCorridor(current, direction);

                } while(map[current].IsDeadEnd);
            }
        }

        internal static Room CreateRoom(int minRoomWidth, int maxRoomWidth, 
            int minRoomHeight, int maxRoomHeight)
        {
            return new Room(Random.Instance.Next(minRoomWidth, maxRoomWidth),
                Random.Instance.Next(minRoomHeight, maxRoomHeight));

        }

        internal static void PlaceRooms(Map map, int numRooms, 
            int minRoomWidth, int maxRoomWidth, 
            int minRoomHeight, int maxRoomHeight)
        {
            for (int roomCounter = 0; roomCounter < numRooms; roomCounter++)
            {
                Room r = CreateRoom(minRoomWidth, maxRoomWidth, minRoomHeight, maxRoomHeight);
                int best = int.MaxValue;
                SNPoint? loc = null;

                foreach (SNPoint p in map.CorridorCellLocations)
                {
                    int currentScore = map.CalculateRoomPlacementScore(p, r);
                    if (currentScore < best)
                    {
                        best = currentScore;
                        loc = p;
                    }
                }

                if (loc != null)
                    map.PlaceRoom(loc.Value, r);
            }
            
        }
    }
}
