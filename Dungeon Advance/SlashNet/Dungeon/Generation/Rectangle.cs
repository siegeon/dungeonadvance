namespace SlashNet.Dungeon.Generation
{
    internal struct Rectangle
    {
        internal readonly int X, Y, Width, Height;

        internal int Right
        {
            get
            {
                return X + Width;
            }
        }

        internal int Bottom
        {
            get
            {
                return Y + Height;
            }
        }

        internal Rectangle(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        internal bool Contains(Rectangle rectangle)
        {
            return rectangle.X > X && rectangle.Right < Right && rectangle.Y > Y && rectangle.Bottom < Bottom;
        }

        internal bool Contains(int x, int y)
        {
            return x > X && x < Right && y > Y && y < Bottom;
        }
    }
}