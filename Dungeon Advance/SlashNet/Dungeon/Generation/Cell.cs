using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Dungeon.Generation
{
    public class Cell
    {
        private bool _Visited;
        private SideType[] _Sides = new SideType[4];

        public Cell(SideType initial)
        {
            for (int i = 0; i < _Sides.Length; i++)
                _Sides[i] = initial;
        }

        public Cell() : this(SideType.Wall)
        {
        }

        public SideType this[DirectionType d]
        {
            get
            {
                return (_Sides[(int)d]);
            }
            set
            {
                _Sides[(int)d] = value;
            }
        }

        public bool Visited
        {
            get
            {
                return _Visited;
            }
            set
            {
                _Visited = value;
            }
        }

        public bool IsDeadEnd
        {
            get
            {
                return WallCount == 3;
            }
        }

        public int WallCount
        {
            get
            {
                int count = 0;
                foreach (SideType side in _Sides)
                    if (side == SideType.Wall)
                        count++;
                return count;
            }
        }

        private bool _Corridor = false;

        public bool IsCorridor
        {
            get
            {
                return _Corridor;
            }
            set
            {
                _Corridor = value;
            }
        }

        public DirectionType CalculateDeadEndCorridorDirection()
        {
            if (!IsDeadEnd) throw new InvalidOperationException();

            for (int i = 0; i < 4; i++)
			{
			    if(_Sides[i] == SideType.Empty)
                    return (DirectionType)i;
			}

            throw new InvalidOperationException();
        }

        public bool IsRock
        {
            get
            {
                return WallCount == 4;
            }
        }
    }
}
