using System;
using System.Collections.Generic;
using System.Text;

using SlashNet.Items;
using SlashNet.Dungeon;
using SlashNet.Dungeon.FOV;
using SlashNet.Dungeon.Generation;
using SlashNet.Player;
using SlashNet.Monsters;

namespace SlashNet
{
    public interface ISlashNetUI
    {
        void Pline(String message);
        void EndPline();

        void ShowStack(ItemStack itemStack, bool isInventory);

        void DYWYPI();
    }

    public class SlashNetGame
    {

        public const int MAXULEV = 30;

        #region Static

        private static SlashNetGame _Instance;

        public static void Initialise()
        {
            MonsterClassDictionary.Initialise();
            Grammar.Initialise();
            Races.Initialise();
            Roles.Initialise();
        }

        public static SlashNetGame NewGame(DungeonStyle style, int dirMod, int sparseMod, int deadMod)
        {
            _Instance = new SlashNetGame(style, dirMod, sparseMod, deadMod);
            _Instance.Populate();
            return _Instance;
        }

        private int _Ticks = 1;
        public int CurrentTick
        {
            get
            {
                return _Ticks;
            }
        }


        private void Populate()
        {
            for (int i = 0; i < 25; i++)
            {
                Gem s = new Gem("green");
                SNPoint p = new SNPoint();
                do
                {
                    p = GenerateInRange();

                } while (_Dungeon.IsObstacle(p));

                _Dungeon[p].Items.Add(s, Random.D(1, 3));
                _Dungeon[p].Items.Add(new Combestible(100, "banana"), 1);
                AMonster monster = MonsterStore.Instance.GenerateRandom();
                monster.Location = p;
                _Dungeon.Add(monster);

            }

        }

        public PlayerCharacter Player
        {
            get
            {
                return _Player;
            }
        }

        public static SlashNetGame Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new SlashNetGame(DungeonStyle.Crawl, 50, 50, 50);
                return _Instance;
            }
        }

        private static MersenneTwister rng = SlashNet.Random.Instance;

        public static SNPoint GenerateInRange()
        {
            int i = rng.Next(0, 79);
            int j = rng.Next(0, 39);
            return new SNPoint(i, j);
        }

        #endregion

        #region Fields

        private IFOVCalculator _FOV;
        private ISlashNetUI _UI;
        private PlayerCharacter _Player;
        private IDungeon _Dungeon;

        #endregion

        private SlashNetGame(DungeonStyle style, int dirMod, int sparseMod, int deadMod)
        {
            Map m = Generator.Generate(GenericDungeon.WIDTH / 2 - 1, GenericDungeon.HEIGHT / 2 - 1,
                dirMod, sparseMod, deadMod, rng.Next(5, 10), 2, 5, 2, 5);

            //style = DungeonStyle.Crawl;
            _Dungeon = m.ExpandToDungeon(style);
            
            DataStore.Initialise(style);
            MonsterStore.Initialise();

            _FOV = new BasicFOVCalculator();

            _Player = new PlayerCharacter("Frizzlespratz", Races.Instance["h"], Roles.Instance["b"]);
            _Player.Location = _Dungeon.Start;
            Dagger d = new Dagger();
            d.Owner = _Player;
            _Player.Inventory.Add(d, 1);
            
            _FOV.CalculateFOV(_Dungeon, _Player, 5);
        }

        #region Controller

        public string PCStatus
        {
            get
            {
                return _Player.Status;
            }
        }

        public void Register(ISlashNetUI ui)
        {
            _UI = ui;
        }

        internal int CurrentLevel
        {
            get
            {
                return 1;
            }
        }

        

        public bool MoveTo(SNPoint n)
        {
            if (_InventoryActive)
                return false;

            SNPoint dir = getTowards(n);

            if (_Dungeon.Contains(n) && !_Dungeon.IsObstacle(n))
            {

                checkTick();

                if (_Dungeon[n].Monster != null)
                {
                    for (int i = 0; i < _Player.Attacks.Length; i++)
                    {
                        _Player.OnAttacks(_Dungeon[n].Monster);
                    }

                    endTurn();
                    return false;
                }

                bool continueMoving = true;
                _Player.Location = n;
                if (_Dungeon[_Player].Items.Count > 0)
                {
                    continueMoving = false;
                    _UI.ShowStack(_Dungeon[_Player].Items, false);
                }
                else
                {
                    _UI.Pline(null);
                    _UI.ShowStack(null, false);
                }
                _FOV.CalculateFOV(_Dungeon, _Player, 5);


                if ((_Dungeon.Contains(_Player.Location.Translate(dir)) &&
                    _Dungeon[_Player.Location.Translate(dir)].Type == TileType.Door) ||
                    (_Dungeon[_Player.Location.Translate(dir)].Monster != null)) 
                    continueMoving = false;

                
                endTurn();


                return continueMoving;
            }

            return false;
        }

        private void checkTick()
        {
            if (_Player.MovePoints < 12)
                Tick();
        }

        private void endTurn()
        {
            _Player.Moved();
            foreach (AMonster monster in _Dungeon)
            {
                monster.Move(_Dungeon);
            }
            _UI.EndPline();
        }

        public int Ticks
        {
            get
            {
                return _Ticks;
            }
        }

        private void Tick()
        {
            _Ticks++;
            _Player.Tick();

            for (int i = 0; i < GenericDungeon.WIDTH; i++)
            {
                for (int j = 0; j < GenericDungeon.HEIGHT; j++)
                {
                    _Dungeon[i, j].Update();
                }
            }


            
        }

        private SNPoint getTowards(SNPoint p)
        {
            return new SNPoint(p.X - _Player.Location.X, p.Y - _Player.Location.Y);
        }

        public void MoveTowards(SNPoint d)
        {

            SNPoint target = getTowards(d);
            while (MoveTo(_Player.Location.Translate(target))) ;
        }


        public void Pickup()
        {
            checkTick();

            if (_InventoryActive)
                return;

            if (_Dungeon[_Player].Items.Count == 0)
                _UI.Pline("There is nothing here to pick up!");
            else
            {
                if (_Dungeon[_Player].Items.Count == 1)
                {
                    SingleOrStack s = _Dungeon[_Player].Items[0];
                    _Dungeon[_Player].Items.RemoveAt(0);
                    _Player.Inventory.Add(s);
                    _UI.Pline("You pick up " + s);
                }
            }

            endTurn();
            
        }

        #endregion

        #region Properties

        public ISlashNetUI UI { get { return _UI; } }

        public SNPoint PCPosition { get { return _Player.Location; } }

        public IDungeon Dungeon { get { return _Dungeon; } }

        #endregion

        private bool _InventoryActive = false;

        public void Apply(int index)
        {
            
            _Player.Inventory[index].Apply(new SNPoint());
        }

        public ItemStack Inventory
        {
            get
            {
                return _Player.Inventory;
            }
        }

        public void Pickup(List<SingleOrStack> list)
        {
            foreach (SingleOrStack i in list)
            {
                _Dungeon[_Player].Items.Remove(i);
                _Player.Inventory.Add(i);
            }
            
            _UI.Pline("You picked up " + list.Count + " item types");
        }

        internal void DYWYPI()
        {
            _UI.DYWYPI();
        }
    }
}
