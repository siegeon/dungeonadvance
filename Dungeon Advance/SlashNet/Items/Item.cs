using System;
using System.Collections.Generic;
using SlashNet.Player;
using SlashNet.Monsters;
using SlashNet.Items.Materials;


namespace SlashNet.Items
{
    delegate ApplyType Action(SNPoint towards);

    public abstract class Item : ADrawable, IComparable<Item>
    {

        protected int _Weight;
        protected string _Name;
        protected List<EffectType> _Resistances = new List<EffectType>();
        protected AMaterial _Material;
        protected CompositeDice _Damage = new CompositeDice(0, 1);
        private Action _DefaultAction;

        internal virtual int ToHit(Being versus)
        {
            int tmp = 0;
            if(_Material != null)
                tmp += _Material.ToHitBonus(versus);
            return tmp;
        }

        public virtual ApplyType Weild()
        {
            if (_Owner.Wielded != null)
                throw new Exception("Can't weild an item when one is already being weilded");
            _Owner.Wielded = this;
            return ApplyType.Wield;
        }

        public virtual ApplyType UnWeild()
        {
            if (_Owner.Wielded == null)
                throw new Exception("Can't unweild an item when one isn't being weilded");
            if (_Owner.Wielded != this)
                throw new Exception("Can't unweild an item which isn't being weilded");
            _Owner.Wielded = null;
            return ApplyType.UnWield;
        }

        protected Being _Owner;

        public Being Owner
        {
            set { _Owner = value; }
            get { return _Owner; }
        }

        public void ApplyEffect(EffectType type)
        {
            if (_Material != null)
                _Material.ApplyEffect(type);
        }

        public ApplyType Apply(SNPoint towards)
        {

            if (_Owner == null)
                throw new Exception("Can't apply an unowned item");

            return _DefaultAction(towards);
        }

        protected Beautitude _Beautitude = Beautitude.Uncursed;

        private bool _Stackable;

        public virtual String Prefix
        {
            get
            {
                string tmp = "";
                if (_Material != null)
                    tmp += _Material.ToString();
                return tmp;
            }
        }

        public virtual String Suffix
        {
            get
            {
                return "";
            }
        }

        public bool Stackable
        {
            get { return _Stackable & !_Artifact; }
        }

        public Beautitude Beautitude
        {
            get
            {
                return _Beautitude;
            }
        }

        protected bool _Artifact = false;
        

        

        protected Item(int weight, string name, bool stackable)
        {
            _Weight = weight;
            _Name = name;
            _Stackable = stackable;
        }

        

        public virtual void SetResistance(EffectType effect, bool set)
        {
            if (set)
                _Resistances.Add(effect);
            else
                _Resistances.Remove(effect);
        }

        public virtual bool Resists(EffectType effect)
        {
            return _Resistances.Contains(effect);
        }

        public virtual int Weight { get { return _Weight; } }

        public abstract string View { get; }

        public virtual string Name { get { return _Name; } }

        public abstract ItemType Type { get; }

        public virtual int Damage(Being versus)
        {
            int tmp = _Damage.Roll();
            if (_Material != null)
                tmp += _Material.DamageBonus(versus);
            return tmp;
        }

        public virtual void Eat()
        {
            SlashNetGame.Instance.UI.Pline("You can't eat that!");
        }

        public override string ToString()
        {
            return Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Item))
                return false;

            return CompareTo(obj as Item) == 0;
        }

        #region IComparable<Item> Members

        public int CompareTo(Item other)
        {
            if (Type.CompareTo(other.Type) != 0)
                return Type.CompareTo(other.Type);
            return Name.CompareTo(other.Name);
        }

        #endregion
    }
}
