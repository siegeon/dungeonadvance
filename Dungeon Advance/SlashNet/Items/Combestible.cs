using System;
using System.Collections.Generic;
using System.Text;


namespace SlashNet.Items
{
    public class Combestible : Item
    {

        public Combestible(int weight, string name) : base(weight, name, false)
        {
            DataStore.Instance.Register(ID, new Drawable("%", SNColor.Green));
        }

        public override ItemType Type
        {
            get { return ItemType.Combestible; }
        }

        public override string View
        {
            get { return "%"; }
        }


     
    }
}
