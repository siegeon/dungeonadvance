using System;
using System.Collections.Generic;
using System.Text;

using SlashNet.Items.Materials;

namespace SlashNet.Items
{
    public abstract class ADagger : AWeapon
    {

        protected ADagger(int weight, string name, SNColor color)
            : base(weight, name, true, color)
        {
        }

        internal sealed override WeaponType WeaponType
        {
            get { return WeaponType.Dagger; }
        }

    }
}
