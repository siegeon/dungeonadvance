using System;
using System.Collections.Generic;
using System.Text;

using SlashNet.Items.Materials;
using SlashNet.Player;

namespace SlashNet.Items
{

    

    public abstract class AWeapon : Item
    {
        protected AWeapon(int weight, string name, bool stackable, SNColor color)
            : base(weight, name, stackable)
        {
            DataStore.Instance.Register(ID, new Drawable("(", color));
        }

        public sealed override string View
        {
            get { return "("; }
        }

        public sealed override string Suffix
        {


            get
            {
                if(_Owner != null && _Owner is PlayerCharacter && (_Owner as PlayerCharacter).Wielded == this)
                    return "(wielded)";
                return "";
            }
        }
        
        public sealed override ItemType Type
        {
            get { return ItemType.Weapon; }
        }

        internal abstract WeaponType WeaponType
        {
            get;
        }
    }
}
