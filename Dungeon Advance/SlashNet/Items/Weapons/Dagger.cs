using System;
using System.Collections.Generic;
using System.Text;
using SlashNet.Items.Materials;

namespace SlashNet.Items
{
    public class Dagger : ADagger
    {
        public Dagger()
            : base(10, "dagger", SNColor.LightGray)
        {
            _Material = new Iron(this);
            _Damage = new CompositeDice(0, 20);
        }

    }
}
