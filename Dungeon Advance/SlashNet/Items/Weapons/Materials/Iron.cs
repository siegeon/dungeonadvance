using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Items.Materials
{
    class Iron : AMaterial
    {
        #region IMaterial Members

        private int _Rusty = 0;
        private int _Corroded = 0;

        internal Iron(Item owner)
            : base(owner)
        {
        }

        internal override void ApplyEffect(EffectType e)
        {
            switch (e)
            {
                case EffectType.Water:
                case EffectType.Rust:
                    {
                        string s = OwnerGrammar.OwnsPrefix(_Item.Owner);
                        if (_Rusty == 3)
                            SlashNetGame.Instance.UI.Pline(s + " " + _Item + " looks completely rusty");
                        else
                        {
                            _Rusty++;
                            SlashNetGame.Instance.UI.Pline(s + " " + _Item + " rusts " 
                                + MaterialGrammar.ProceedsFromLevel(_Rusty));
                        }
                        break;
                    }
                case EffectType.Acid:
                    {
                        string s = OwnerGrammar.OwnsPrefix(_Item.Owner);
                        if (_Corroded == 3)
                            SlashNetGame.Instance.UI.Pline(s + " " + _Item + " looks completely corroded");
                        else
                        {
                            _Corroded++;
                            SlashNetGame.Instance.UI.Pline(s + " " + _Item + " corrodes " 
                                + MaterialGrammar.ProceedsFromLevel(_Corroded));
                        }
                        break;
                    }
            }
        }

        internal override int ToHitBonus(Being versus)
        {
            return -_Rusty - _Corroded;
        }

        internal override int DamageBonus(Being versus)
        {
            return -_Rusty - _Corroded;
        }

        public override string ToString()
        {
            string tmp = "";
            if (_Rusty > 0)
                tmp += MaterialGrammar.GetFromLevel(_Rusty) + " rusty";

            if (tmp != "")
                tmp += " ";

            if (_Corroded > 0)
                tmp += MaterialGrammar.GetFromLevel(_Corroded) + " corroded";

            if (_Item.Resists(EffectType.Rust))
                return "rustproof";
            
            return tmp;
        }

        #endregion
    }
}
