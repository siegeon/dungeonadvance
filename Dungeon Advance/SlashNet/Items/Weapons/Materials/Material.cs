using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Items.Materials
{
    public abstract class AMaterial
    {
        protected Item _Item;

        protected AMaterial(Item o)
        {
            _Item = o;
        }

        internal abstract void ApplyEffect(EffectType e);

        internal abstract int ToHitBonus(Being versus);

        internal abstract int DamageBonus(Being versus);

    }

    public class MaterialGrammar
    {
        public static string GetFromLevel(int value)
        {
            switch (value)
            {
                case 2:
                    return "very";
                case 3:
                    return "thoroughly";
                default:
                    return "";
            }
        }

        internal static string ProceedsFromLevel(int value)
        {
            switch (value)
            {
                case 2:
                    return "further ";
                case 3:
                    return "completely ";
                default:
                    return "";
            }
        }
    }
}
