
namespace SlashNet.Items
{
    public enum WeaponType
    {
        LongSword, ShortSword, Dagger
    }
}
