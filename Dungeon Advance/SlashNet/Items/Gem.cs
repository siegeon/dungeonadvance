using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet.Items
{
    public class Gem : Item
    {
        public Gem(string color) : base(10, color + " gem", true)
        {

            SNColor c = new SNColor();
            switch (color)
            {
                case "green":
                    c = SNColor.Green;
                    break;
                case "red":
                    c = SNColor.Red;
                    break;
                case "orange":
                    c = SNColor.Orange;
                    break;
            }

            DataStore.Instance.Register(ID, new Drawable("*", c));
        }



        public override string View
        {
            get { return "*"; }
        }

        public override ItemType Type
        {
            get { return ItemType.Gem; }
        }
    }
}
