using System.Collections.Generic;

namespace SlashNet
{
    public interface IRollable
    {
        int Roll();
    }

    public struct CompositeDice : IRollable
    {
        private List<IRollable> _Dice;
        private int _Bonus;

        public CompositeDice(int bonus, params int[] dice)
        {
            _Dice = new List<IRollable>();
            _Bonus = bonus;
            foreach (int i in dice)
                _Dice.Add(new Die(i));

        }

        #region IRollable Members

        public int Roll()
        {
            int i = 0;
            foreach (IRollable r in _Dice)
                i += r.Roll();
            return i + _Bonus;
        }

        #endregion
    }

    internal struct Die : IRollable
    {
        internal static Die Zero = new Die(-1);

        private int _Sides;

        internal Die(int sides)
        {
            _Sides = sides;
        }


        #region IRollable Members

        public int Roll()
        {
            if (_Sides < 0)
                return 0;
            return Random.Instance.Next(1, _Sides);
        }

        #endregion
    }

}