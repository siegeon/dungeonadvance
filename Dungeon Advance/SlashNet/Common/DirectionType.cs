namespace SlashNet
{
    public enum DirectionType
    {
        North,
        East,
        South,
        West
    };

    public class DirectionOperations
    {
        public static DirectionType Opposite(DirectionType dir)
        {
            switch (dir)
            {
                case DirectionType.North:
                    return DirectionType.South;
                case DirectionType.South:
                    return DirectionType.North;
                case DirectionType.West:
                    return DirectionType.East;
                case DirectionType.East:
                    return DirectionType.West;
                default:
                    return DirectionType.North;
            }
        }

        public static int[] GetXY(DirectionType dir)
        {
            switch (dir)
            {
                case DirectionType.North:
                    return new int[] { 0, -1 };
                case DirectionType.South:
                    return new int[] { 0, 1 };
                case DirectionType.East:
                    return new int[] { 1, 0 };
                case DirectionType.West:
                    return new int[] { -1, 0 };
            }
            return null;
        }
    }
}