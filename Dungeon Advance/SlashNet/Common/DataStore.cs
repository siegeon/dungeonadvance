using System;
using System.Collections.Generic;
using System.Text;


using SlashNet.Items;
using SlashNet.Dungeon;
using SlashNet.Monsters;

namespace SlashNet
{
    public class DataStore
    {

        private static DataStore _Instance;

        public static void Initialise(DungeonStyle style)
        {
            _Instance = new DataStore(style);
        }

        public static DataStore Instance
        {
            get
            {
                return _Instance;
            }
        }

        private Dictionary<TileType, Drawable> _TileDictionary = new Dictionary<TileType, Drawable>();
        private Dictionary<int, Drawable> _ADrawableDictionary = new Dictionary<int, Drawable>();
        

        public Drawable this[Tile t, SingleOrStack i]
        {
            get
            {
                if (t.Visible)
                    return _ADrawableDictionary[i.Template.ID];
                else return this[t, t.Type];
            }
        }


        public void Register(int id, Drawable d)
        {
            if (!_ADrawableDictionary.ContainsKey(id))
                _ADrawableDictionary.Add(id, d);
        }

        public Drawable this[Tile t, TileType type]
        {
            get 
            {
                if (t.Visible)
                    return _TileDictionary[type];
                else if (t.Seen)
                    return new Drawable(_TileDictionary[type].Char, SNColor.Gray);
                else
                    return Drawable.Null;
            }
        }


        private DataStore(DungeonStyle style)
        {
            if (style == DungeonStyle.Hack)
            {
                _TileDictionary.Add(TileType.Door, new Drawable("+", SNColor.Yellow));
                _TileDictionary.Add(TileType.Floor, new Drawable(".", SNColor.White));
                _TileDictionary.Add(TileType.Rock, new Drawable(" ", SNColor.Black));
                _TileDictionary.Add(TileType.Corridor, new Drawable("#", SNColor.White));
                _TileDictionary.Add(TileType.HWall, new Drawable("-", SNColor.LightGray));
                _TileDictionary.Add(TileType.VWall, new Drawable("|", SNColor.LightGray));

                _TileDictionary.Add(TileType.NECorner, new Drawable("-", SNColor.LightGray));
                _TileDictionary.Add(TileType.SECorner, new Drawable("-", SNColor.LightGray));
                _TileDictionary.Add(TileType.NWCorner, new Drawable("-", SNColor.LightGray));
                _TileDictionary.Add(TileType.SWCorner, new Drawable("-", SNColor.LightGray));

                _TileDictionary.Add(TileType.ETWall, new Drawable("|", SNColor.LightGray));
                _TileDictionary.Add(TileType.WTWall, new Drawable("|", SNColor.LightGray));
                _TileDictionary.Add(TileType.NTWall, new Drawable("-", SNColor.LightGray));
                _TileDictionary.Add(TileType.STWall, new Drawable("-", SNColor.LightGray));

                _TileDictionary.Add(TileType.XWall, new Drawable("+", SNColor.LightGray));
            }
            else
            {
                _TileDictionary.Add(TileType.Door, new Drawable("+", SNColor.Yellow));
                _TileDictionary.Add(TileType.Floor, new Drawable(".", SNColor.White));
                _TileDictionary.Add(TileType.Rock, new Drawable("#", SNColor.LightGray));
            }

        }
    }
}
