using System;

namespace SlashNet
{
    public struct SNPoint
    {
        public readonly int X;
        public readonly int Y;

        public SNPoint(int x, int y)
        {
            X = x;
            Y = y;
        }

        public SNPoint Translate(int x, int y)
        {
            return new SNPoint(X + x, Y + y);
        }

        public SNPoint Translate(SNPoint p)
        {
            return Translate(p.X, p.Y);
        }

        public int ManhattanDistance(SNPoint p)
        {
            return Math.Abs(p.X - X) + Math.Abs(p.Y - Y);
        }
        
        public SNPoint Translate(DirectionType dir)
        {
            switch (dir)
            {
                case DirectionType.North:
                    return new SNPoint(X, Y - 1);
                case DirectionType.South:
                    return new SNPoint(X, Y + 1);
                case DirectionType.West:
                    return new SNPoint(X - 1, Y);
                case DirectionType.East:
                    return new SNPoint(X + 1, Y);
                default:
                    return new SNPoint();
            }
            
        }

        public override int GetHashCode()
        {
            return X * Y;
        }

        public override bool Equals(object obj)
        {
            if(!(obj is SNPoint))
                return false;

            if (obj == null)
                return false;

            SNPoint o = (SNPoint)obj;

            return (X == o.X && Y == o.Y);
        }

        public static bool operator ==(SNPoint a, SNPoint b)
        {
            return (a.Equals(b));
        }

        public static bool operator !=(SNPoint a, SNPoint b)
        {
            return (!a.Equals(b));
        }

        internal double Distance(int i, int j)
        {
            int a = Math.Abs(i - X);
            int b = Math.Abs(j - Y);

            return Math.Sqrt(i * i + j * j);

        }
    }
}