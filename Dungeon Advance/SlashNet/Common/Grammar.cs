using System;
using System.Collections.Generic;
using System.Text;

using SlashNet.Monsters;

namespace SlashNet
{
    public class Grammar
    {

        private static Dictionary<AttackType, string> _Words = new Dictionary<AttackType, string>();
        private static Dictionary<AttackType, string> _Endings = new Dictionary<AttackType, string>();

        public static void Initialise()
        {
            _Words.Add(AttackType.Bite, "bite");

            _Words.Add(AttackType.Weapon, "hit");

            _Words.Add(AttackType.Claw, "claw");

            _Words.Add(AttackType.Kick, "kick");

            _Words.Add(AttackType.Zap, "zap");
            _Endings.Add(AttackType.Zap, "you");

            _Words.Add(AttackType.Touch, "touch");
            _Endings.Add(AttackType.Touch, "you");
        }

        public static string MakeAttackString(Being attacker, Being defender, AttackType type)
        {
            string prefix = MakePrefix(attacker);
            string action;
            if (_Words.ContainsKey(type))
                action = _Words[type];
            else
                action = "attack";
            string ending;
            if (_Endings.ContainsKey(type))
                ending = " " + _Endings[type];
            else
                ending = "";


            string suffix;
            if (attacker == SlashNetGame.Instance.Player)
                suffix = " the " + defender;
            else
            {
                if (action.EndsWith("ch"))
                    suffix = "es";
                else
                    suffix = "s";
            }

            return prefix + action + suffix + ending;
        }

        internal static string MakePrefix(Being being)
        {
            return being == SlashNetGame.Instance.Player ? "You " : "The " + being + " ";
        }

        internal static string MakePossessivePrefix(Being being)
        {
            return being == SlashNetGame.Instance.Player ? "Your " : "The " + being + "'s ";
        }

        internal static string MakeIntrVSuffix(Being being, string verb)
        {
            return being == SlashNetGame.Instance.Player ? "" : "s";
        }

        internal static string MakeMissString(Being attacker, Being defender)
        {
            string prefix = MakePrefix(attacker);
            string action = "miss" + ((attacker == SlashNetGame.Instance.Player) ? " the " + defender : "es");
            return prefix + action;
        }
    }
}