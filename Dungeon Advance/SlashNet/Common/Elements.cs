using System.Collections.Generic;
using SlashNet.Player;
using System;
namespace SlashNet
{
    public enum EffectType
    {
        Physical, Fire, Ice, Lightning, Sleep, Poison, Water, Acid, Special, Rust, Disintegration, Speed,
        Stealth, Death
    }

    public class IntrinsicGrammar
    {
        private static IntrinsicGrammar _Instance;

        internal static IntrinsicGrammar Instance
        {
            get { return _Instance; }
        }

        internal static void Initialise()
        {
            _Instance = new IntrinsicGrammar();
        }


        public string MakeMessage(EffectType type, bool gain, bool level, PlayerCharacter player)
        {

            switch (type)
            {
                case EffectType.Ice:
                    if (gain)
                    {
                        if (level)
                            return "You feel warm";
                        else
                            return "You feel full of hot air";
                    }
                    else
                    {
                        return "You feel cooler";
                    }
                case EffectType.Disintegration:
                    if (player.Hallucinating)
                        return "You feel totally together, man";
                    else
                        return "You feel very firm";
                case EffectType.Fire:
                    if (gain)
                    {
                        if (level)
                            return "You feel cool";
                        else
                            return "You feel a momentary chill";
                    }
                    else
                    {
                        return "You feel warmer";
                    }
                case EffectType.Poison:
                    if (gain)
                    {
                        if (level)
                        {
                            if (player.Role.Name == "monk")
                                return "You feel healthy";
                            else if (player.Role.Name == "tourist")
                                return "You feel hardy";
                            else
                                throw new Exception("Gain level poison and not monk/tourist?");
                        }
                        else
                        {
                            if (true)
                                return "You feel healthy";
                        }
                    }
                    else
                    {
                        return "You feel a little sick";
                    }
                case EffectType.Lightning:
                    if (gain)
                    {
                        if (level)
                        {
                            return "You feel insulated";
                        }
                        else
                        {
                            if (player.Hallucinating)
                                return "You feel grounded in reality";
                            else
                                return "Your health currently feels amplified";
                        }
                    }
                    else
                    {
                        return "You feel conductive";
                        
                    }
                case EffectType.Sleep:
                    if (gain)
                    {
                        if (level)
                        {
                            return "You feel awake";
                        }
                        else
                        {
                            return "You feel wide awake";
                        }
                    }
                    else
                        return "You feel tired";
            }

            return null;
        }
    }
}