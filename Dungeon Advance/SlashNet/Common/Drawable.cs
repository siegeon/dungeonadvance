using System;
using System.Collections.Generic;
using System.Text;


namespace SlashNet
{
    public struct Drawable
    {
        public readonly string Char;
        public readonly SNColor Color;

        public Drawable(string character, SNColor color)
        {
            Char = character;
            Color = color;
        }

        public static Drawable Null = new Drawable(" ", SNColor.Black);
    }
}
