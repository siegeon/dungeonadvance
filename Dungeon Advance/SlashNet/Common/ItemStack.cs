using System;
using System.Collections.Generic;
using System.Text;

using SlashNet.Items;

namespace SlashNet
{
    public class ItemStack : IEnumerable<string>
    {
        private List<SingleOrStack> _Items = new List<SingleOrStack>();

        public int Count
        {
            get { return _Items.Count; }
        }

        public SingleOrStack this[int i]
        {
            get
            {
                return _Items[i];
            }
        }
	

        internal void Add(Item i, int amount)
        {
            if (!i.Stackable && amount != 1)
                throw new Exception(i.Name + " is not stackable.");
            if (i.Stackable)
            {
                foreach (SingleOrStack list in _Items)
                {
                    if (i.Equals(list.Template))
                    {
                        list.Add(amount);
                        return;
                    }
                }
                SingleOrStack s = new SingleOrStack(i, amount);
                _Items.Add(s);

            }
            else
            {
                SingleOrStack s = new SingleOrStack(i, amount);
                _Items.Add(s);
            }

            _Items.Sort();
        }

        internal void Remove(Item i, int amount)
        {

            for (int index = 0; index < _Items.Count; index++)
            {
                if (i.Equals(_Items[index].Template))
                {
                    if (_Items[index].Remove(amount))
                        _Items.RemoveAt(index);
                    return;
                }
            }
        }



        #region IEnumerable Members

        public IEnumerator<string> GetEnumerator()
        {

            for (int i = 0; i < _Items.Count; i++)
            {
                SingleOrStack s = _Items[i];
                if (i == 0)
                    yield return "_" + s.Template.Type.ToString() + "_";
                else if (s.Template.Type != _Items[i - 1].Template.Type)
                    yield return "_" + s.Template.Type.ToString() + "_";
                yield return s.ToString();
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            yield return GetEnumerator();
        }

        #endregion

        #region IEnumerable<SingleOrStack> Members

        

        public IEnumerable<SingleOrStack> GetItemEnumerator()
        {
            List<SingleOrStack> items = new List<SingleOrStack>();
            foreach (SingleOrStack i in _Items)
                items.Add(i);
            return items;
        }

        #endregion

        internal void RemoveAt(int index)
        {
            _Items.RemoveAt(index);

        }

        internal void Add(SingleOrStack s)
        {
            if (s.Template.Stackable)
            {
                foreach (SingleOrStack list in _Items)
                {
                    if (s.Template.Equals(list.Template))
                    {
                        list.Add(s.Count);
                        return;
                    }
                }
                _Items.Add(s);

            }
            else
            {
                _Items.Add(s);
            }

            _Items.Sort();
        }

        internal void Remove(SingleOrStack item)
        {
            for (int index = 0; index < _Items.Count; index++)
            {
                if (item.Template.Equals(_Items[index].Template))
                {
                    _Items.RemoveAt(index);
                    return;
                }
            }
        }
    }

    public class SingleOrStack : IComparable<SingleOrStack>
    {
        public int Count;

        internal SingleOrStack(Item template, int amount)
        {
            Template = template;
            Count = amount;
        }

        public Item Template;
        public Type Type { get { return Template.GetType(); } }

        internal void Add(int amount)
        {
            Count += amount;
        }

        /// <summary>
        /// Removes an item from this stack.
        /// </summary>
        /// <returns>true if the stack is now empty</returns>
        internal bool Remove(int amount)
        {
            if (Count - amount < 0)
                throw new Exception();
            Count -= amount;
            return Count == 0;
        }


        #region IComparable<SingleOrStack> Members

        public int CompareTo(SingleOrStack other)
        {
            return Template.CompareTo(other.Template);
        }

        #endregion

        public override string ToString()
        {
            if (Count == 1)
                return "a " + Template.Prefix + Template.ToString() + " " + Template.Suffix;
            return Count + " " + Template.Prefix + Template.ToString() + "s " + Template.Suffix;
        }

        internal void Apply(SNPoint towards)
        {
            ApplyType t = Template.Apply(towards);
            switch (t)
            {
                case ApplyType.Wield:
                    SlashNetGame.Instance.UI.Pline("You wield " + this);
                    break;
                case ApplyType.UnWield:
                    SlashNetGame.Instance.UI.Pline("You unwield " + this);
                    break;
            }
        }
    }
}
