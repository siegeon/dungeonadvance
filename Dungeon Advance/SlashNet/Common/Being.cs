using System;
using System.Collections.Generic;
using System.Text;
using SlashNet.Items;
using SlashNet.Monsters;

namespace SlashNet
{
    public abstract class Being : ADrawable
    {
        protected int _Health, _MaxHealth;

        protected Being(int health)
        {
            _Health = health;
        }

        protected Attack[] _Attacks;

        public Attack[] Attacks
        {
            get
            {
                return _Attacks;
            }
        }

        protected Being()
        {
        }

        //Return true if the attack does damage.
        internal abstract AttackResult OnAttacks(Being defender);
        internal abstract AttackResult OnAttacks(Being defender, int attack);
        //Return true if the attack does damage.
        internal abstract bool OnAttacked(Being attacker);
        

        public abstract int AC { get; }

        public abstract int ToHit(Being versus);

        //public Attack

        protected List<EffectType> _Intrinsics;

        /// <summary>
        /// Applies an effect to a being.
        /// </summary>
        /// <param name="effectType">The effect to apply</param>
        /// <returns>true if the effect is not resisted</returns>
        internal virtual bool Apply(EffectType effectType)
        {
            if (!_Intrinsics.Contains(effectType))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public abstract int DamageBonus(Being versus);

        protected Item _Wielded;

        public Item Wielded
        {
            get
            {
                return _Wielded;
            }
            set
            {
                if (value == null)
                    _Attacks[0] = Attack.BareHanded;
                else
                    _Attacks[0] = Attack.Weapon;
                _Wielded = value;
            }
        }

        public int Health
        {
            get
            {
                return _Health;
            }
        }

        internal bool LoseHP(int amount)
        {
            _Health -= amount;
            return (_Health <= 0);
            
        }

        internal abstract void Die(Being attacker);

    }
}
