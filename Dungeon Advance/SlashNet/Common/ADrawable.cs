using System;
using System.Collections.Generic;
using System.Text;

namespace SlashNet
{
    public abstract class ADrawable
    {
        private int _ID;

        private static int _NextID = 0;

        public int ID { get { return _ID; } }

        protected ADrawable()
        {
            _ID = _NextID++;
        }
    }
}
