﻿using Assets.Code.DataAccess.Repositories;
using Assets.Code.SlashNet.Factories;
using Assets.Code.SlashNet.Factories.Specifications;
using DeepEqual.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dungeon_Advance.Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class WeaponRepository_Tests
    {
        [TestMethod]
        public void WeaponRepository_Read()
        {
            var sut = WeaponRepository;
            var weapon = WeaponBuilder.Configure(WeaponSpecifications.PlayerWeaponDefault).Build();

            sut.AddOrUpdate(weapon);

            sut.Get(weapon.Id).Case(
                some: p => p.ShouldDeepEqual(weapon),
                none: Assert.Fail);
        }

        [TestMethod]
        public void WeaponRepository_Update()
        {
            var sut = WeaponRepository;
            var weapon = WeaponBuilder.Configure(WeaponSpecifications.PlayerWeaponDefault).Build();

            sut.AddOrUpdate(weapon);

            weapon = WeaponBuilder.Configure(weapon.ToModel)
                .SetName("Dagger")
                .Build();
            sut.AddOrUpdate(weapon);

            sut.Get(weapon.Id).Case(
                some: p => p.ShouldDeepEqual(weapon),
                none: Assert.Fail);
        }

        [TestMethod]
        public void WeaponRepository_Delete()
        {
            var sut = WeaponRepository;
            var weapon = WeaponBuilder.Configure(WeaponSpecifications.PlayerWeaponDefault).Build();
            sut.AddOrUpdate(weapon);
            sut.Remove(weapon);

            sut.Get(weapon.Id).IfSome(p => Assert.Fail());
        }

        private static WeaponRepository WeaponRepository => new WeaponRepository(new InMemoryPersistance());
    }
}