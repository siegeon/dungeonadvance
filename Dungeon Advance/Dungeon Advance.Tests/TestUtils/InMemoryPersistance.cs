﻿using Assets.Code.DataAccess.Base;
using Assets.Code.Maybe;

namespace Dungeon_Advance.Tests
{
    public class InMemoryPersistance : IPersistance
    {
        private string _file;

        public InMemoryPersistance()
        {
            _file = string.Empty;
        }

        public Maybe<string> Load() => _file == string.Empty ? 
            Maybe<string>.None : 
            Maybe<string>.Some(_file);


        public void Save(string data) => _file = data;
    }
}