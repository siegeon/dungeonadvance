﻿using Assets.Code.DataAccess.Repositories;
using Assets.Code.SlashNet.Factories;
using Assets.Code.SlashNet.Factories.Specifications;
using Assets.Code.SlashNet.Player;
using DeepEqual.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dungeon_Advance.Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class PlayerRepository_Tests
    {
        [TestMethod]
        public void PlayerRepository_Read()
        {
            var sut = PlayerRepository;
            var player = PlayerBuilder.Configure(PlayerSpecifications.Default).Build;

            sut.AddOrUpdate(player);

            sut.Get(player.Id).Case(
                some: p => p.ShouldDeepEqual(player),
                none: Assert.Fail);
        }

        [TestMethod]
        public void PlayerRepository_Update()
        {
            var sut = PlayerRepository;
            var player = PlayerBuilder.Configure(PlayerSpecifications.Default).Build;
            sut.AddOrUpdate(player);

            player.Name = "bill";
            sut.AddOrUpdate(player);

            sut.Get(player.Id).Case(
                some: p => p.ShouldDeepEqual(player),
                none: Assert.Fail);
        }

        [TestMethod]
        public void PlayerRepository_Delete()
        {
            var sut = PlayerRepository;
            var player = PlayerBuilder.Configure(PlayerSpecifications.Default).Build;
            sut.AddOrUpdate(player);
            sut.Remove(player);

            sut.Get(player.Id).IfSome(p => Assert.Fail());
        }

        private static PlayerRepository PlayerRepository => new PlayerRepository(new InMemoryPersistance(), WeaponRepository, ArmorRepository);
        private static WeaponRepository WeaponRepository => new WeaponRepository(new InMemoryPersistance());
        private static ArmorRepository ArmorRepository => new ArmorRepository(new InMemoryPersistance());
    }
}